# BOPAPP

## Using

Include the built source (build/bopapp.js) in the page and initialize it with some transaction data...

	bopapp.init(transactionData);

### Dependencies

The following is a list of dependencies and tested CDNs for each.

jQuery: https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js
bacon: https://cdnjs.cloudflare.com/ajax/libs/bacon.js/0.4.2/Bacon.min.js
bacon.UI https://raw.github.com/raimohanska/Bacon.UI.js/master/Bacon.UI.js (no CDN for this at time of writing.. so it is included in the minified source for now)
bootstrap: https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/2.3.2/js/bootstrap.min.js
underscore: https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.4.4/underscore-min.js
handlebars: https://cdnjs.cloudflare.com/ajax/libs/handlebars.js/1.0.0/handlebars.min.js

## Development

Development uses requirejs and the starting point is src/dev.html

Run:
```
  npm start
```
Visit:
```
  http://localhost:8000/dev.html?channel=stdSARB
```

### Building.

This project uses the r.js optimizer from the requirejs project (http://requirejs.org/). 

Using NodeJS, you can build the minified version from here with the following command:

	node r.js -o build.js


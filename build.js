({
  optimize: 'none',
  baseUrl : 'src',
  appDir    : "",
  dir     : "build/src",
  modules: [
    {
      name : 'app.module'
    }
  ],

  optimizeCss: 'none',
  removeCombined: true,
  mainConfigFile: 'src/app.module.js',
  //wrapShim: true,
 // wrap: true,
 // wrap: {
 //   start: "define(['require'],function(require){",
 //   end: ";return require('app.module');});"
 // },
  shim: {
    "angular-material": ['angular','angular-animate','angular-aria','angular-messages'],
    'angular-animate': ['angular'],
    'angular-aria': ['angular'],
    'angular-messages': ['angular'],
    'angular-sanitize': ['angular'],
    'angular-bootstrap': ['angular'],
    'angular-bootstrap-templates': ['angular','angular-bootstrap'],
    angular                      : {deps: [ 'underscore'], exports: "angular"},
  },

  paths: {
    'config'          : 'empty:',
    coreRules             : 'empty:',
    text              : '../node_modules/requirejs-text/text',
    angular           : 'empty:',//: '../bower_components/angular/angular',

    'angular-animate': '../node_modules/angular-animate/angular-animate',
    'angular-aria': '../node_modules/angular-aria/angular-aria',
    'angular-messages': '../node_modules/angular-messages/angular-messages',
    'angular-sanitize': '../node_modules/angular-sanitize/angular-sanitize',
    'angular-bootstrap': '../node_modules/angular-ui-bootstrap/dist/ui-bootstrap',
    'angular-bootstrap-templates': '../node_modules/angular-ui-bootstrap/dist/ui-bootstrap-tpls',
    'angular-material':'../node_modules/angular-material/angular-material',
    'es6-promise': '../node_modules/es6-promise/dist/es6-promise',
    underscore                   : '../node_modules/underscore/underscore',
    'ng-file-upload': '../node_modules/ng-file-upload/dist/ng-file-upload'
  }

})
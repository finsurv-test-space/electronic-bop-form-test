define(['require','../rules/tools/DependencyTools'],function(require) {
  /**
   * Load test data from a channel or revert to default testData.
   */
  return function(channelName, basePath) {
    basePath = basePath ? basePath : "../rules/reg_rule_repo";
    return new Promise(function(resolve, reject) {
      require([
        basePath + "/" + channelName + "/tests/testData"
      ], function(testData) {
        resolve(testData);
      }, function() { // if not exists, load default testData
        require(["./testData"], function(testData) {
          resolve(testData);
        }, reject);
      });
    });
  };
});


// attempt at reusable BOP tree walker




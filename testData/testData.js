var testData = [
  {
    name: "Empty transaction",
    data: {}
  },
  {
    name: "Minimal individuals Out",
    data: {
      Flow: "OUT",
      Resident: {
        Individual: {}
      },
      NonResident: {
        Individual: {}
      }
    }
  },
  {
    name: "Minimal individuals In",
    data: {
      Flow: "IN",
      Resident: {
        Individual: {}
      },
      NonResident: {
        Individual: {}
      }
    }
  },
  {
    name: "Minimal information INV",

    data: {
      transaction: {
        TrnReference: "InfoProvidedInv2",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        ZAREquivalent: "1234",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660"
          }
        }
      }
    }
  },

  {
    name: "Minimal entities",
    data: {
      Flow: "OUT",
      Resident: {
        Entity: {}
      },
      NonResident: {
        Entity: {}
      }
    }
  },
  {
    name: "zz1 with 3rd party",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "NON REPORTABLE",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "",
              AddressLine2: "",
              Suburb: "",
              City: "",
              PostalCode: "",
              Province: ""
            },
            PostalAddress: {
              AddressLine1: "",
              AddressLine2: "",
              Suburb: "",
              City: "",
              PostalCode: "",
              Province: ""
            },
            ContactDetails: {
              ContactName: "",
              ContactSurname: "",
              Email: "",
              Fax: "",
              Telephone: ""
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          },
          Description: "John, Johnson"
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "ZZ1",
            ForeignValue: "1000",
            LocationCountry: "",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "",
              SARBAuthRefNumber: ""
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1",
            Description: "Report this BOPCUS transaction as a NON REPORTABLE",
            ThirdParty: { Individual: { DateOfBirth: "2016-09-04" } }
          }
        ],
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      },
      customData: {
        AllowZZ1: true
      }
    }
  },
  {
    name: "Loan Validation test",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "SBOT16257ZA0003524001",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2016-09-14",
        FlowCurrency: "USD",
        TotalForeignValue: 6,
        BranchCode: "ZA01",
        BranchName: "Foreign Trade Services Central Gauteng",
        OriginatingBank: "SBZAZAJ0XXX",
        OriginatingCountry: "ZA",
        CorrespondentBank: "CHASUSU0XXX",
        CorrespondentCountry: "US",
        ReceivingBank: "SCBLUS33XXX",
        ReceivingCountry: "US",
        IsADLA: "N",
        Resident: {
          Entity: {
            EntityName: "Compair (S A) (Pty) Ltd.",
            TradingName: "test",
            RegistrationNumber: "1927/000445/07",
            InstitutionalSector: "02",
            IndustrialClassification: "03",
            AccountName: "Columbus Stainless (Pty) Ltd.",
            AccountIdentifier: "FCA RESIDENT",
            AccountNumber: "0000000090401247",
            TaxNumber: "9700/156/71/5",
            VATNumber: "NO VAT NUMBER",
            CustomsClientNumber: "21143776",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "Stand 255",
              AddressLine2: "Berrange Road",
              Suburb: "Wadeville",
              City: "Germiston",
              PostalCode: "1428"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "56 treetop street",
              AddressLine2: "applegate avenue",
              Suburb: "BRYANSTAN",
              City: "Johannesburg",
              PostalCode: "2001"
            },
            ContactDetails: {
              ContactName: "lucy",
              ContactSurname: "lucy",
              Email: "sandeep@gmail.com"
            }
          },
          Description: "Compair (S A) (Pty) Ltd., t/a test"
        },
        NonResident: {
          Entity: {
            EntityName: "Cross Border Bene",
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "0000000090637704",
            Address: { Country: "US" }
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: 6,
            SARBAuth: { RulingsSection: "B.13(A)" },
            CategoryCode: "810",
            CategorySubCode: "",
            ThirdParty: {
              Individual: {
                Surname: "mishra",
                Name: "sandeep",
                Gender: "M",
                IDNumber: "33454445",
                TempResPermitNumber: "444444444",
                PassportNumber: "j665565665",
                PassportCountry: "US",
                IDType: "Identification number"
              },
              TaxNumber: "22222222",
              VATNumber: "44444444",
              CustomsClientNumber: "70707070",
              StreetAddress: {
                Province: "GAUTENG",
                AddressLine1: "st pauls",
                AddressLine2: "paulshof",
                Suburb: "rodepoort",
                City: "joburg",
                PostalCode: "2191"
              },
              PostalAddress: {
                Province: "GAUTENG",
                AddressLine1: "st peters",
                AddressLine2: "paulshof",
                Suburb: "roodepoort",
                City: "joburg",
                PostalCode: "2195"
              },
              ContactDetails: {
                ContactName: "rahul",
                ContactSurname: "das",
                Fax: "0785987812"
              }
            },
            CannotCategorize: "otherBopDetails",
            LoanRefNumber: "99456745674567",
            LoanTenor: "ON DEMAND",
            LoanInterestRate: "13.89 13.89",
            LocationCountry: "US",
            SequenceNumber: 1,
            Description:
              "Loan made by a resident to a resident temporarily abroad",
            ThirdPartyKind: "Individual"
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: true,
        AccountDrCr: "CR",
        snapShot: {
          Version: "FINSURV",
          TrnReference: "SBOT16257ZA0003524001",
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          ValueDate: "2016-09-14",
          FlowCurrency: "USD",
          TotalForeignValue: 6,
          BranchCode: "ZA01",
          BranchName: "Foreign Trade Services Central Gauteng",
          OriginatingBank: "SBZAZAJ0XXX",
          OriginatingCountry: "ZA",
          CorrespondentBank: "CHASUSU0XXX",
          CorrespondentCountry: "US",
          ReceivingBank: "SCBLUS33XXX",
          ReceivingCountry: "US",
          IsADLA: "N",
          Resident: {
            Entity: {
              EntityName: "Compair (S A) (Pty) Ltd.",
              TradingName: "test",
              RegistrationNumber: "1927/000445/07",
              InstitutionalSector: "02",
              IndustrialClassification: "03",
              AccountName: "Columbus Stainless (Pty) Ltd.",
              AccountIdentifier: "FCA RESIDENT",
              AccountNumber: "0000000090401247",
              TaxNumber: "9700/156/71/5",
              VATNumber: "NO VAT NUMBER",
              CustomsClientNumber: "21143776",
              StreetAddress: {
                Province: "GAUTENG",
                AddressLine1: "Stand 255",
                AddressLine2: "Berrange Road",
                Suburb: "Wadeville",
                City: "Germiston",
                PostalCode: "1428"
              },
              PostalAddress: {
                Province: "GAUTENG",
                AddressLine1: "56 treetop street",
                AddressLine2: "applegate avenue",
                Suburb: "BRYANSTAN",
                City: "Johannesburg",
                PostalCode: "2001"
              },
              ContactDetails: {
                ContactName: "lucy",
                ContactSurname: "lucy",
                Email: "sandeep@gmail.com"
              }
            }
          },
          NonResident: {
            Entity: {
              EntityName: "Cross Border Bene",
              AccountIdentifier: "NON RESIDENT OTHER",
              AccountNumber: "0000000090637704",
              Address: { Country: "US" }
            }
          },
          MonetaryAmount: [
            {
              MoneyTransferAgentIndicator: "AD",
              ForeignValue: 6,
              SARBAuth: { RulingsSection: "B.13(A)" },
              CategoryCode: "810",
              CategorySubCode: "",
              ThirdParty: {
                Individual: {
                  Surname: "mishra",
                  Name: "sandeep",
                  Gender: "M",
                  IDNumber: "33454445",
                  TempResPermitNumber: "444444444",
                  PassportNumber: "j665565665",
                  PassportCountry: "US"
                },
                Entity: { Name: "sandeep", RegistrationNumber: "54555556" },
                TaxNumber: "22222222",
                VATNumber: "44444444",
                CustomsClientNumber: "70707070",
                StreetAddress: {
                  Province: "GAUTENG",
                  AddressLine1: "st pauls",
                  AddressLine2: "paulshof",
                  Suburb: "rodepoort",
                  City: "joburg",
                  PostalCode: "2191"
                },
                PostalAddress: {
                  Province: "GAUTENG",
                  AddressLine1: "st peters",
                  AddressLine2: "paulshof",
                  Suburb: "roodepoort",
                  City: "joburg",
                  PostalCode: "2195"
                },
                ContactDetails: {
                  ContactName: "rahul",
                  ContactSurname: "das",
                  Fax: "0785987812"
                }
              },
              CannotCategorize: "otherBopDetails",
              LoanRefNumber: "99456745674567",
              LoanTenor: "ON DEMAND",
              LoanInterestRate: "13.89 13.89",
              LocationCountry: "US",
              SequenceNumber: 1
            }
          ]
        }
      }
    }
  },
  {
    name: "Std Bank Test",
    data: {
      customData: {
        readOnly: true
      },
      transaction: {
        TrnReference: "TestRef12414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        TotalForeignValue: "1001",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        ReplacementTransaction: "Y",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            ThirdParty: {
              Individual: {
                Surname: "mishra",
                Name: "sandeep",
                Gender: "M",
                IDNumber: "33454445",
                TempResPermitNumber: "444444444",
                PassportNumber: "j665565665",
                PassportCountry: "US",
                IDType: "Identification number"
              },
              TaxNumber: "",
              VATNumber: "44444444",
              CustomsClientNumber: "70707070",
              StreetAddress: {
                Province: "GAUTENG",
                AddressLine1: "st pauls",
                AddressLine2: "paulshof",
                Suburb: "rodepoort",
                City: "joburg",
                PostalCode: "2191"
              },
              PostalAddress: {
                Province: "GAUTENG",
                AddressLine1: "st peters",
                AddressLine2: "paulshof",
                Suburb: "roodepoort",
                City: "joburg",
                PostalCode: "2195"
              },
              ContactDetails: {
                ContactName: "rahul",
                ContactSurname: "das",
                Fax: "0785987812"
              }
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "2"
          }
        ]
      } //,
      // customData: {readOnly: true}
    }
  },
  // {
  //   name: "simple valid",
  //   data: {
  //     transaction: {
  //       TotalValue: 1000,
  //       TrnReference: "TestRef12414",
  //       Version: "FINSURV",
  //       ReportingQualifier: "BOPCUS",
  //       Flow: "OUT",
  //       AccountHolderStatus: "South African Resident",
  //       LocationCountry: "NZ",
  //       Resident: {
  //         Individual: {
  //           StreetAddress: {
  //             AddressLine1: "MNI Towers 2",
  //             AddressLine2: "11 Jalan Pinang",
  //             Suburb: "Kuala Lumpur",
  //             City: "Johannesburg",
  //             PostalCode: "2196",
  //             Province: "GAUTENG",
  //             Country: "ZA"
  //           },
  //           PostalAddress: {
  //             AddressLine1: "50200",
  //             AddressLine2: "Kuala Lumpur",
  //             Suburb: "POSTALSUBURB",
  //             City: "Johannesburg",
  //             PostalCode: "2196",
  //             Province: "GAUTENG"
  //           },
  //           ContactDetails: {
  //             ContactName: "CONTACTNAME",
  //             ContactSurname: "CONTACTSURNAME",
  //             Email: "CONTACTEMAIL@GMAIL.COM",
  //             Fax: "2761360578",
  //             Telephone: "2761305978"
  //           },
  //           Name: "John",
  //           Surname: "Johnson",
  //           AccountIdentifier: "RESIDENT OTHER",
  //           AccountName: "Account_NT 1 p",
  //           AccountNumber: "221495398",
  //           TaxNumber: "123456",
  //           VATNumber: "66666",
  //           Gender: "M",
  //           DateOfBirth: "1955-02-29",
  //           IDNumber: "5502290001080"
  //         }
  //       },
  //       NonResident: {
  //         Individual: {
  //           Address: {
  //             AddressLine1: "addressline1",
  //             AddressLine2: "addressline2",
  //             Suburb: "suurb",
  //             City: "townname",
  //             PostalCode: "postalcode",
  //             Country: "NG",
  //             State: "GAUTENG"
  //           },
  //           AccountIdentifier: "NON RESIDENT OTHER",
  //           AccountNumber: "9032243812587",
  //           Name: "Fred",
  //           Gender: "M",
  //           Surname: "Georgeson"
  //         }
  //       },
  //       MonetaryAmount: [
  //         {
  //           CategoryCode: "407",
  //           ForeignValue: "1000",
  //           LocationCountry: "US",
  //           SARBAuth: {
  //             AuthIssuer: "024",
  //             SARBAuthAppNumber: "1234",
  //             SARBAuthRefNumber: "1234"
  //           },
  //           MoneyTransferAgentIndicator: "AD",
  //           SequenceNumber: "1"
  //         }
  //       ],
  //       TotalForeignValue: "1000",
  //       ValueDate: "2015-06-26",
  //       FlowCurrency: "USD",
  //       OriginatingBank: "SBZAZAJJ",
  //       OriginatingCountry: "ZA",
  //       ReceivingCountry: "UG",
  //       ReceivingBank: "SBICUGKX",
  //       PaymentDetail: {
  //         IsCorrespondentBank: "N",
  //         BeneficiaryBank: { BankName: "Foo Bank" },
  //         SWIFTDetails: "Payment"
  //       }
  //     },
  //     customData: {
  //       TotalDomesticAmount: 199.13
  //     }
  //   }
  // },
  {
    name: "simple valid entity",
    data: {
      transaction: {
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            EntityName: "John",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { BankName: "Foo Bank" },
          SWIFTDetails: "Payment"
        }
      },
      customData: {
        TotalDomesticAmount: 199.13
      }
    }
  },
  {
    name: "Valid importExport",
    data: {
      customData: {
        LUClient: "N",
        TotalDomesticAmount: 200.22,
        readOnly: true
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360978",
              Telephone: "2761305978"
            },
            EntityName: "John Trading",
            RegistrationNumber: "12341234",
            IndustrialClassification: "03",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080",
            CustomsClientNumber: "12345661",
            InstitutionalSector: "02",
            TradingName: "Foo Bar's"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "103",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "01",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                SubsequenceNumber: "1",
                IVSResponse: { status: "pass" }
              },
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 300,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "235523",
                CustomsClientNumber: "95245830",
                SubsequenceNumber: "2",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          },
          {
            CategoryCode: "101",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "03",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "INV1234567",
                SubsequenceNumber: "1"
              },
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 300,
                ImportControlNumber: "INV0101234564",
                SubsequenceNumber: "2"
              }
            ],
            MoneyTransferAgentIndicator: "AD",
            SARBAuth: {
              AuthIssuer: "016",
              ADInternalAuthNumberDate: "2016-01-06",
              ADInternalAuthNumber: "12341234"
            },
            ADInternalAuthResponse: { status: "pass" },
            SequenceNumber: "2"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CCNResponse: {
          status: "warning",
          errorCode: "100",
          errorMessage: "Warning."
        }
      }
    }
  },
  {
    name: "valid Loan",
    data: {
      TrnReference: "TestRef12411",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360597",
            Telephone: "2761365278"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          //"TaxNumber": "123456",
          //"VATNumber": "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "309",
          ForeignValue: "1000",
          LocationCountry: "US",
          CategorySubCode: "06",
          LoanRefNumber: "3333333",
          LoanInterest: { InterestType: "FIXED", Rate: "0.3" },
          LoanInterestRate: "0.30",
          SARBAuth: {
            AuthIssuer: "024",
            SARBAuthAppNumber: "1234",
            SARBAuthRefNumber: "1234"
          },
          MoneyTransferAgentIndicator: "PAYPAL",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "valid 3rd party",
    data: {
      TrnReference: "TestRef12454",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2613605978"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "256",
          ForeignValue: "1000",
          LocationCountry: "US",
          ThirdParties: "Name1, Surname1",
          ThirdParty: {
            Individual: {
              Surname: "Surname1",
              Name: "Name1",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "8902205150088",
              PassportNumber: "12312312312312",
              PassportCountry: "ZA"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1244",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "Email0",
              Fax: "Fax1111111",
              Telephone: "Telephone0"
            }
          },
          SARBAuth: { AuthIssuer: "123" },
          MoneyTransferAgentIndicator: "WESTERNUNION",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "simple bad",
    data: {
      TrnReference: "TestRef22414",
      Version: "FOO",
      ReportingQualifier: "BAR",
      Flow: "BAD",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196#",
            Province: "GAUTENGS"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM FOO",
            Fax: "27613605978zzz",
            Telephone: "27613605978zz"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER ZZ",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "S",
          DateOfBirth: "2070-02-29",
          IDNumber: "7002290001080"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      TotalForeignValue: "-1000",
      ValueDate: "-2015-06-26",
      FlowCurrency: "USDS",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZAS",
      ReceivingCountry: "UGS",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "BAD importExport",
    data: {
      TrnReference: "TestRef1244B",
      Version: "BOPCUS",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM BAD",
            Fax: "27613605978BAD",
            Telephone: "27613605978BAD"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398BAD",
          TaxNumber: "1234BAD56",
          VATNumber: "66666",
          Gender: "BAD",
          DateOfBirth: "BAD-1955-02-29",
          IDNumber: "5502290001080BAD"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "USBAD"
          },
          AccountIdentifier: "NON RESIDENT OTHER BAD",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "103",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          CategorySubCode: "01",
          ImportExport: [
            {
              PaymentCurrencyCode: "USDBAD",
              PaymentValue: "345BAD",
              ImportControlNumber: "DBN200101011234567BAD",
              TransportDocumentNumber: "124124",
              CustomsClientNumber: "64562345BAD"
            },
            {
              PaymentCurrencyCode: "USDBAD",
              PaymentValue: "655BAD",
              ImportControlNumber: "DBN200101011234567BAD",
              TransportDocumentNumber: "235523",
              CustomsClientNumber: "95245834BAD"
            }
          ]
        }
      ],
      TotalForeignValue: "1000BAD",
      ValueDate: "BAD-2015-06-26",
      FlowCurrency: "USDBAD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZABAD",
      ReceivingCountry: "UGBAD",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Bad Loan",
    data: {
      TrnReference: "TestRef18414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "27613605978",
            Telephone: "27613605978"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29-BAD",
          IDNumber: "5502290001080BAD"
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "USBAD"
          },
          AccountIdentifier: "NON RESIDENT OTHER BAD",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "309",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          CategorySubCode: "06",
          LoanRefNumber: "EE3333",
          LoanInterest: { InterestType: "FIXED", Rate: "0.3" }
        }
      ],
      TotalForeignValue: "1000",
      ValueDate: "2015-06-26",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "UG",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Bad 3rd party - no NonRes",
    data: {
      TrnReference: "TestRef52414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196BAD",
            Province: "GAUTENGBAD"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM BAD",
            Fax: "27613605978BADS",
            Telephone: "27613605978BAD"
          },
          Name: "John",
          Surname: "Johnson",
          AccountIdentifier: "RESIDENT OTHER BAD",
          AccountName: "Account_NT 1 p",
          AccountNumber: "221495398",
          TaxNumber: "123456",
          VATNumber: "66666",
          Gender: "M",
          DateOfBirth: "1955-02-29",
          IDNumber: "5502290001080BAD"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "256",
          ForeignValue: "1000",
          LocationCountry: "USBAD",
          LoanRefNumber: "",
          LoanInterest: { InterestType: null, Rate: "0.3" },
          ThirdParties: "Name1, Surname1",
          ThirdParty: {
            Individual: {
              Surname: "Surname1",
              Name: "Name1",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "0000000000000BAD",
              PassportNumber: "12312312312312BAD",
              PassportCountry: "ZABAD"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1244BAD",
              Province: "GAUTENGBAD"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234BAD",
              Province: "GAUTENGBAD"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "Email0",
              Fax: "Fax1111111",
              Telephone: "Telephone0"
            }
          }
        }
      ],
      TotalForeignValue: "1000BAD",
      ValueDate: "2015BAD-06-26",
      FlowCurrency: "USDBAD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZABAD",
      ReceivingCountry: "UGBAD",
      ReceivingBank: "SBICUGKX"
    }
  },
  {
    name: "Own Transfer",
    data: {
      TrnReference: "TestRef22414",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Individual: {
          Name: "Verma",
          Surname: "VERMA SVRINDER KUMAR",
          Gender: "M",
          DateOfBirth: "1990-09-05",
          StreetAddress: {},
          PostalAddress: {},
          ContactDetails: {},
          AccountIdentifier: "CFC RESIDENT",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          TaxNumber: "12345",
          VATNumber: "2468"
        }
      },
      NonResident: {
        Individual: {
          Name: "Account NT 4",
          Surname: "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "ZZ1",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "US",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "ADLA",
    data: {
      TrnReference: "TestRef82414",
      customData: { ADLA: true },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "CFC RESIDENT",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "401",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "Own Transfer FCA",
    data: {
      TrnReference: "TestRef92414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "ZA",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Entity: {
          EntityName: "Foo",
          // "Surname": "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "ZA"
          },
          AccountIdentifier: "FCA RESIDENT",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "513",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "Data",
              Name: "Good",
              Gender: "M",
              DateOfBirth: "2006-05-04",
              IDNumber: "8810225493083"
            },
            TaxNumber: "TaxNumber0",
            VATNumber: "VATNumber0",
            CustomsClientNumber: "12341234",
            StreetAddress: {
              AddressLine1: "AddressLine10",
              AddressLine2: "AddressLine20",
              Suburb: "Suburb0",
              City: "City0",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "AddressLine11",
              AddressLine2: "AddressLine21",
              Suburb: "Suburb1",
              City: "City1",
              PostalCode: "1234",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "ContactName0",
              ContactSurname: "ContactSurname0",
              Email: "email@mail.com",
              Fax: "1231231234",
              Telephone: "1231234123"
            },
            $$hashKey: "object:59"
          },
          AdHocRequirement: { Subject: "NO", Description: "NONE" }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "USD",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Own Transfer CFC",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "513",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Future Date",
    data: {
      transaction: {
        TrnReference: "TestRef72414",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2050-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },
  {
    name: "AuthTest",
    data: {
      transaction: {
        TrnReference: "TestRef12499",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        Flow: "OUT",
        ValueDate: "25/02/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "NG",
        ReceivingBank: "SBICNGLX",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "100",
            MoneyTransferAgentIndicator: "TRAVELEX",
            ImportExport: [],
            ForeignValue: "1",
            ReversalTrnRefNumber: "23445",
            LocationCountry: "AG",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "NG",
            ReceivingBank: "SBICNGLX"
          }
        ],
        Resident: {
          Entity: {
            TradingName: "Grinaker Lta International Holdings Ltd ",
            RegistrationNumber: "C20326",
            EntityName: "Grinaker Lta International Holdings Ltd ",
            IndustrialClassification: "10",
            InstitutionalSector: "03",
            StreetAddress: {
              AddressLine1: "Jet Park Road ",
              AddressLine2: "Kempton Park ",
              Suburb: "Ruimsig",
              City: "Port Louis",
              PostalCode: "1620",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "P O Box 1517",
              AddressLine2: "Kempton Park ",
              Suburb: "Kempton Park",
              City: "JOHANNESBURG",
              PostalCode: "1620",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "OOSTHUIZEN ",
              ContactSurname: "MARGARET ",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Mr Dan Rensburg",
            AccountNumber: "0000000022652418",
            TaxNumber: "43897888",
            VATNumber: "287945613"
          }
        },
        NonResident: {
          Individual: {
            Name: "abcd",
            Surname: "cdf",
            Gender: "M",
            PassportNumber: "9009050462086",
            Address: {
              AddressLine1: "35 vale Aveune",
              AddressLine2: "35 vale Aveune",
              Suburb: "NIG",
              City: "nigeria",
              PostalCode: "9001",
              State: "NG",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "0000876679"
          }
        }
      },
      customData: {
        readOnly: true,
        Valid: false,
        ADLA: false
      },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "Own Transfer ZZ1",
    data: {
      TrnReference: "TestRef654321",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "12345",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "ZZ1",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "AuthIssuer in Auth",
    data: {
      transaction: {
        TrnReference: "TestRef223344",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        Flow: "OUT",
        ValueDate: "01/03/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBZAZAJJ",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "ZZ1",
            SWIFTDetails: "as",
            MoneyTransferAgentIndicator: "MOMENTUM",
            ImportExport: [],
            ForeignValue: "1",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "ZA",
            ReceivingBank: "SBZAZAJJ"
          }
        ],
        Resident: {
          Individual: {
            Name: "JOHN",
            Surname: "PETER",
            Gender: "M",
            IDNumber: "9009050462086",
            ForeignIDNumber: "9009050462086",
            DateOfBirth: "",
            StreetAddress: {
              AddressLine1: "83 Club Street",
              Suburb: "Linksfield",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "246 Abbortsford avenue",
              AddressLine2: "abbotsford",
              Suburb: "Kempton Park",
              City: "JOHANNESBURG",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "Golan",
              ContactSurname: "Golan",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "GolanDoron",
            AccountNumber: "0000000022606068"
          }
        },
        NonResident: {
          Exception: {
            ExceptionName: "CFC RESIDENT NON REPORTABLE",
            AccountNumber: "000021701229"
          }
        }
      },
      customData: { readOnly: false, Valid: false, ADLA: false },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "simple valid IN Entity",
    data: {
      transaction: {
        TrnReference: "TestRef98765",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Entity: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            EntityName: "Fred Inc."
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "US",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },
  {
    name: "CFC ZAR",
    data: {
      TrnReference: "TestRef098765",
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "OUT",
      Resident: {
        Entity: {
          EntityName: "Verma",
          StreetAddress: {},
          PostalAddress: {},
          ContactDetails: {},
          AccountIdentifier: "CFC RESIDENT",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          TaxNumber: "12345",
          VATNumber: "2468"
        }
      },
      NonResident: {
        Individual: {
          Name: "Account NT 4",
          Surname: "Smith",
          Address: {
            AddressLine1: "2 Rissik Street",
            AddressLine2: "2 Rissik Street",
            City: "Johannesburg",
            PostalCode: "4001",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "000040452"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "401",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1"
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "US",
      ReceivingBank: "SBZAZAJJ"
    }
  },
  {
    name: "Test for Resident Rand to Resident FCA",
    data: {
      transaction: {
        TrnReference: "TestRef55555",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1.24",
        Flow: "OUT",
        ValueDate: "2016-04-15",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "MU",
        ReceivingBank: "SBZAZAJJ",
        Resident: {
          Entity: {
            RegistrationNumber: "1998/020171/07",
            EntityName: "Grains for Africa Commodity Brokers (Pty) Ltd.",
            IndustrialClassification: "01",
            StreetAddress: {
              AddressLine1: "3 Seventeenth Street",
              AddressLine2: "Orange Grove",
              Suburb: "Abbotsford",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "24 abbotsford road",
              AddressLine2: "abbotford",
              Suburb: "BoksburgWest",
              City: "Johannesburg",
              PostalCode: "2192",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "benny",
              ContactSurname: "benny",
              Telephone: "1234567890"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "NBOL TEST VOST ACC 4",
            AccountNumber: "0000000002355280"
          }
        },
        NonResident: {
          Entity: {
            EntityName: "Mr Pieter van Rensburg",
            Address: {
              AddressLine1: "Jet Park Road ",
              AddressLine2: "Kempton Park ",
              Suburb: "Ruimsig",
              City: "Port Louis",
              PostalCode: "1620",
              Country: "MU"
            },
            AccountIdentifier: "FCA RESIDENT",
            AccountNumber: "000090056574"
          }
        }
      },
      customData: {
        ADLA: false
      }
    }
  },
  {
    name: "Interest rate",
    data: {
      transaction: {
        TrnReference: "TestRef54321",
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "1",
        LoanInterestRate: "0.5",
        Flow: "OUT",
        ValueDate: "19/04/2016",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "NA",
        ReceivingBank: "SBNMNANX",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            CategoryCode: "801",
            MoneyTransferAgentIndicator: "AYOBA",
            ImportExport: [],
            SARBAuth: {
              ADInternalAuthNumber: "12800025",
              ADInternalAuthNumberDate: "2016-04-20",
              AuthIssuer: "016"
            },
            ADInternalAuthNumber: "12800025",
            ForeignValue: "1.00",
            LocationCountry: "US",
            OriginatingBank: "SBZAZAJJ",
            OriginatingCountry: "ZA",
            ReceivingCountry: "NA",
            ReceivingBank: "SBNMNANX",
            LoanInterestRate: "1.0",
            LoanRefNumber: "123456789"
          }
        ],
        Resident: {
          Individual: {
            Name: "Adam",
            Surname: "Gilchrist",
            Gender: "M",
            IDNumber: "8006269818083",
            ForeignIDNumber: "8006269818083",
            ForeignIDCountry: "ZA",
            DateOfBirth: "1980-06-26",
            StreetAddress: {
              AddressLine1: "4 hill street",
              AddressLine2: "Winsent Avenue",
              Suburb: "Soweto",
              City: "Pimville",
              PostalCode: "1724",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "4 hill street",
              AddressLine2: "Winsent Avenue",
              Suburb: "Soweto",
              City: "Pimville",
              PostalCode: "1724",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "Reigns",
              ContactSurname: "Roman",
              Email: "Roman.Rei@co.za",
              Fax: "4567890987",
              Telephone: "5678901234"
            },
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Acc24/04/2015",
            AccountNumber: "654650004",
            TaxNumber: "PK888",
            VATNumber: "4565"
          }
        },
        NonResident: {
          Individual: {
            Name: "crossbene",
            Surname: "ravi",
            Gender: "M",
            PassportNumber: "76756567",
            Address: {
              AddressLine1: "cfddgdgd",
              City: "gfgdfgdg",
              State: "NA",
              Country: "NA"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "655247939"
          }
        }
      },
      customData: {
        readOnly: true,
        Valid: false,
        ADLA: false,
        savePredefinedBOPThirdParty: true,
        saveAdhocBOPThirdParty: true
      },
      ivsSuccessCacheMap: {}
    }
  },
  {
    name: "simple valid IN Entity - no details for remitter",
    data: {
      transaction: {
        TrnReference: "TestRef24623626",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "123456",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "US",
        ReceivingCountry: "ZA",
        ReceivingBank: "SBICUGKX"
      } //,
      // customData: {readOnly: true}
    }
  },

  {
    name: "LARGE_DATA",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "103",
            ForeignValue: "500",
            LocationCountry: "US",
            CategorySubCode: "01",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "1234",
              SARBAuthRefNumber: "1234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "BOPCARD NON RESIDENT",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCARD NON RESIDENT",
        Flow: "IN",
        Resident: {
          Individual: {
            Surname: "lhv Etbldb",
            Name: "Skmwp Yixojb",
            IDNumber: "7910125082084"
          }
        },
        NonResident: { Individual: {} },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "CARD",
            SARBAuth: {},
            CategoryCode: "",
            CategorySubCode: "",
            SequenceNumber: 1,
            CardIndicator: "MASTER",
            ForeignCardHoldersPurchasesRandValue: "123",
            ForeignCardHoldersCashWithdrawalsRandValue: "0",
            LocationCountry: "AE"
          }
        ],
        TrnReference: "LEG3369868",
        ValueDate: "2013-02-17",
        FlowCurrency: "ZAR",
        TotalForeignValue: 75,
        IsADLA: false
      }
    }
  },
  {
    name: "Weidt ext call",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "1606210155SC7722",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2016-06-21",
        FlowCurrency: "USD",
        TotalForeignValue: 167,
        BranchCode: "01015500",
        BranchName: "TPS PAYMENTS PROCESSING",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingBank: "CHASUS33",
        ReceivingCountry: "US",
        ReplacementTransaction: "N",
        Resident: {
          Individual: {
            Surname: "SGTURWG",
            Name: "PJZMTPI",
            Gender: "F",
            DateOfBirth: "1958-01-17",
            IDNumber: "5801173580081",
            AccountName: "CXHWLNG NSLWSUM",
            AccountIdentifier: "RESIDENT OTHER",
            AccountNumber: "55102824548",
            TaxNumber: "1347624938",
            VATNumber: "9579654840P",
            TaxClearanceCertificateIndicator: "N",
            CustomsClientNumber: "00032392",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "GEXJZUNYC LFG 502",
              AddressLine2: "QKFDLCGL",
              Suburb: "BROOKLYN",
              City: "PRETORIA",
              PostalCode: "0181"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "GEXJZUNYC LFG 502",
              AddressLine2: "QKFDLCGL",
              Suburb: "BROOKLYN",
              City: "PRETORIA",
              PostalCode: "0181"
            },
            ContactDetails: {
              ContactName: "IWCGV",
              ContactSurname: "OWUKJIUW",
              Telephone: "8760841861"
            }
          }
        },
        NonResident: {
          Individual: {
            Surname: "UEGJ",
            Name: "UEGJ",
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "46876543",
            Address: { Country: "US" }
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "AD",
            RandValue: 1032.13,
            ForeignValue: 100,
            SARBAuth: { RulingsSection: "B.1(G)(V)" },
            CategoryCode: "106",
            CategorySubCode: "",
            LoanRefNumber: "99789078907890",
            LocationCountry: "NA",
            SequenceNumber: 1
          },
          {
            MoneyTransferAgentIndicator: "AD",
            RandValue: 691.53,
            ForeignValue: 67,
            SARBAuth: { RulingsSection: "B.1(G)(V)" },
            CategoryCode: "106",
            CategorySubCode: "",
            LoanRefNumber: "8200008343461",
            LocationCountry: "US",
            SequenceNumber: 2
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: false,
        AccountDrCr: "CR",
        TotalDomesticAmount: 1723.66
      }
    }
  },
  {
    name: "BOPCARD RESIDENT",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "8FD229D540DB1EE7A3AE5C0FAD249A",
        ReportingQualifier: "BOPCARD RESIDENT",
        Flow: "IN",
        ValueDate: "2017-08-29",
        FlowCurrency: "ZAR",
        TotalForeignValue: 810.66,
        ReplacementTransaction: "N",
        Resident: {
          Individual: {
            Surname: "Stavrinos",
            Name: "Angie",
            Gender: "F",
            DateOfBirth: "1980 - 02 - 14",
            IDNumber: "8002140092089",
            TempResPermitNumber: "0000000000000",
            AccountName: "DISCOVERY PLATINUM CARD",
            AccountIdentifier: "CREDIT CARD",
            AccountNumber: "08307185352376372",
            StreetAddress: {
              AddressLine1: "155 West",
              AddressLine2: "Sandton",
              Suburb: "",
              City: "Johannesburg",
              PostalCode: "2146"
            },
            ContactDetails: {
              ContactName: "Angie",
              ContactSurname: "Stavrinos",
              Email: "Angie@testing.co.za",
              Telephone: ""
            }
          },
          Description: "Angie, Stavrinos"
        },
        NonResident: {
          Entity: {
            CardMerchantName: "VISA",
            CardMerchantCode: "asss",
            Address: {}
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "CARD",
            RandValue: 810.66,
            ForeignValue: 810.66,
            SARBAuth: {},
            CategoryCode: "",
            CategorySubCode: "",
            ThirdParty: {
              Individual: {},
              StreetAddress: {},
              PostalAddress: {},
              ContactDetails: {}
            },
            CardChargeBack: "N",
            CardIndicator: "VISA",
            ElectronicCommerceIndicator: "0",
            POSEntryMode: "09",
            SequenceNumber: 1,
            Description:
              "Adjustments / Reversals / Refunds applicable to merchandise",
            ThirdPartyKind: "Individual"
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: true,
        AccountDrCr: "CR",
        TotalDomesticAmount: 810.66,
        snapShot: {
          Version: "FINSURV",
          TrnReference: "8FD229D540DB1EE7A3AE5C0FAD249A",
          ReportingQualifier: "BOPCARD RESIDENT",
          Flow: "IN",
          ValueDate: "2017 - 08 - 29",
          FlowCurrency: "ZAR",
          TotalForeignValue: 810.66,
          ReplacementTransaction: "N",
          Resident: {
            Individual: {
              Surname: "Stavrinos",
              Name: "Angie",
              Gender: "F",
              DateOfBirth: "1980 - 02 - 14",
              IDNumber: "8002140092089",
              TempResPermitNumber: "0000000000000",
              AccountName: "DISCOVERY PLATINUM CARD",
              AccountIdentifier: "CREDIT CARD",
              AccountNumber: "08307185352376372",
              StreetAddress: {
                AddressLine1: "155 West",
                AddressLine2: "Sandton",
                Suburb: "",
                City: "Johannesburg",
                PostalCode: "2146"
              },
              ContactDetails: {
                ContactName: "Angie",
                ContactSurname: "Stavrinos",
                Email: "Angie@testing.co.za",
                Telephone: ""
              }
            }
          },
          NonResident: {
            Entity: { CardMerchantName: "", CardMerchantCode: "", Address: {} }
          },
          MonetaryAmount: [
            {
              MoneyTransferAgentIndicator: "CARD",
              RandValue: 810.66,
              ForeignValue: 810.66,
              SARBAuth: {},
              CategoryCode: "",
              CategorySubCode: "",
              ThirdParty: {
                Individual: {},
                StreetAddress: {},
                PostalAddress: {},
                ContactDetails: {}
              },
              CardChargeBack: "N",
              CardIndicator: "VISA",
              ElectronicCommerceIndicator: "0",
              POSEntryMode: "09",
              CardFraudulentTransactionIndicator: "N",
              SequenceNumber: 1
            }
          ]
        }
      }
    }
  },
  {
    name: "ieBork",
    data: {
      customData: {
        DealerType: "AD",
        Corporate: "Acme Ltd",
        Division: "West",
        OVID: "123456789",
        edited: true,
        isValid: true
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            VATNumber: "66666",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080",
            CustomsClientNumber: "12341234"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "101",
            ForeignValue: "999",
            LocationCountry: "US",
            SARBAuth: {},
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1",
            TravelMode: {},
            ThirdParty: { Individual: {} },
            CategorySubCode: "01",
            ImportExport: [
              {
                SubsequenceNumber: "1",
                PaymentCurrencyCode: "USD",
                ImportControlNumber: "INVsf",
                PaymentValue: "999"
              }
            ]
          },
          {
            CategoryCode: "101",
            CategorySubCode: "02",
            ThirdParty: { Individual: {} },
            SequenceNumber: "2",
            SARBAuth: {},
            ForeignValue: "1",
            ImportExport: [
              {
                SubsequenceNumber: "1",
                PaymentCurrencyCode: "USD",
                ImportControlNumber: "INVfdsf",
                PaymentValue: "1"
              }
            ],
            TravelMode: {}
          }
        ],
        TotalValue: 1000,
        TrnReference: "TestRef12414",
        AccountHolderStatus: "South African Resident",
        LocationCountry: "NZ",
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "GAUTENG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Gender: "M",
            Surname: "Georgeson"
          }
        },
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { BankName: "Foo Bank" },
          SWIFTDetails: "Payment"
        },
        TransactionCurrency: "USD",
        RateConfirmation: "Y",
        IsInvestecFCA: "N"
      }
    }
  },
  {
    name: "InfoProvidedInv1",
    data: {
      customData: { OVID: "123456789" },
      transaction: {
        TrnReference: "InfoProvidedInv1",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine2: "ACME Bank",
              AddressLine1: "Ground Floor",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660"
          }
        }
      }
    }
  },
  {
    name: "InfoMissingInv1",
    data: {
      customData: { OVID: "123456789" },
      transaction: {
        ReportingQualifier: "BOPCUS",
        TrnReference: "InfoMissingInv1",
        Flow: "OUT",
        ValueDate: "2017-05-10",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "1000.00",
        Resident: {
          Entity: {
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            PostalAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine1: "Ground Floor",
              AddressLine2: "ACME Bank",
              Suburb: "Johannesburg",
              PostalCode: "2001",
              City: "Johannesburg",
              Province: ""
            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "",
            AccountName: "",
            AccountNumber: ""
          }
        }
      }
    }
  },
  {
    name: "Test Scenario 1 ",
    data: {
      transaction: {
        TrnReference: "2017102838383",
        Flow: "OUT",
        ValueDate: "2017-10-28",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "32146.90",
        ZAREquivalent: "432038.26",
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine1: "PO Box 12451",
              AddressLine2: "",
              Suburb: "Die Boord",
              PostalCode: "7613",
              City: "Cape Town",
              Province: "WESTERN CAPE"
            },
            StreetAddress: {
              AddressLine1: "16 De Boord",
              AddressLine2: "",
              Suburb: "Stellenbosch",
              PostalCode: "7613",
              City: "Stellenbosch",
              Province: "WESTERN CAPE"
            },
            EntityName: "Ferro South Africa (Pty) Limited",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "Ferro South Africa (Pty) Limited",
            RegistrationNumber: "1980/003695/06",
            AccountName: "",
            AccountNumber: "",
            AccountIdentifier: "",
            TaxNumber: "897293874",
            VATNumber: "12345678",
            CustomsClientNumber: ""
          }
        },
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "32146.90",
            RandValue: "432038.26"
          }
        ]
      },
      customData: {
        OVID: "b35f4017-9435-40e2-9183-6986aa845692",
        TradeReference: "5149504",
        CalypsoLegalEntityID: "399365",
        DocumentumID: "2017102838383"
      }
    }
  },
  {
    name: "Test Scenario 2 ",
    data: {
      transaction: {
        TrnReference: "201710045142",
        Flow: "OUT",
        ValueDate: "2017-10-04",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "EUR",
        TotalForeignValue: "51153.15",
        ZAREquivalent: "709749.96",
        Resident: {
          Individual: {
            PostalAddress: {
              AddressLine1: "29 Nicklaus Street",
              AddressLine2: "",
              Suburb: "Silver Lakes",
              PostalCode: "0001",
              City: "Pretoria",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine1: "29 Nicklaus Street",
              AddressLine2: "",
              Suburb: "Silver Lakes",
              PostalCode: "0001",
              City: "Pretoria",
              Province: "GAUTENG"
            },
            AccountName: "",
            AccountNumber: "",
            AccountIdentifier: "",
            DateOfBirth: "1955-04-28",
            Name: "Edward",
            PassportCountry: "ZA",
            PassportNumber: "A10000000",
            Gender: "Male",
            IDNumber: "5504285106080",
            Surname: "Kentridge",
            TaxNumber: "1636075846",
            VATNumber: "",
            CustomsClientNumber: "",
            TempResPermitNumber: ""
          }
        },
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "51153.15",
            RandValue: "709749.96"
          }
        ]
      },
      customData: {
        OVID: "4126f271-cc93-4b8c-9dae-c628785cf7a8",
        TradeReference: "4863973",
        CalypsoLegalEntityID: "399653",
        DocumentumID: "201710045142",
        IsOutsourcer: "N"
      }
    }
  },
  {
    name: "Test Scenario 3 3rd Party entity",
    data: {
      transaction: {
        TrnReference: "2017100318893",
        Flow: "OUT",
        ValueDate: "2017-10-03",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "EUR",
        TotalForeignValue: "22386.30",
        ZAREquivalent: "361529.79",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "22386.30",
            RandValue: "361529.79",
            ThirdParty: {
              Entity: {
                Name: "Peregrine Treasury Solutions (Pty) Ltd",
                RegistrationNumber: "2001/002395/01"
              },
              PostalAddress: {
                AddressLine1: "13 Bondev Park",
                AddressLine2: "",
                City: "Pretoria",
                PostalCode: "0171",
                Province: "GAUTENG",
                Suburb: "asdasd"
              },
              StreetAddress: {
                AddressLine1: "13 Bondev Park",
                AddressLine2: "",
                City: "Pretoria",
                PostalCode: "0171",
                Province: "GAUTENG",
                Suburb: "asdasd"
              },
              CustomsClientNumber: "",
              TaxNumber: "4562378654",
              VATNumber: "1890689546678",
              ContactDetails: {
                ContactName: "asdasd",
                ContactSurname: "asdasd",
                Email: "asdasd@asdsd.asd"
              }
            }
          }
        ]
      },
      customData: {
        OVID: "69743f90-fe97-4d2d-9b0b-134d9ab8031b",
        TradeReference: "5163920",
        CalypsoLegalEntityID: "1413499",
        DocumentumID: "2017100318893",
        IsOutsourcer: "Y"
      }
    }
  },
  {
    name: "Test Scenario 4 3rd Party Ind",
    data: {
      transaction: {
        TrnReference: "2017100412341",
        Flow: "OUT",
        ValueDate: "2017-10-04",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "23800.46",
        ZAREquivalent: "322996.1",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "23800.46",
            RandValue: "322996.1",
            ThirdParty: {
              Individual: {
                DateOfBirth: "1957-04-12",
                Gender: "Male",
                IDNumber: "5704125003089",
                Name: "Edward",
                PassportCountry: "ZA",
                PassportNumber: "A10345600",
                Surname: "Matthews",
                TempResPermitNumber: "string"
              },
              PostalAddress: {
                AddressLine1: "Postnet Suite 94",
                AddressLine2: "",
                City: "Pretoria",
                PostalCode: "0040",
                Province: "GAUTENG",
                Suburb: "Lynnwood Ridge"
              },
              StreetAddress: {
                AddressLine1: "29 Nicklaus Street",
                AddressLine2: "Silver Lakes",
                City: "Pretoria",
                PostalCode: "0001",
                Province: "GAUTENG",
                Suburb: "Silver Lakes"
              },
              CustomsClientNumber: "",
              TaxNumber: "234561145",
              VATNumber: ""
            }
          }
        ]
      },
      customData: {
        OVID: "41fa9203-9785-4cda-98e4-5e804972bd24",
        TradeReference: "5166571",
        CalypsoLegalEntityID: "1254203",
        DocumentumID: "2017100412341",
        IsOutsourcer: "Y"
      }
    }
  },
  {
    name: "Test Scenario 5",
    data: {
      transaction: {
        TrnReference: "WSS_013193717000001",
        Flow: "IN",
        ValueDate: "2017-08-25",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "IVESZAJJ0",
        OriginatingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "170000.00",
        ZAREquivalent: "2270350.00",
        ReceivingCountry: "ZA",
        PaymentDetail: {
          IsCorrespondentBank: "N",
          BeneficiaryBank: { SWIFTBIC: "ACBACB12XXX" }
        },
        Resident: {
          Entity: {
            PostalAddress: {
              AddressLine1: "",
              AddressLine2: "",
              Suburb: "",
              PostalCode: "",
              City: "",
              Province: ""
            },
            StreetAddress: {
              AddressLine1: "Office 10, 1st Floor, The Firs",
              AddressLine2: "",
              Suburb: "Rosebank",
              PostalCode: "2196",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            EntityName: "Currency Partners (Pty) Ltd",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "Currency Partners (Pty) Ltd",
            RegistrationNumber: "2012/021237/07",
            AccountName: "",
            AccountNumber: "",
            AccountIdentifier: "",
            TaxNumber: "9043231233",
            VATNumber: "234567",
            CustomsClientNumber: ""
          }
        },
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "170000.00",
            RandValue: "2270350.00"
          }
        ]
      },
      customData: {
        OVID: "b18cdf6d-9723-4baf-892c-9e3a5365106c",
        TradeReference: "5076108",
        CalypsoLegalEntityID: "1983772",
        DocumentumID: "WSS_013193717000001",
        IsOutsourcer: "N"
      }
    }
  },
  {
    name: "Test Scenario 6",
    data: {
      transaction: {
        TrnReference: "WSS_012236472000001",
        Flow: "IN",
        ValueDate: "2017-03-15",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "",
        OriginatingCountry: "",
        FlowCurrency: "EUR",
        TotalForeignValue: "51153.15",
        ZAREquivalent: "709749.96",
        Resident: {
          Individual: {
            PostalAddress: {
              AddressLine1: "72 Houghton Drive",
              AddressLine2: "",
              Suburb: "Houghton",
              PostalCode: "2198",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            StreetAddress: {
              AddressLine1: "72 Houghton Drive",
              AddressLine2: "",
              Suburb: "Houghton",
              PostalCode: "2198",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            AccountName: "",
            AccountNumber: "",
            AccountIdentifier: "",
            DateOfBirth: "1955-04-28",
            Name: "Edward",
            PassportCountry: "ZA",
            PassportNumber: "A10000000",
            Gender: "Male",
            IDNumber: "5504285106080",
            Surname: "Kentridge",
            TaxNumber: "1636075846",
            VATNumber: "",
            CustomsClientNumber: "",
            TempResPermitNumber: ""
          }
        },
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "51153.15",
            RandValue: "709749.96"
          }
        ]
      },
      customData: {
        OVID: "4126f271-cc93-4b8c-9dae-c628785cf7a8",
        TradeReference: "4863973",
        CalypsoLegalEntityID: "399653",
        DocumentumID: "WSS_012236472000001",
        IsOutsourcer: "N"
      }
    }
  },
  {
    name: "Test Scenario 7",
    data: {
      transaction: {
        TrnReference: "WSS_013327983000001",
        Flow: "IN",
        ValueDate: "2017-08-25",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "",
        OriginatingCountry: "",
        FlowCurrency: "USD",
        TotalForeignValue: "3654.98",
        ZAREquivalent: "48099.54",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "3654.98",
            RandValue: "48099.54",
            ThirdParty: {
              Entity: {
                Name: "Currency Partners (Pty) Ltd",
                RegistrationNumber: "2015/374045/07"
              },
              PostalAddress: {
                AddressLine1: "Suite 729, Private Bag X16",
                AddressLine2: "",
                City: "Cape Town",
                PostalCode: "7848",
                Province: "WESTERN CAPE",
                Suburb: "Tokai"
              },
              StreetAddress: {
                AddressLine1: "Suite 9, 2nd Floor, Madison Place",
                AddressLine2: "",
                City: "Cape Town",
                PostalCode: "7800",
                Province: "WESTERN CAPE",
                Suburb: "Constantia"
              },
              CustomsClientNumber: "",
              TaxNumber: "9265199175",
              VATNumber: "4890269311"
            }
          }
        ]
      },
      customData: {
        OVID: "26d7f093-66e5-439f-a5b1-d7c8e8ae516f",
        TradeReference: "5104509",
        CalypsoLegalEntityID: "899390",
        DocumentumID: "WSS_013327983000001",
        IsOutsourcer: "Y"
      }
    }
  },
  {
    name: "Test Scenario 8",
    data: {
      transaction: {
        TrnReference: "WSS_011883035000001",
        Flow: "IN",
        ValueDate: "2017-01-20",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "",
        OriginatingCountry: "",
        FlowCurrency: "GBP",
        TotalForeignValue: "23800.46",
        ZAREquivalent: "746550.00",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "23800.46",
            RandValue: "746550.00",
            ThirdParty: {
              Individual: {
                DateOfBirth: "1957-04-12",
                Gender: "Male",
                IDNumber: "5704125003089",
                Name: "Edward",
                PassportCountry: "ZA",
                PassportNumber: "A10345600",
                Surname: "Matthews",
                TempResPermitNumber: "string"
              },
              PostalAddress: {
                AddressLine1: "Postnet Suite 94",
                AddressLine2: "",
                City: "Pretoria",
                PostalCode: "0040",
                Province: "GAUTENG",
                Suburb: "Lynnwood Ridge"
              },
              StreetAddress: {
                AddressLine1: "29 Nicklaus Street",
                AddressLine2: "Silver Lakes",
                City: "Pretoria",
                PostalCode: "0001",
                Province: "GAUTENG",
                Suburb: "Silver Lakes"
              },
              CustomsClientNumber: "",
              TaxNumber: "234561145",
              VATNumber: "27655"
            }
          }
        ]
      },
      customData: {
        OVID: "41fa9203-9785-4cda-98e4-5e804972bd24",
        TradeReference: "4779734",
        CalypsoLegalEntityID: "1254203",
        DocumentumID: "WSS_011883035000001",
        IsOutsourcer: "Y"
      }
    }
  },
  {
    name: "ThirdPartyErrors",
    data: {
      customData: {
        OVID: "41fa9203-9785-4cda-98e4-5e804972bd24",
        TradeReference: "4779734",
        CalypsoLegalEntityID: "1254203",
        DocumentumID: "WSS_011883035000001",
        IsOutsourcer: "Y"
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Entity: {
            VATNumber: "",
            InstitutionalSector: "02",
            EntityName: "Currency Partners (Pty) Ltd",
            IndustrialClassification: "02",
            AccountNumber: "1100677565289",
            PostalAddress: {
              AddressLine2: "",
              AddressLine1: "",
              Suburb: "",
              PostalCode: "",
              Country: "",
              City: "",
              Province: ""
            },
            StreetAddress: {
              AddressLine2: "Cnr Cradock and Bierman Avenue",
              AddressLine1: "Office 10, 1st Floor, The Firs",
              Suburb: "Rosebank",
              PostalCode: "2019",
              Country: "ZA",
              City: "Johannesburg",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactSurname: "Dickosn",
              Email: "adrian.dickson@gmail.com",
              ContactName: "Adrian"
            },
            CustomsClientNumber: "899765",
            AccountIdentifier: "",
            TradingName: "Currency Partners (Pty) Ltd",
            RegistrationNumber: "2012/021237/07",
            TaxNumber: "9043231233",
            AccountName: ""
          }
        },
        NonResident: {
          Entity: {
            EntityName: "ABC Shoes",
            Address: {
              AddressLine1: "72 smart road",
              Suburb: "Beverley Gardens",
              State: "New York",
              PostalCode: "34567890",
              Country: "US",
              City: "New York"
            },
            IsMutualParty: "N",
            AccountNumber: "34567890098 - On SWIFT"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "511",
            ForeignValue: "170000.00",
            SARBAuth: {},
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                SubsequenceNumber: "1",
                PaymentValue: "170000"
              }
            ],
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            CategorySubCode: "01"
          }
        ],
        ReceivingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "170000.00",
        TrnReference: "Tyrone_Corp_Entity_In",
        OriginatingCountry: "",
        LocationCountry: "US",
        OriginatingBank: "",
        PaymentDetail: {
          BeneficiaryBank: {
            SWIFTBIC: "CITUSJJXXX"
          },
          SWIFTDetails: "Not sure this should be here - on SWIFT"
        },
        ValueDate: "2017-11-17",
        AccountHolderStatus: "South African Resident",
        ReceivingBank: "IVESZAJJ0"
      }
    }
  },
  {
    name: "24Nov2017 - remitter in error",
    data: {
      customData: {
        OVID: "008d482e-02dc-4fa7-bafe-d1a818bbfcda",
        TradeReference: "24Nov_Outsourcer_Entity_Applicant_Entity_In",
        CalypsoLegalEntityID: "899390",
        DocumentumID: "24Nov_Outsourcer_Entity_Applicant_Entity_In",
        EquationId: "198871",
        IsOutsourcer: "Y",
        SourceSystem: "Calypso"
      },
      transaction: {
        TrnReference: "24Nov_Outsourcer_Entity_Applicant_Entity_In",
        Flow: "IN",
        ValueDate: "2017-11-28",
        ReportingQualifier: "BOPCUS",
        OriginatingBank: "",
        OriginatingCountry: "",
        ReceivingBank: "IVESZAJJ0",
        ReceivingCountry: "ZA",
        FlowCurrency: "USD",
        TotalForeignValue: "3654.98",
        ZAREquivalent: "48099.54",
        MonetaryAmount: [
          {
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "3654.98",
            RandValue: "48099.54",
            ThirdParty: {
              Entity: {
                Name: "Currency Partners (Pty) Ltd",
                RegistrationNumber: "2015/374045/07"
              },
              PostalAddress: {
                AddressLine1: "Suite 729",
                AddressLine2: "Private Bag X16",
                City: "Cape Town",
                PostalCode: "7848",
                Province: "WESTERN CAPE",
                Suburb: "Tokai"
              },
              StreetAddress: {
                AddressLine1: "The Coach House",
                AddressLine2: "8 Bower Road",
                City: "Cape Town",
                PostalCode: "7800",
                Province: "WESTERN CAPE",
                Suburb: "Wynberg"
              },
              CustomsClientNumber: "",
              TaxNumber: "9265199175",
              VATNumber: "4890269311"
            }
          }
        ]
      }
    }
  },
  {
    name: "Anita issue 2018/02/06",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
          Entity: {
            AccountNumber: "1100203024194",
            EntityName: "Compo Agencies",
            TradingName: "",
            RegistrationNumber: "1973/003642/07",
            TaxNumber: "9135070846",
            VATNumber: "4410119830",
            StreetAddress: {
              AddressLine1: "ypbyoitbmk",
              Suburb: "Western Extension",
              City: "Benoni",
              Province: "Benoni",
              PostalCode: "1501",
              Country: "South Africa"
            },
            ContactDetails: {
              ContactSurname: "eeXXXXre",
              ContactName: "viXXXXav",
              Email: "JUciL@someweb.co.za",
              Telephone: "0115558228",
              Fax: ""
            },
            TaxClearanceCertificateIndicator: "N"
          },
          Type: "Entity"
        },
        NonResident: {
          Entity: {
            EntityName: "anita",
            Address: {
              Country: "AD"
            }
          }
        },
        MonetaryAmount: [
          {
            ImportExport: [],
            SARBAuth: {},
            TravelMode: {},
            ForeignValue: "12340.0",
            RandValue: "153442.96",
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            CategoryCode: "401"
          }
        ],
        ReceivingCountry: "ZA",
        FlowCurrency: "USD",
        OriginatingBank: "",
        TotalForeignValue: "12340.0",
        TrnReference: "090f4a1180755759",
        OriginatingCountry: "",
        ValueDate: "2018-02-07",
        ReceivingBank: "IVESZAJJ0",
        ZAREquivalent: "153442.96",
        LocationCountry: "AD"
      },
      customData: {
        EquationId: "205766",
        DocumentumID: "090f4a1180755759",
        OVID: "d6491a59-adfb-47b5-abca-b72f5d1fe0f2",
        IsOutsourcer: "Y",
        CalypsoLegalEntityID: "1838138",
        TradeReference: "CCM201802050002",
        SourceSystem: "Calypso",
        DealerType: "AD",
        snapShot: {
          ReceivingCountry: "ZA",
          FlowCurrency: "USD",
          OriginatingBank: "",
          TotalForeignValue: "12340.0",
          MonetaryAmount: [
            {
              ForeignValue: "12340.0",
              RandValue: "153442.96",
              SequenceNumber: "1",
              MoneyTransferAgentIndicator: "AD",
              ThirdParty: {
                PostalAddress: {
                  AddressLine2: "Monument Park",
                  AddressLine1: "Po Box 25752",
                  Suburb: " ",
                  PostalCode: "0105",
                  Country: "ZA",
                  City: "Pretoria",
                  Province: "GAUTENG"
                },
                VATNumber: "4560271480",
                Entity: {
                  RegistrationNumber: "2013/176186/07",
                  Name: "Russellstone Treasury (Pty) Limited"
                },
                StreetAddress: {
                  AddressLine2: "Monument Park",
                  AddressLine1: "510 Makou Street",
                  Suburb: " ",
                  PostalCode: "0105",
                  Country: "ZA",
                  City: "Pretoria",
                  Province: "GAUTENG"
                },
                CustomsClientNumber: " ",
                TaxNumber: "9234928191"
              }
            }
          ],
          TrnReference: "090f4a1180755759",
          OriginatingCountry: "",
          ReportingQualifier: "BOPCUS",
          ValueDate: "2018-02-07",
          Flow: "IN",
          ReceivingBank: "IVESZAJJ0",
          ZAREquivalent: "153442.96",
          Resident: {
            Entity: {
              AccountNumber: "1100203024194",
              EntityName: "Compo Agencies",
              TradingName: "",
              RegistrationNumber: "1973/003642/07",
              TaxNumber: "9135070846",
              VATNumber: "4410119830",
              StreetAddress: {
                AddressLine1: "ypbyoitbmk",
                Suburb: "Western Extension",
                City: "Benoni",
                Province: "Benoni",
                PostalCode: "1501",
                Country: "South Africa"
              },
              ContactDetails: {
                ContactSurname: "eeXXXXre",
                ContactName: "viXXXXav",
                Email: "JUciL@someweb.co.za",
                Telephone: "0115558228",
                Fax: ""
              }
            }
          }
        },
        isValid: false,
        edited: true
      }
    }
  },
  {
    name: "12 Feb 2018 - ZAR error",
    data: {
      customData: {
        OVID: "008d482e-02dc-4fa7-bafe-d1a818bbfcda",
        TradeReference: "24Nov_Outsourcer_Entity_Applicant_Entity_In",
        CalypsoLegalEntityID: "899390",
        DocumentumID: "24Nov_Outsourcer_Entity_Applicant_Entity_In",
        EquationId: "198871",
        IsOutsourcer: "Y",
        SourceSystem: "Calypso"
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            CardNumber: "",
            VATNumber: "",
            InstitutionalSector: "",
            EntityName: "dfadsfasdf",
            IndustrialClassification: "",
            AccountNumber: "1300035937500",
            PostalAddress: {
              AddressLine2: "",
              AddressLine1: "",
              Suburb: "",
              PostalCode: "",
              City: "",
              Province: ""
            },
            StreetAddress: {
              AddressLine2: "",
              AddressLine1: "dasfasdf",
              Suburb: "dfasdfsa",
              PostalCode: "2342",
              City: "dfadfasdf",
              Province: "LIMPOPO"
            },
            ContactDetails: {
              ContactSurname: "fasdfsdad",
              Email: "",
              Telephone: "0820000001",
              Fax: "",
              ContactName: "dfadsfasd"
            },
            CustomsClientNumber: "",
            AccountIdentifier: "",
            TaxClearanceCertificateIndicator: "",
            SupplementaryCardIndicator: "",
            TaxClearanceCertificateReference: "",
            RegistrationNumber: "dfadfsadfds",
            TradingName: "",
            TaxNumber: "",
            AccountName: "ZAR 1300035937500 Call And Notice Account"
          },
          Type: "Entity"
        },
        NonResident: {
          Entity: {
            Address: {
              Country: "AE"
            },
            AccountNumber: "LE823884949",
            EntityName: "dafasdf"
          }
        },
        MonetaryAmount: [
          {
            ImportExport: [],
            SARBAuth: {},
            TravelMode: {},
            ThirdParty: {},
            CategoryCode: "295",
            ForeignValue: "451"
          }
        ],
        ReceivingCountry: "LS",
        FlowCurrency: "ZAR",
        CorrespondentBank: "",
        TotalForeignValue: "451.00",
        TrnReference: "FT18020000004021",
        OriginatingCountry: "",
        ReplacementTransaction: "",
        ValueDate: "2018-02-12",
        CorrespondentCountry: "",
        PaymentDetail: {
          Charges: "SHA - Charges are shared by the beneficiary and applicant",
          IsCorrespondentBank: "N",
          BeneficiaryBank: {
            SwiftBIC: "SBICLSMXXXX",
            BankName: "STANDARD LESOTHO BANK LTD.",
            BranchCode: "32423",
            Address: "KINGSWAY",
            City: "MASERU"
          }
        },
        HubCode: "",
        HubName: "",
        TotalValue: "",
        OriginatingBank: "",
        BranchName: "",
        ReplacementOriginalReference: "",
        BranchCode: "",
        ReceivingBank: "",
        LocationCountry: "US"
      }
    }
  },
  {
    name: "NaN issue",
    data: {
      customData: {
        DocumentumID: "",
        LUClient: "",
        Branch: "",
        OVID: "",
        SaleslogixID: "",
        IsOutsourcer: "",
        CalypsoLegalEntityID: "",
        TradeReference: "",
        SourceSystem: "",
        EcommerceID: "469",
        EcommerceName: "FREDDYHIRSCHGROUP",
        DealerType: ""
      },
      transaction: {
        ReceivingCountry: "",
        FlowCurrency: "GBP",
        CorrespondentBank: "",
        TotalForeignValue: "10.00",
        TrnReference: "FT18020000004398",
        OriginatingCountry: "",
        ReplacementTransaction: "",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2018-02-23",
        CorrespondentCountry: "",
        PaymentDetail: {
          Charges:
            "OUR - All charges including offshore charges are paid by the applicant",
          IsCorrespondentBank: "N",
          BeneficiaryBank: {
            SWIFTBIC: "MIDLGB22XXX",
            BankName: "HSBC BANK PLC",
            BranchCode: "123123",
            Address: "8 CANADA SQUARE",
            City: "LONDON"
          }
        },
        NonResident: {},
        BeneficiaryBank: {
          SWIFTBIC: "MIDLGB22XXX",
          BankName: "HSBC BANK PLC",
          BranchCode: "",
          Address: "8 CANADA SQUARE",
          City: "LONDON"
        },
        HubCode: "",
        HubName: "",
        TotalValue: "",
        OriginatingBank: "",
        Resident: {
          Entity: {
            CardNumber: "",
            VATNumber: "4890106893",
            InstitutionalSector: "",
            EntityName: "Freddy Hirsch Group (Pty) Limited",
            IndustrialClassification: "03",
            AccountNumber: "1300035937503",
            PostalAddress: {
              AddressLine2: "Po Box 2554",
              AddressLine1: "Po Box 2554",
              Suburb: "",
              PostalCode: "8000",
              City: "Cape Town",
              Country: "ZA",
              Province: "WESTERN CAPE"
            },
            StreetAddress: {
              AddressLine2: "Voortrekker Street",
              AddressLine1: "11th Avenue",
              Suburb: "Maitland East",
              PostalCode: "7404",
              City: "Cape Town",
              Country: "ZA",
              Province: "WESTERN CAPE"
            },
            ContactDetails: {
              ContactSurname: "",
              Email: "",
              Telephone: "",
              Fax: "",
              ContactName: ""
            },
            CustomsClientNumber: "00034786",
            AccountIdentifier: "",
            TaxClearanceCertificateIndicator: "",
            SupplementaryCardIndicator: "",
            TaxClearanceCertificateReference: "",
            RegistrationNumber: "1956/000899/07",
            TradingName: "",
            TaxNumber: "9550133715",
            AccountName: "GBP 1300035937503 Loan Account"
          },
          Type: "Entity"
        },
        BranchName: "",
        ReplacementOriginalReference: "",
        BranchCode: "",
        ReceivingBank: ""
      }
    }
  },
  {
    name: "Adrian's issue 23 Feb 2018",
    data: {
      customData: {
        EquationId: "205766",
        DocumentumID: "090f4a1180766248",
        edited: true,
        OVID: "d6491a59-adfb-47b5-abca-b72f5d1fe0f2",
        isValid: true,
        IsOutsourcer: "Y",
        CalypsoLegalEntityID: "1838138",
        TradeReference: "CCM20180208005",
        SourceSystem: "Calypso",
        DealerType: "AD"
      },
      transaction: {
        ReceivingCountry: "AD",
        FlowCurrency: "CAD",
        MonetaryAmount: [
          {
            CategoryCode: "401",
            ForeignValue: "7206.00",
            ForeignCardHoldersPurchasesRandValue: "",
            SARBAuth: {},
            ImportExport: [],
            RandValue: "71496.06",
            SequenceNumber: "1",
            MoneyTransferAgentIndicator: "AD",
            TravelMode: {},
            ThirdParty: {
              PostalAddress: {
                AddressLine2: "Monument Park",
                AddressLine1: "Po Box 25752",
                Suburb: "mo",
                PostalCode: "0105",
                Country: "ZA",
                City: "Pretoria",
                Province: "GAUTENG"
              },
              VATNumber: "4560271480",
              Entity: {
                RegistrationNumber: "2013/176186/07",
                Name: "Russellstone Treasury (Pty) Limited"
              },
              StreetAddress: {
                AddressLine2: "Monument Park",
                AddressLine1: "510 Makou Street",
                Suburb: "mo",
                PostalCode: "0105",
                Country: "ZA",
                City: "Pretoria",
                Province: "GAUTENG"
              },
              CustomsClientNumber: "12345678",
              TaxNumber: "9234928191"
            },
            ForeignCardHoldersCashWithdrawalsRandValue: ""
          }
        ],
        TotalForeignValue: "7206.00",
        TrnReference: "090f4a1180766248",
        OriginatingCountry: "ZA",
        LocationCountry: "DE",
        ReportingQualifier: "BOPCUS",
        // "NonResident": {
        // },
        Flow: "OUT",
        OriginatingBank: "IVESZAJJ0",
        Version: "FINSURV",
        PaymentDetail: {
          BeneficiaryBank: {
            BankName: "RRR",
            Address: "RRR",
            City: "RR",
            BranchCode: "FRR"
          },
          IsCorrespondentBank: "N"
        },
        Resident: {
          Entity: {
            VATNumber: "4310183340",
            StreetAddress: {
              AddressLine2: "ouuhxsstqi",
              AddressLine1: "pmapbvlotg",
              Suburb: "Springfield Park",
              PostalCode: "4051",
              Country: "South Africa",
              City: "Durban North",
              Province: "Durban North"
            },
            EntityName: "Moore Sound and Electronics",

            TradingName: "",
            RegistrationNumber: "1999/041061/23",
            TaxNumber: "9140325201",
            AccountNumber: "1100203024196"
          },
          Type: "Entity"
        },
        ValueDate: "2018-02-12",
        ReceivingBank: "",
        ZAREquivalent: "71,496.06"
      }
    }
  },
  {
    name: "Kudz' issue",
    data: {
      transaction: {
        Version: "FINSURV",
        TrnReference: "xnkkZkavxuoH",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        ValueDate: "2018-02-07",
        FlowCurrency: "USD",
        TotalForeignValue: 1000,
        OriginatingBank: "OBaRbnhAMDWW",
        OriginatingCountry: "ZA",
        CorrespondentBank: "WLsFzjFSgKRJ",
        CorrespondentCountry: "SA",
        ReceivingBank: "JMSvXtWDClqr",
        ReceivingCountry: "US",
        ReplacementTransaction: "N",
        Resident: {
          Individual: {
            Surname: "ZafhmAdWVKPH",
            Name: "LnDXLOJdAGtw",
            Gender: "M",
            DateOfBirth: "1985-01-01",
            IDNumber: "8501016184086",
            AccountIdentifier: "CASH",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "iQetXQKeSFrL",
              Suburb: "bHNCCZvdXPSi",
              City: "XHZhKsScSnjt"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "OPlcgFBFKqRy",
              Suburb: "CglNuHlYmIWH",
              City: "ACMdcIVXADnK",
              PostalCode: "2021"
            },
            ContactDetails: {
              ContactName: "uVIaDaZasWzL",
              ContactSurname: "oGDTttfOGcfX",
              Email: "timmy@generic.co.za"
            }
          }
        },
        NonResident: {
          Individual: {
            Surname: "iHnrQbzRVocm",
            Name: "fefw",
            AccountIdentifier: "CASH",
            Address: { Country: "US" }
          }
        },
        MonetaryAmount: [
          {
            MoneyTransferAgentIndicator: "AD",
            RandValue: 2000,
            ForeignValue: 1000,
            SARBAuth: {
              RulingsSection: "Circular 22/2015",
              SARBAuthAppNumber: "dq",
              SARBAuthRefNumber: "dqw"
            },
            CategoryCode: "401",
            CategorySubCode: "",
            AdHocRequirement: {
              Subject: "REMITTANCE DISPENSATION",
              Description: "testing123"
            },
            LocationCountry: "US",
            SequenceNumber: 1
          }
        ]
      },
      customData: {
        DealerType: "AD",
        ShowOldCodes: true,
        AccountDrCr: "DR",
        TotalDomesticAmount: 2000
      }
    }
  },
  {
    name: "invUXP1",
    data: {
      ValueDate: "2018-02-12",
      TotalForeignValue: "7206.00",
      TrnReference: "090f4a1180766248",
      OriginatingCountry: "ZA",
      LocationCountry: "DE",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      FlowCurrency: "USD",
      OriginatingBank: "IVESZAJJ0",
      Version: "FINSURV",
      PaymentDetail: {
        BeneficiaryBank: {
          BankName: "RRR",
          Address: "RRR",
          City: "RR",
          BranchCode: "FRR"
        },
        IsCorrespondentBank: "N"
      },
      Resident: {
        Entity: {
          VATNumber: "4310183340",
          StreetAddress: {
            AddressLine2: "ouuhxsstqi",
            AddressLine1: "pmapbvlotg",
            Suburb: "Springfield Park",
            PostalCode: "4051",
            Country: "South Africa",
            City: "Durban North",
            Province: "Durban North"
          },
          EntityName: "Moore Sound and Electronics",

          TradingName: "",
          RegistrationNumber: "1999/041061/23",
          TaxNumber: "9140325201",
          AccountNumber: "1100203024196"
        },
        Type: "Entity"
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        },
        Type: "Individual"
      },
      MonetaryAmount: [
        {
          CategoryCode: "833"
        }
      ]
    }
  },
  {
    name: "invUXP2",
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Entity: {
          VATNumber: "4310183340",
          StreetAddress: {
            AddressLine2: "ouuhxsstqi",
            AddressLine1: "pmapbvlotg",
            Suburb: "Springfield Park",
            PostalCode: "4051",
            Country: "ZA",
            City: "Durban North",
            Province: "LIMPOPO"
          },
          EntityName: "Moore Sound and Electronics",
          TradingName: "",
          RegistrationNumber: "1999/041061/23",
          TaxNumber: "9140325201",
          AccountNumber: "1100203024196",
          ContactDetails: {
            Fax: "",
            Email: "asd@asd.asd",
            ContactSurname: "asda",
            ContactName: "asd"
          }
        },
        Type: "Entity"
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "addressline1",
            AddressLine2: "addressline2",
            Suburb: "suurb",
            City: "townname",
            PostalCode: "postalcode",
            Country: "US"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "9032243812587",
          Name: "Fred",
          Surname: "Georgeson"
        },
        Type: "Individual"
      },
      ValueDate: "2018-02-12",
      TrnReference: "090f4a1180766248",
      OriginatingCountry: "ZA",
      LocationCountry: "DE",
      FlowCurrency: "USD",
      OriginatingBank: "IVESZAJJ0",
      PaymentDetail: {
        BeneficiaryBank: {
          BankName: "RRR",
          Address: "RRR",
          City: "RR",
          BranchCode: "FRR"
        },
        IsCorrespondentBank: "N",
        SWIFTDetails: "124124124124"
      },
      TransactionCurrency: "USD",
      RateConfirmation: "N",
      IsInvestecFCA: "N",
      CounterpartyStatus: "Non Resident",
      ReceivingCountry: "AD",
      AccountHolderStatus: "South African Resident"
    }
  },
  {
    name: "weird zz1 to BOPCUS",

    data: {
      Version: "FINSURV",
      ReportingQualifier: "NON REPORTABLE",
      Flow: "IN",
      Resident: {
        Entity: {
          StreetAddress: {
            AddressLine1: "AECI Place   Woodlands Drive",
            AddressLine2: "Woodmead",
            Suburb: "Sandton",
            City: "WITKOPPEN",
            PostalCode: "2191",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "Private Bag  X21",
            AddressLine2: "Gallo Manor",
            Suburb: "BRYANSTON",
            City: "GAUTENG",
            PostalCode: "2052",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "TREVOR STARKE",
            ContactSurname: "SurNm contact",
            Email: "willwilliam.massinga@standardbank.co.za",
            Fax: "118068728",
            Telephone: "0118068700"
          },
          InstitutionalSector: "02",
          IndustrialClassification: "01",
          RegistrationNumber: "1924/002590/06",
          AccountIdentifier: "",
          AccountName: "The Standard Bank of South Africa L",
          AccountNumber: "7116979",
          TaxNumber: "22332233",
          VATNumber: "4350103539",
          TempResPermitNumber: "123123",
          EntityName: "Ash",
          TradingName: "AECI Ltd.",
          CustomsClientNumber: null
        }
      },
      NonResident: {
        Individual: {
          Address: {
            AddressLine1: "2009 EBIONN",
            AddressLine2: "",
            Suburb: "",
            City: "",
            PostalCode: null,
            Country: "ZA"
          },
          AccountIdentifier: "NON RESIDENT OTHER",
          AccountNumber: "7116979",
          Name: "TJ Madonsel",
          Surname: "MADONSEL",
          Gender: ""
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: null,
          CategorySubCode: null,
          ImportExport: [],
          SARBAuth: { SARBAuthAppNumber: "", SARBAuthRefNumber: "" },
          TravelMode: {},
          ThirdParty: {},
          SequenceNumber: "1",
          IECurrencyCode: "ZAR",
          ForeignValue: 1300,
          LocationCountry: "ZA",
          MoneyTransferAgentIndicator: "AD",
          Description: "CCN: "
        }
      ],
      TotalValue: 1300,
      TrnReference: "IL18177ZA0000170006",
      BranchCode: "ZA00",
      BranchName: "ZA00",
      HubCode: "ZA06",
      HubName: "ZA06",
      TotalForeignValue: 1300,
      ValueDate: "2018-10-02",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJ0XXX",
      OriginatingCountry: "ZA",
      ReceivingCountry: "",
      ReceivingBank: "",
      CorrespondentCountry: "ZA",
      CorrespondentBank: "SBZAZAJJXXX",
      Description: ""
    }
  },
  {
    name: "IBR valid transaction (ZAR)",
      data: {
        transaction: {
          Version: "FINSURV",
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: {
            Entity: {
              AccountIdentifier: "RESIDENT OTHER",
              InstitutionalSector: "02",
              IndustrialClassification: "02",
              AccountNumber: "12341233",
              EntityName: "asdasd asdsad",
              TradingName: "asd sad sa d",
              RegistrationNumber: "asd as ads d sa",
              StreetAddress: {
                AddressLine1: "sadasd",
                Suburb: "asdads",
                City: "asdsad",
                Province: "GAUTENG",
                PostalCode: "1234"
              },
              PostalAddress: {
                AddressLine1: "qweqwe",
                Suburb: "qweewq eqw",
                City: "qweqwe",
                PostalCode: "1234",
                Province: "NORTH WEST"
              },
              ContactDetails: {
                Email: "asdasd@asddas.as",
                ContactSurname: "asdasd",
                ContactName: "asdasddsa"
              },
              AccountName: "asfasf afafs af"
            },
            Type: "Entity"
          },
          NonResident: {
            Entity: {
              AccountIdentifier: "NON RESIDENT OTHER",
              Address: {
                Country: "BR"
              },
              EntityName: "asfafs",
              AccountNumber: "124124124"
            },
            Type: "Entity"
          },
          MonetaryAmount: [{
            CategoryCode: "101",
            CategorySubCode: "01",
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: 1
          }],
          TrnReference: "2972c862-2e43-48b9-b846-af665a",
          RateConfirmation: "N",
          TotalForeignValue: 1200,
          ValueDate: "2019-05-05",
          FlowCurrency: "USD"
        },
        customData: {
          docList: [],
          edited: true,
          TotalDomesticAmount: 40000,
          inZAR: true
        }
      }
  },
  {
    name: "IBR valid transaction (ZAR)",
      data: {
        transaction: {
          Version: "FINSURV",
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: {
            Entity: {
              AccountIdentifier: "RESIDENT OTHER",
              InstitutionalSector: "02",
              IndustrialClassification: "02",
              AccountNumber: "12341233",
              EntityName: "asdasd asdsad",
              TradingName: "asd sad sa d",
              RegistrationNumber: "asd as ads d sa",
              StreetAddress: {
                AddressLine1: "sadasd",
                Suburb: "asdads",
                City: "asdsad",
                Province: "GAUTENG",
                PostalCode: "1234"
              },
              PostalAddress: {
                AddressLine1: "qweqwe",
                Suburb: "qweewq eqw",
                City: "qweqwe",
                PostalCode: "1234",
                Province: "NORTH WEST"
              },
              ContactDetails: {
                Email: "asdasd@asddas.as",
                ContactSurname: "asdasd",
                ContactName: "asdasddsa"
              },
              AccountName: "asfasf afafs af"
            },
            Type: "Entity"
          },
          NonResident: {
            Entity: {
              AccountIdentifier: "NON RESIDENT OTHER",
              Address: {
                Country: "BR"
              },
              EntityName: "asfafs",
              AccountNumber: "124124124"
            },
            Type: "Entity"
          },
          MonetaryAmount: [{
            CategoryCode: "101",
            CategorySubCode: "01",
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: 1
          }],
          TrnReference: "2972c862-2e43-48b9-b846-af665a",
          RateConfirmation: "N",
          TotalForeignValue: 1200,
          ValueDate: "2019-05-05",
          FlowCurrency: "USD"
        },
        customData: {
          docList: [],
          edited: true,
          TotalDomesticAmount: 40000,
          inZAR: true
        }
      }
  },
  {
    name : "ExceptionName issue",
    data: {
      State: "End",
      customData: {
        DealerType: "AD"
      },
      transaction: {
        FlowCurrency: "USD",
        MonetaryAmount: [
          {
            CategoryCode: "256",
            Description: "Travel services for residents - holiday travel",
            Category: "256",
            IECurrencyCode: "USD",
            LocationCountry: "AE",
            ForeignCurrencyCode: "USD",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "1000",
            DomesticCurrencyCode: "NAD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000.00",
        Report: {
          DomesticCurrency: "ZAR"
        },
        TrnReference: "FINNA3105195808C",
        ReportingQualifier: "BOPCUS",
        ReplacementTransaction: "N",
        NonResident: {
          Exception: {
            ExceptionName: "BULK BANK CHARGES"
          }
        },
        Flow: "IN",
        Version: "FINSURV",
        ValueDate: "2019-05-31",
        Resident: {
          Description: "XXXXXX, XXXXXX",
          Individual: {
            DateOfBirth: "1993-02-26",
            ForeignIDCountry: "",
            Gender: "M",
            ForeignIDNumber: "",
            Name: "XXXXXX",
            PostalAddress: {
              AddressLine2: "",
              AddressLine1: "PO BOX 621",
              Suburb: "nautilus",
              PostalCode: "99999",
              City: "NA",
              Province: "CAPRIVI"
            },
            TempResPermitNumber: "",
            StreetAddress: {
              AddressLine2: "",
              AddressLine1: "PO BOX 621",
              Suburb: "nautilus",
              PostalCode: "99999",
              City: "NA",
              Province: "CAPRIVI"
            },
            ContactDetails: {
              ContactSurname: "XXXXXX",
              Email: "emailid@stanbic.com",
              Telephone: "1234500000",
              ContactName: "XXXXXX"
            },
            AccountIdentifier: "RESIDENT OTHER",
            PassportNumber: "93022600518",
            Surname: "XXXXXX",
            PassportCountry: "NA",
            IDNumber: "93022600518",
            AccountName: "XXXXXX"
          }
        },
        BranchName: "OKAHANDJA",
        ReplacementOriginalReference: "",
        BranchCode: "08427300"
      }
    }
  },
  {
    name: "Sequence Issue",
    data: {
      customData: {
        DealerType: "AD"
      },
      transaction: {
        FlowCurrency: "NAD",
        MonetaryAmount: [
          {
            CategoryCode: "101",
            Description: "Import advance payment - capital goods",
            Category: "101/02",
            IECurrencyCode: "NAD",
            ImportExport: [
              {
                ImportControlNumber: "",
                PaymentCurrencyCode: "NAD",
                SubsequenceNumber: "1",
                PaymentValue: "101.02"
              }
            ],
            LocationCountry: "US",
            MoneyTransferAgentIndicator: "AD",
            DomesticValue: "101.02",
            CategorySubCode: "02",
            DomesticCurrencyCode: "NAD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "101.02",
        TrnReference: "20190620NAMRD001",
        ReportingQualifier: "BOPCUS",
        ReplacementTransaction: "N",
        NonResident: {
          Individual: {
            Address: {
              AddressLine2: "",
              AddressLine1: "",
              Suburb: "",
              State: "",
              PostalCode: "",
              Country: "ZA"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            PassportNumber: "",
            Gender: "",
            Surname: "leng",
            Name: "anna",
            PassportCountry: "",
            AccountNumber: "387700307"
          }
        },
        Flow: "OUT",
        Version: "FINSURV",
        ValueDate: "2019-06-20",
        Resident: {
          Description: "XXXXXX, XXXXXX",
          Individual: {
            DateOfBirth: "1985-07-16",
            ForeignIDCountry: "",
            Gender: "F",
            ForeignIDNumber: "",
            AccountNumber: "249041278",
            Name: "XXXXXX",
            PostalAddress: {
              AddressLine1: "po box 2005",
              City: "walvis bay",
              Province: "KHOMAS"
            },
            TempResPermitNumber: "",
            StreetAddress: {
              AddressLine2: "",
              AddressLine1: "BOX 2005 WALVIS  BAY",
              Suburb: "kuisebmond",
              PostalCode: "9999",
              City: "NA",
              Province: "KHOMAS"
            },
            ContactDetails: {
              ContactSurname: "XXXXXX",
              Email: "",
              Telephone: "1234567890",
              ContactName: "XXXXXX"
            },
            CustomsClientNumber: "1234",
            AccountIdentifier: "RESIDENT OTHER",
            PassportNumber: "85071610094",
            Surname: "XXXXXX",
            PassportCountry: "NA",
            IDNumber: "85071610094",
            AccountName: "XXXXXX"
          }
        },
        BranchName: "ONDANGWA",
        BranchCode: "00597200"
      }
    }
  },
  {
    name: "Future dated issue",
    data: {
      State: "Incomplete",
      customData: {
        DealerType: "AD"
      },
      transaction: {
        FlowCurrency: "EUR",
        MonetaryAmount: [
          {
            CategoryCode: "256",
            Description: "Travel services for residents - holiday travel",
            Category: "256",
            IECurrencyCode: "EUR",
            LocationCountry: "US",
            ForeignCurrencyCode: "EUR",
            MoneyTransferAgentIndicator: "AD",
            ForeignValue: "200",
            DomesticCurrencyCode: "NAD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "200.00",
        Report: {
          DomesticCurrency: "ZAR"
        },
        TrnReference: "FINNA0407198995D",
        ReportingQualifier: "BOPCUS",
        ReplacementTransaction: "N",
        NonResident: {
          Exception: {
            ExceptionName: "MUTUAL PARTY"
          }
        },
        Flow: "OUT",
        Version: "FINSURV",
        ValueDate: "2019-07-04",
        Resident: {
          Description: "chuma, mwala",
          Individual: {
            DateOfBirth: "2019-07-24",
            PostalAddress: {
              AddressLine1: "choto",
              City: "choto",
              Province: "CAPRIVI"
            },
            StreetAddress: {
              AddressLine1: "choto",
              Suburb: "choto",
              City: "choto",
              Province: "CAPRIVI"
            },
            ContactDetails: {
              ContactSurname: "mwala",
              Telephone: "0814839146",
              ContactName: "chuma"
            },
            AccountIdentifier: "CASH",
            PassportNumber: "9310",
            Gender: "F",
            Surname: "mwala",
            Name: "chuma",
            PassportCountry: "US",
            IDNumber: "93102200427"
          }
        },
        BranchName: "RUNDU",
        ReplacementOriginalReference: "",
        BranchCode: "08517300"
      }
    }
  },
  {
    name: "Hidden mfield issue IBR",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            AccountName: "SIT ONE",
            AccountNumber: "302127720",
            TaxClearanceCertificateIndicator: "N",
            AccountIdentifier: "RESIDENT OTHER",
            EntityName: "TESTINGTOKEN",
            TradingName: "TESTINGTOKEN",
            RegistrationNumber: "2000/381021/07",
            VATNumber: "1234567890",
            IndustrialClassification: "10",
            InstitutionalSector: "04",
            StreetAddress: {
              AddressLine1: "52 Anderson St ",
              Suburb: "Marshalltown",
              City: "Johannesburg",
              Province: "GAUTENG",
              PostalCode: "2001",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "52 Anderson St ",
              Suburb: "Marshalltown",
              City: "Johannesburg",
              Province: "GAUTENG",
              PostalCode: "2001",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "Milanzi",
              ContactName: "Upile",
              Email: "1.upile@gmail.com",
              Telephone: "0842942915"
            }
          },
          Type: "Entity"
        },
        NonResident: {
          Entity: {
            Address: {
              AddressLine1: "kjdskjdsf",
              AddressLine2: "kjdskjdsf",
              City: "kdsfjdskfj",
              Country: "US"
            },
            type: "ENTITY",
            AccountIdentifier: "NON RESIDENT OTHER",
            EntityName: "dssjfsdjf",
            ReceivingCountry: "US",
            AccountNumber: "123456"
          },
          Type: "Entity"
        },
        MonetaryAmount: [{
          CategoryCode: "101",
          SARBAuth: {},
          TravelMode: {},
          ThirdParty: {},
          ForeignValue: "300.00",
          RandValue: 4283.46,
          TotalDomesticAmount: 4283.46,
          isZAR: false,
          allowance: "N",
          MoneyTransferAgentIndicator: "AD",
          LoanInterest: {},
          SequenceNumber: "1",
          CategorySubCode: "01",
          ImportExport: [{
            SubsequenceNumber: "1",
            PaymentValue: "300.00",
            PaymentCurrencyCode: "USD",
            ImportControlNumber: "INV"
          }]
        }],
        FlowCurrency: "USD",
        TotalForeignValue: "300",
        ValueDate: "2019-07-10",
        AccountHolderStatus: "Non South African Resident",
        OriginatingCountry: "ZA",
        LocationCountry: "US",
        ReceivingCountry: "US",
        CounterpartyStatus: "Non Resident",
        PaymentDetail: {
          BeneficiaryBank: {
            SWIFTBIC: "SCBLUS33"
          }
        },
        TrnReference: "75538517-113d-489a-b695-9602ae",
        TransactionCurrency: "USD",
        RateConfirmation: "N"
      },
      customData: {
        DealerType: "AD"
        
      }
    }
  },
  {
    name: "TP Address line 1 issue",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            AccountNumber: "41857844",
            TaxClearanceCertificateIndicator: "N",
            AccountIdentifier: "RESIDENT OTHER",
            EntityName: "companyName",
            TradingName: "companyName",
            RegistrationNumber: "1234567890",
            VATNumber: "1051234656",
            IndustrialClassification: "05",
            InstitutionalSector: "04",
            StreetAddress: {
              AddressLine1: "5 Klaarwater Rd ",
              Suburb: "Savannah Park",
              City: "Pinetown",
              Province: "KWAZULU NATAL",
              PostalCode: "3610",
              Country: "ZA"
            },
            PostalAddress: {
              AddressLine1: "5 Klaarwater Rd ",
              Suburb: "Savannah Park",
              City: "Pinetown",
              Province: "KWAZULU NATAL",
              PostalCode: "3610",
              Country: "ZA"
            },
            ContactDetails: {
              ContactSurname: "Sokhele",
              ContactName: "Mandla",
              Email: "mathapelo.molefe@standardbank.co.za",
              Telephone: "0008253485"
            }
          },
          Type: "Entity"
        },
        NonResident: {
          Entity: {
            Address: {
              AddressLine1: "No 15 Adekunle Fajuyi way",
              AddressLine2: "GRA, Ikeja",
              City: "Lagos",
              Country: "NG"
            },
            type: "ENTITY",
            AccountIdentifier: "NON RESIDENT OTHER",
            EntityName: "The Gold Company",
            ReceivingCountry: "NG",
            AccountNumber: 1234567890
          },
          Type: "Entity"
        },
        MonetaryAmount: [{
          CategoryCode: "106",
          SARBAuth: {},
          TravelMode: {},
          ForeignValue: "300",
          allowance: "SDA",
          DomesticValue: "4912.74",
          MoneyTransferAgentIndicator: "AD",
          LoanInterest: {},
          SequenceNumber: 1,
          AdHocRequirement: {
            Subject: "SDA",
            Description: "Single Discretionary Allowance"
          },
          ThirdPartyKind: "Individual",
          ThirdParty: {
            Individual: {},
            StreetAddress: {
              AddressLine2: "",
              AddressLine1: ""
            }
          }
        }],
        FlowCurrency: "USD",
        TotalForeignValue: "300",
        ValueDate: "2019-07-14",
        AccountHolderStatus: "Non South African Resident",
        OriginatingCountry: "ZA",
        LocationCountry: "NG",
        ReceivingCountry: "NG",
        CounterpartyStatus: "Non Resident",
        PaymentDetail: {
          BeneficiaryBank: {
            SWIFTBIC: "GTBINGLA"
          }
        },
        TrnReference: "9a03a76c-acf7-4fcd-a270-ac6423",
        TransactionCurrency: "USD",
        RateConfirmation: "N"
      },
      customData: {
        DealerType: "AD",
        msgHide: false,
        inZAR: false,
        TotalDomesticAmount: 4912.74,
        docList: [],
        edited: true,
        isValid: false,
        LUClient: "Y",
        side:"DEBIT",
        decisions: [
        { 
          accStatusFilter: "Individual", 
          manualSection: "Manual Section B2 - B (ii) (for OUT) (investment funds)", 
          reportable: "REPORTABLE", 
          flow: "OUT", 
          reportingSide: "DEBIT", 
          nonResSide: "CREDIT", 
          nonResAccountType: "FCA RESIDENT", 
          resSide: "DEBIT", 
          resAccountType: "RESIDENT OTHER", 
          possibleDrAccountTypes: ["LOCAL_ACC"], 
          possibleCrAccountTypes: ["FCA"], 
          category: ["513"] 
        },
        { 
          manualSection: "B and T Section B.1 (A) - page 1 (non investment funds)", 
          reportable: "ZZ1REPORTABLE", 
          flow: "OUT", 
          reportingSide: "DEBIT", 
          nonResException: ["FCA RESIDENT NON REPORTABLE"], 
          resSide: "DEBIT", 
          resAccountType: "RESIDENT OTHER", 
          possibleDrAccountTypes: ["LOCAL_ACC"], 
          possibleCrAccountTypes: ["FCA"], 
          notCategory: ["513"] 
        }, 
        { 
          reportable: "NONREPORTABLE", 
          reportingSide: "CREDIT" 
        }]
      }
    }
  },
  {
    name: "NBOL special dispensation",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "John",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "M",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [],
        TotalForeignValue: "1000",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      },
      customData: {
        MoneyTransferAgentIndicator: "ACE",
        dispensation: [
          {
            category: "101/04",
            AuthIssuer: "024",
            SARBAuthRefNumber: "sdfdsfsd",
            SARBAuthAppNumber: "asdsfsda"
          },
          {
            category: "102/01",
            CustomsClientNumber: "abcdef",
            SectionOfRulings: "b(100)"
          },
          {
            category: "104/03",
            CustomsClientNumber: "abcdef",
            AuthIssuer: "016",
            ADInternalAuthNumberDate: "2016-10-12",
            ADInternalAuthNumber: "150000028"
          }
        ]
      }
    }
  },
  {
    name: "NBOL Test",
    data: {
      customData: {
        allowAdhocBOPThirdPartyCapture: "true",
        allowSaveOfAdhocThirdParty: "true"
      },
      transaction: {
        ValueDate : "2020-01-15",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@MAIL.COM",
              Fax: "2761360578",
              Telephone: "2613605978"
            },
            Name: "Name1",
            Surname: "Surname1",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4310183340",
            Gender: "F",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "US"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "401",
            ForeignValue: "1000",
            LocationCountry: "US",
            ThirdParties: "Name1, Surname1",
            ThirdParty: {
              Individual: {
                Surname: "Surname1",
                Name: "Name1",
                Gender: "M",
                DateOfBirth: "2006-05-04",
                IDNumber: "8902205150088",
                PassportNumber: "123123123",
                PassportCountry: "ZA"
              },
              TaxNumber: "0254/089/06/3",
              VATNumber: "4310183340",
              CustomsClientNumber: "00034786",
              StreetAddress: {
                AddressLine1: "AddressLine10",
                AddressLine2: "AddressLine20",
                Suburb: "Suburb0",
                City: "City0",
                PostalCode: "1244",
                Province: "GAUTENG"
              },
              PostalAddress: {
                AddressLine1: "AddressLine11",
                AddressLine2: "AddressLine21",
                Suburb: "Suburb1",
                City: "City1",
                PostalCode: "1234",
                Province: "GAUTENG"
              },
              ContactDetails: {
                ContactName: "ContactName0",
                ContactSurname: "ContactSurname0",
                Email: "Email@mailcom",
                Fax: "0111234567",
                Telephone: "0111234567"
              }
            },
            Category: "401",
            SARBAuth: { AuthIssuer: "16" },
            MoneyTransferAgentIndicator: "WESTERNUNION",
            SequenceNumber: "1",
            allowAdhocBOPThirdParty: "true"
          }
        ],
        TotalForeignValue: "1000",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      },
    }
  },
  {
    name: "simple valid NBOL test",
    data: {
      customData: {
        allowAdhocBOPThirdParty: "true",
        allowAdhocBOPThirdPartyCapture: "true",
        allowSaveOfAdhocThirdParty: "true"
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "Uma-Looma",
            Surname: "Johnson",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "F",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG",
              State: "Adamawa"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson",
            Gender: "M",
            PassportNumber: "665565665",
            PassportCountry: "US",
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            Category: "401",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "8888",
              SARBAuthRefNumber: "2020-12345678"
            },
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "1000",
        ValueDate: "2020-01-16",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CorrespondentBank: "CHASUSU0XXX",
        CorrespondentCountry: "US",
      },
      customData: {  }
    }
  },
  {
    name: "LIBRA Custom Data",
    data: {
      customData: {
        DealerType: "AD",
        msgHide: false,
        inZAR: false,
        TotalDomesticAmount: 4912.74,
        docList: [],
        edited: true,
        isValid: false,
        LUClient: "Y",
        side:"DEBIT",
        decisions: [
        { 
          accStatusFilter: "Individual", 
          manualSection: "Manual Section B2 - B (ii) (for OUT) (investment funds)", 
          reportable: "REPORTABLE", 
          flow: "IN", 
          reportingSide: "CREDIT", 
          nonResSide: "CREDIT", 
          nonResAccountType: "FCA RESIDENT", 
          resSide: "DEBIT", 
          resAccountType: "RESIDENT OTHER", 
          possibleDrAccountTypes: ["LOCAL_ACC"], 
          possibleCrAccountTypes: ["FCA"], 
          category: [] 
        },
        { 
          accStatusFilter: "Individual", 
          manualSection: "Manual Section B2 - B (ii) (for OUT) (investment funds)", 
          reportable: "REPORTABLE", 
          flow: "OUT", 
          reportingSide: "DEBIT", 
          nonResSide: "CREDIT", 
          nonResAccountType: "FCA RESIDENT", 
          resSide: "DEBIT", 
          resAccountType: "RESIDENT OTHER", 
          possibleDrAccountTypes: ["LOCAL_ACC"], 
          possibleCrAccountTypes: ["FCA"], 
          category: ["100"] 
        },
        { 
          manualSection: "B and T Section B.1 (A) - page 1 (non investment funds)", 
          reportable: "ZZ1REPORTABLE", 
          flow: "OUT", 
          reportingSide: "DEBIT", 
          nonResException: ["FCA RESIDENT NON REPORTABLE"], 
          resSide: "DEBIT", 
          resAccountType: "RESIDENT OTHER", 
          possibleDrAccountTypes: ["LOCAL_ACC"], 
          possibleCrAccountTypes: ["FCA"], 
          notCategory: ["513"] 
        }, 
        { 
          reportable: "NONREPORTABLE", 
          reportingSide: "CREDIT" 
        }]
      },
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {

        },
        NonResident: {

        },
        MonetaryAmount: [
          {
            CategoryCode: "407",
            ForeignValue: "1000",
            LocationCountry: "US",
            Category: "401",
            SARBAuth: {
              AuthIssuer: "024",
              SARBAuthAppNumber: "8888",
              SARBAuthRefNumber: "2020-12345678"
            },
            SequenceNumber: "1"
          }   
        ],
        TotalForeignValue: "1000",
        ValueDate: "2020-01-16",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CorrespondentBank: "CHASUSU0XXX",
        CorrespondentCountry: "US"
      }
    }
  },
  {
    name: "TradeSuite Set 1",
    data: {
      customData: {
        PaymentType: "OPEN_ACCOUNT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "Uma-Looma",
            Surname: "Johnson",
            CustomsClientNumber:"00034786",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "F",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "103",
            ForeignValue: "500",
            RandValue: "200",
            LocationCountry: "US",
            CategorySubCode: "01",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "",
                CustomsClientNumber: "64562345",
                SubsequenceNumber: "1",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "16",
              ADInternalAuthNumberDate: "2016-01-06",
              ADInternalAuthNumber: "12341234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CCNResponse: {
          status: "warning",
          errorCode: "100",
          errorMessage: "Warning."
        }
      }
    }
  },
  {
    name: "TradeSuite Set 2",
    data: {
      transaction: {
          Version: "FINSURV",
          ReportingQualifier: "BOPCUS",
          Flow: "OUT",
          Resident: {
              Entity: {
                  VATNumber: "4920114990",
                  InstitutionalSector: "null",
                  EntityName: "Bonpak",
                  IndustrialClassification: "null",
                  AccountNumber: "43636363636",
                  StreetAddress: {
                      AddressLine1: "undefined",
                      AddressLine2: "undefined",
                      Suburb: "undefined",
                      PostalCode: "undefined",
                      City: "undefined",
                      Province: "undefined"
                  },
                  ContactDetails: {
                      ContactSurname: "Kamaraju",
                      Email: "divyakamaraju238+authorizer@gmail.com",
                      Telephone: "+27833833466",
                      ContactName: "Divya",
                      Fax: "+27833833466"
                  },
                  AccountIdentifier: "RESIDENT OTHER",
                  TradingName: "Bonpak",
                  RegistrationNumber: "2003/005527/07",
                  TaxNumber: "9257330847",
                  CustomsClientNumber: "00044632"
              },
              Type: "Entity"
          },
          NonResident: {
              Entity: {
                  Address: {
                      AddressLine2: "NO1299,YINXIAN AVE 17f",
                      AddressLine1: "NO1299,YINXIAN AVE 17f",
                      Suburb: "South building Yinxian Avenue",
                      PostalCode: "undefined",
                      Country: "HK",
                      City: "Hong Kong Central"
                  },
                  AccountIdentifier: "NON RESIDENT OTHER",
                  AccountNumber: "12345678",
                  Name: "Ningbo Subir Commodity co, ltd"
              },
              Type: "Entity"
          },
          MonetaryAmount: [{
                  CategoryCode: "104",
                  ImportExport: [{
                          PaymentCurrencyCode: "HK",
                          ImportControlNumber: "",
                          TransportDocumentNumber: "",
                          SubsequenceNumber: "1"
                      }
                  ],
                  SARBAuth: {},
                  TravelMode: {},
                  ThirdParty: {},
                  CategorySubCode: "02",
                  ForeignValue: "200",
                  LocationCountry: "HK",
                  RegulatorAuth: {
                      AuthFacilitator: "null",
                      AuthIssuer: "null",
                      CBAuthAppNumber: "null",
                      CBAuthRefNumber: "null",
                      REInternalAuthNumber: "null",
                      REInternalAuthNumberDate: "null",
                      REAuthAppNumber: ""
                  },
                  SequenceNumber: "1",
                  CategoryDescription: "Import payment - capital goods (Merchandise)",
                  LoanInterest: {}
              }
          ],
          TotalDomesticValue: "12229",
          ReceivingCountry: "HK",
          FlowCurrency: "USD",
          TotalForeignValue: "200",
          TrnReference: "edc497b7-60c1-40e8-982b-9491e2939ab7",
          OriginatingCountry: "ZA",
          OriginatingBank: "",
          ValueDate: "2020-11-05T10:28:00",
          ReceivingBank: "",
          TransactionCurrency: "USD",
          RateConfirmation: "N"
      },
      customData: {
          PaymentType: "OPEN_ACCOUNT",
          DealerType: "AD",
          isValid: false,
          edited: true,
          docList: []
      }
    } 
  },
  {
    name: "TradeSuite Set 3",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
            Entity: {
                VATNumber: "4920114990",
                InstitutionalSector: "null",
                EntityName: "Bonpak",
                IndustrialClassification: "null",
                AccountNumber: "43636363636",
                StreetAddress: {
                    AddressLine1: "undefined",
                    AddressLine2: "undefined",
                    Suburb: "undefined",
                    PostalCode: "undefined",
                    City: "undefined",
                    Province: "undefined"
                },
                ContactDetails: {
                    ContactSurname: "Kamaraju",
                    Email: "divyakamaraju238+authorizer@gmail.com",
                    Telephone: "+27833833466",
                    ContactName: "Divya",
                    Fax: "+27833833466"
                },
                AccountIdentifier: "RESIDENT OTHER",
                TradingName: "Bonpak",
                RegistrationNumber: "2003/005527/07",
                TaxNumber: "9257330847",
                CustomsClientNumber: "0004463245"
            },
            Type: "Entity"
        },
        NonResident: {
            Entity: {
                Address: {
                    AddressLine2: "NO1299,YINXIAN AVE 17f",
                    AddressLine1: "NO1299,YINXIAN AVE 17f",
                    Suburb: "South building Yinxian Avenue",
                    PostalCode: "undefined",
                    Country: "HK",
                    City: "Hong Kong Central"
                },
                AccountIdentifier: "NON RESIDENT OTHER",
                AccountNumber: "12345678",
                Name: "Ningbo Subir Commodity co, ltd"
            },
            Type: "Entity"
        },
        MonetaryAmount: [{
                CategoryCode: "103",
                ImportExport: [{
                        PaymentCurrencyCode: "HK",
                        ImportControlNumber: "DBN200101011234567",
                        TransportDocumentNumber: "1234567890",
                        SubsequenceNumber: "1"
                    }
                ],
                SARBAuth: {},
                TravelMode: {},
                ThirdParty: {},
                CategorySubCode: "02",
                ForeignValue: "200",
                LocationCountry: "HK",
                RegulatorAuth: {
                    AuthFacilitator: "null",
                    AuthIssuer: "null",
                    CBAuthAppNumber: "null",
                    CBAuthRefNumber: "null",
                    REInternalAuthNumber: "null",
                    REInternalAuthNumberDate: "null",
                    REAuthAppNumber: ""
                },
                SequenceNumber: "1",
                CategoryDescription: "Import payment - capital goods (Merchandise)",
                LoanInterest: {}
            }
        ],
        TotalDomesticValue: "12229",
        ReceivingCountry: "HK",
        FlowCurrency: "USD",
        TotalForeignValue: "200",
        TrnReference: "edc497b7-60c1-40e8-982b-9491e2939ab7",
        OriginatingCountry: "ZA",
        OriginatingBank: "",
        ValueDate: "2020-11-05T10:28:00",
        ReceivingBank: "",
        TransactionCurrency: "USD",
        RateConfirmation: "N"
      },
      customData: {
          PaymentType: "OPEN_ACCOUNT",
          DealerType: "AD",
          isValid: false,
          edited: true,
          docList: []
      }
    }
  },
  {
    name: "TradeSuite Set 4",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
            Entity: {
                VATNumber: "4920114990",
                InstitutionalSector: "null",
                EntityName: "Bonpak",
                IndustrialClassification: "null",
                AccountNumber: "43636363636",
                StreetAddress: {
                    AddressLine1: "undefined",
                    AddressLine2: "undefined",
                    Suburb: "undefined",
                    PostalCode: "undefined",
                    City: "undefined",
                    Province: "undefined"
                },
                ContactDetails: {
                    ContactSurname: "Kamaraju",
                    Email: "divyakamaraju238+authorizer@gmail.com",
                    Telephone: "+27833833466",
                    ContactName: "Divya",
                    Fax: "+27833833466"
                },
                AccountIdentifier: "RESIDENT OTHER",
                TradingName: "Bonpak",
                RegistrationNumber: "2003/005527/07",
                TaxNumber: "9257330847",
                CustomsClientNumber: "0004463245"
            },
            Type: "Entity"
        },
        NonResident: {
            Entity: {
                Address: {
                    AddressLine2: "NO1299,YINXIAN AVE 17f",
                    AddressLine1: "NO1299,YINXIAN AVE 17f",
                    Suburb: "South building Yinxian Avenue",
                    PostalCode: "undefined",
                    Country: "HK",
                    City: "Hong Kong Central"
                },
                AccountIdentifier: "NON RESIDENT OTHER",
                AccountNumber: "12345678",
                Name: "Ningbo Subir Commodity co, ltd"
            },
            Type: "Entity"
        },
        MonetaryAmount: [{
                CategoryCode: "101",
                ImportExport: [{
                        PaymentCurrencyCode: "HK",
                        ImportControlNumber: "INVJP2910",
                        TransportDocumentNumber: "",
                        SubsequenceNumber: "1"
                    }
                ],
                SARBAuth: {},
                TravelMode: {},
                ThirdParty: {},
                CategorySubCode: "01",
                ForeignValue: "1200",
                LocationCountry: "HK",
                RegulatorAuth: {
                    AuthFacilitator: "null",
                    AuthIssuer: "null",
                    CBAuthAppNumber: "null",
                    CBAuthRefNumber: "null",
                    REInternalAuthNumber: "null",
                    REInternalAuthNumberDate: "null",
                    REAuthAppNumber: ""
                },
                SequenceNumber: "1",
                CategoryDescription: "Import advance payment (Merchandise)",
                LoanInterest: {}
            }
        ],
        TotalDomesticValue: "12229",
        ReceivingCountry: "HK",
        FlowCurrency: "USD",
        TotalForeignValue: "1200",
        TrnReference: "4dda08bc-a81d-4d9f-a744-d9e7da1fb1cf",
        OriginatingCountry: "ZA",
        OriginatingBank: "",
        ValueDate: "2020-11-06T14:04:00",
        ReceivingBank: "",
        TransactionCurrency: "USD",
        RateConfirmation: "N"
      },
      customData: {
          PaymentType: "ADVANCE_PAYMENT",
          DealerType: "AD",
          isValid: false,
          edited: true,
          docList: []
      }
    }
  },
  {
    name: "TradeSuite Set 5",
    data: {
      transaction: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
            Entity: {
                VATNumber: "4920114990",
                InstitutionalSector: "null",
                EntityName: "Bonpak",
                IndustrialClassification: "null",
                AccountNumber: "43636363636",
                StreetAddress: {
                    AddressLine1: "undefined",
                    AddressLine2: "undefined",
                    Suburb: "undefined",
                    PostalCode: "undefined",
                    City: "undefined",
                    Province: "undefined"
                },
                ContactDetails: {
                    ContactSurname: "Kamaraju",
                    Email: "divyakamaraju238+authorizer@gmail.com",
                    Telephone: "+27833833466",
                    ContactName: "Divya",
                    Fax: "+27833833466"
                },
                AccountIdentifier: "RESIDENT OTHER",
                TradingName: "Bonpak",
                RegistrationNumber: "2003/005527/07",
                TaxNumber: "9257330847",
                CustomsClientNumber: "00044632"
            },
            Type: "Entity"
        },
        NonResident: {
            Entity: {
                Address: {
                    AddressLine2: "NO1299,YINXIAN AVE 17f",
                    AddressLine1: "NO1299,YINXIAN AVE 17f",
                    Suburb: "South building Yinxian Avenue",
                    PostalCode: "undefined",
                    Country: "HK",
                    City: "Hong Kong Central"
                },
                AccountIdentifier: "NON RESIDENT OTHER",
                AccountNumber: "12345678",
                Name: "Ningbo Subir Commodity co, ltd"
            },
            Type: "Entity"
        },
        MonetaryAmount: [{
                CategoryCode: "102",
                ImportExport: [{
                        PaymentCurrencyCode: "HK",
                        ImportControlNumber: "INVJP2910",
                        TransportDocumentNumber: "",
                        SubsequenceNumber: "1"
                    }
                ],
                SARBAuth: {},
                TravelMode: {},
                ThirdParty: {},
                CategorySubCode: "02",
                ForeignValue: "1200",
                LocationCountry: "HK",
                RegulatorAuth: {
                    AuthFacilitator: "null",
                    AuthIssuer: "null",
                    CBAuthAppNumber: "null",
                    CBAuthRefNumber: "null",
                    REInternalAuthNumber: "null",
                    REInternalAuthNumberDate: "null",
                    REAuthAppNumber: ""
                },
                SequenceNumber: "1",
                CategoryDescription: "Import advance payment i.t.o. import undertaking - capital goods (Merchandise)",
                LoanInterest: {}
            }
        ],
        TotalDomesticValue: "12229",
        ReceivingCountry: "HK",
        FlowCurrency: "USD",
        TotalForeignValue: "1200",
        TrnReference: "4dda08bc-a81d-4d9f-a744-d9e7da1fb1cf",
        OriginatingCountry: "ZA",
        OriginatingBank: "",
        ValueDate: "2020-11-06T14:04:00",
        ReceivingBank: "",
        TransactionCurrency: "USD",
        RateConfirmation: "N"
      },
      customData: {
          PaymentType: "ADVANCE_PAYMENT",
          DealerType: "AD",
          isValid: false,
          edited: true,
          docList: []
      }
    }
  },
  {
    name: "TradeSuite Set 6",
    data: {
      customData: {
		  PaymentType: "ADVANCE_PAYMENT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "Uma-Looma",
            CustomsClientNumber:"00012345",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "12345678",
            VATNumber: "12345786"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "",
            ForeignValue: "500",
            RandValue: "200",
            LocationCountry: "US",
            CategorySubCode: "",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "INV2001010",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                SubsequenceNumber: "1",
                IVSResponse: { status: "pass" }
              }
            ],
            RegulatorAuth: {
              ADInternalAuthNumber: "123456"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CCNResponse: {
          status: "warning",
          errorCode: "100",
          errorMessage: "Warning."
        }
      }
    }
  },
  {
    name: "TradeSuite BALANCE PAYMENT",
    data: {
      customData: {
        PaymentType: "BALANCE_PAYMENT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            PostalAddress: {

            },
            StreetAddress: {

            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660",
            TaxNumber: "9700/156/71/5",
            VATNumber: "4090103146"

          }
        },
        NonResident: {
          Individual: {
            Address: {

            },
          }
        },
        MonetaryAmount: [
          {
            ForeignValue: "700",
            DomesticValue: "12229",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        TotalDomesticValue: "12229",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "TradeSuite OPEN ACCOUNT",
    data: {
      customData: {
        PaymentType: "OPEN_ACCOUNT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            PostalAddress: {

            },
            StreetAddress: {

            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660",
            TaxNumber: "9700/156/71/5",
            VATNumber: "4090103146"

          }
        },
        NonResident: {
          Individual: {
            Address: {

            },
          }
        },
        MonetaryAmount: [
          {
            ForeignValue: "700",
            DomesticValue: "12229",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        TotalDomesticValue: "12229",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "TradeSuite ADVANCE PAYMENT",
    data: {
      customData: {
        PaymentType: "ADVANCE_PAYMENT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            PostalAddress: {

            },
            StreetAddress: {

            },
            EntityName: "ACME BK OF ZA LTD",
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            AccountIdentifier: "RESIDENT OTHER",
            IndustrialClassification: "08",
            InstitutionalSector: "01",
            TradingName: "ACME BK OF ZA LTD",
            RegistrationNumber: "196200073806",
            AccountName: "CONTRA MTSS TREAS DIV",
            AccountNumber: "9864660",
            TaxNumber: "9700/156/71/5",
            VATNumber: "4090103146"

          }
        },
        NonResident: {
          Individual: {
            Address: {

            },
          }
        },
        MonetaryAmount: [
          {
            ForeignValue: "700",
            DomesticValue: "12229",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        TotalDomesticValue: "12229",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX"
      }
    }
  },
  {
    name: "TradeSuite The SBFA dataset",
    data: {
      customData: {
        PaymentType: "ADVANCE_PAYMENT"
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "OUT",
        Resident: {
          Entity: {
            PostalAddress: {

            },
            StreetAddress: {

            },
            ContactDetails: {
              ContactSurname: "Fudd",
              Email: "elma.fudd@acme.co.za",
              Telephone: "27116365094",
              ContactName: "Elma"
            },
            TaxNumber: "9700/156/71/5",
            VATNumber: "4090103146"

          }
        },
        NonResident: {
          Individual: {
            Address: {

            },
          }
        },
        MonetaryAmount: [
          {
            ForeignValue: "700",
            DomesticValue: "12229",
            LocationCountry: "US",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 700,
                SubsequenceNumber: "1"
              }
            ],
            SARBAuth: {

            },
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        TotalDomesticValue: "12229",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
      }
    }
  },
  {
    name: "Test set non-reportable",
    data: {
      customData: {
      },
      transaction: {
        TrnReference: "TestRef12424",
        Version: "FINSURV",
        ReportingQualifier: "NON REPORTABLE",
        Flow: "OUT",
        Resident: {
          Individual: {
            StreetAddress: {
              AddressLine1: "MNI Towers 2",
              AddressLine2: "11 Jalan Pinang",
              Suburb: "Kuala Lumpur",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            PostalAddress: {
              AddressLine1: "50200",
              AddressLine2: "Kuala Lumpur",
              Suburb: "POSTALSUBURB",
              City: "Johannesburg",
              PostalCode: "2196",
              Province: "GAUTENG"
            },
            ContactDetails: {
              ContactName: "CONTACTNAME",
              ContactSurname: "CONTACTSURNAME",
              Email: "CONTACTEMAIL@GMAIL.COM",
              Fax: "2761360578",
              Telephone: "2761305978"
            },
            Name: "Uma-Looma",
            Surname: "Johnson",
            CustomsClientNumber:"00034786",
            AccountIdentifier: "RESIDENT OTHER",
            AccountName: "Account_NT 1 p",
            AccountNumber: "221495398",
            TaxNumber: "0254/089/06/3",
            VATNumber: "4090103146",
            Gender: "F",
            DateOfBirth: "1955-02-29",
            IDNumber: "5502290001080"
          }
        },
        NonResident: {
          Individual: {
            Address: {
              AddressLine1: "addressline1",
              AddressLine2: "addressline2",
              Suburb: "suurb",
              City: "townname",
              PostalCode: "postalcode",
              Country: "NG"
            },
            AccountIdentifier: "NON RESIDENT OTHER",
            AccountNumber: "9032243812587",
            Name: "Fred",
            Surname: "Georgeson"
          }
        },
        MonetaryAmount: [
          {
            CategoryCode: "",
            ForeignValue: "500",
            RandValue: "200",
            LocationCountry: "US",
            CategorySubCode: "",
            ImportExport: [
              {
                PaymentCurrencyCode: "USD",
                PaymentValue: 200,
                ImportControlNumber: "DBN200101011234567",
                TransportDocumentNumber: "124124",
                CustomsClientNumber: "64562345",
                SubsequenceNumber: "1",
                IVSResponse: { status: "pass" }
              }
            ],
            SARBAuth: {
              AuthIssuer: "16",
              ADInternalAuthNumberDate: "2016-01-06",
              ADInternalAuthNumber: "12341234"
            },
            MoneyTransferAgentIndicator: "AD",
            SequenceNumber: "1"
          }
        ],
        TotalForeignValue: "700",
        ValueDate: "2015-06-26",
        FlowCurrency: "USD",
        OriginatingBank: "SBZAZAJJ",
        OriginatingCountry: "ZA",
        ReceivingCountry: "UG",
        ReceivingBank: "SBICUGKX",
        CCNResponse: {
          status: "warning",
          errorCode: "100",
          errorMessage: "Warning."
        }
      }
    }
  },  
  {
    name: "MTA ADLA",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        TotalDomesticAmount: 199.13,
        DealerType: "ADLA",
      }
    }
  },
  {
    name: "Total Rand Amount - Transaction",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TotalDomesticValue: "1000.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        DealerType: "ADLA",
      }
    }
  },
  {
    name: "Total Rand Amount - Custom",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        TotalDomesticAmount: 1000.00,
        DealerType: "ADLA",
      }
    }
  },
  {
    name: "Total Calculated Rand Amount - Custom",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        TotalCalculatedDomesticAmount: 1000.00,
        DealerType: "ADLA",
      }
    }
  },
  {
    name: "Both Total Rand Amounts - Custom",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        TotalCalculatedDomesticAmount: 1000.00,
        TotalDomesticAmount: 1000.00,
        DealerType: "ADLA",
      }
    }
  },
  {
    name: "All Total Rand Amounts",
    data: {
      transaction: {
        Flow: "OUT",
        BranchName: "MOHALE'SHOEK",
        ValueDate: "2020-08-17",
        Resident: {
            Description: "XXXXXX, XXXXXX",
            Individual: {
                AccountNumber: "9080003139901",
                ForeignIDCountry: "",
                PostalAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                ContactDetails: {
                    ContactSurname: "XXXXXX",
                    Telephone: "1234512245",
                    Email: "",
                    ContactName: "XXXXXX"
                },
                PassportNumber: "076152204720",
                ForeignIDNumber: "",
                Name: "XXXXXX",
                DateOfBirth: "1953-04-08",
                PassportCountry: "LS",
                IDNumber: "0021",
                Gender: "F",
                AccountName: "XXXXXX",
                Surname: "XXXXXX",
                StreetAddress: {
                    PostalCode: "500",
                    Suburb: "hhs",
                    Province: "BUTHA-BUTHE",
                    AddressLine1: "ST JAMES HIGH SCHOOL",
                    City: "LS",
                    AddressLine2: "PO BOX 14"
                },
                TempResPermitNumber: "",
                AccountIdentifier: "RESIDENT OTHER"
            }
        },
        ReplacementOriginalReference: "",
        MonetaryAmount: [
            {
                MoneyTransferAgentIndicator: "ADLA",
                LocationCountry: "US",
                Category: "256",
                Description: "Travel services for residents - holiday travel",
                SARBAuth: {},
                CategoryCode: "256",
                DomesticCurrencyCode: "LSL",
                ImportExport: [],
                ThirdParty: {},
                ForeignValue: "10",
                SequenceNumber: "1",
                TravelMode: {},
                IECurrencyCode: "USD"
            }
        ],
        ReportingQualifier: "BOPCUS",
        TotalForeignValue: "10.00",
        TotalDomesticValue: "1000.00",
        TrnReference: "FINLS170820141D",
        FlowCurrency: "USD",
        BranchCode: "060767",
        NonResident: {
            Exception: {
                ExceptionName: "MUTUAL PARTY"
            }
        },
      },
      customData: {
        TotalCalculatedDomesticAmount: 1000.00,
        TotalDomesticAmount: 1000.00,
        DealerType: "ADLA",
      }
    }
  },


  {
    name: "Third Party IN Flow with Resident as Entity and empty TAX number",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "516",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "mishra",
              Name: "sandeep",
              Gender: "M",
              IDNumber: "33454445",
              TempResPermitNumber: "444444444",
              PassportNumber: "j665565665",
              PassportCountry: "US",
              IDType: "Identification number"
            },
            TaxNumber: "",
            VATNumber: "44444444",
            CustomsClientNumber: "70707070",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "st pauls",
              AddressLine2: "paulshof",
              Suburb: "rodepoort",
              City: "joburg",
              PostalCode: "2191"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "st peters",
              AddressLine2: "paulshof",
              Suburb: "roodepoort",
              City: "joburg",
              PostalCode: "2195"
            },
            ContactDetails: {
              ContactName: "rahul",
              ContactSurname: "das",
              Fax: "0785987812"
            }
          }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ",
    }
  },

  {
    name: "Third Party IN Flow with Resident as Entity and valid TAX number",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "IN",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "02",
          TaxNumber: "1216516",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "516",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "mishra",
              Name: "sandeep",
              Gender: "M",
              IDNumber: "33454445",
              TempResPermitNumber: "444444444",
              PassportNumber: "j665565665",
              PassportCountry: "US",
              IDType: "Identification number"
            },
            TaxNumber: "5656545466",
            VATNumber: "44444444",
            CustomsClientNumber: "70707070",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "st pauls",
              AddressLine2: "paulshof",
              Suburb: "rodepoort",
              City: "joburg",
              PostalCode: "2191"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "st peters",
              AddressLine2: "paulshof",
              Suburb: "roodepoort",
              City: "joburg",
              PostalCode: "2195"
            },
            ContactDetails: {
              ContactName: "rahul",
              ContactSurname: "das",
              Fax: "0785987812"
            }
          }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },

  {
    name: "Third Party IN Flow with Resident as Individual",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "IN",
      Resident: {
        Individual: {
          AccountNumber: "9080003139901",
          ForeignIDCountry: "",
          PostalAddress: {
            PostalCode: "500",
            Suburb: "hhs",
            Province: "BUTHA-BUTHE",
            AddressLine1: "ST JAMES HIGH SCHOOL",
            City: "LS",
            AddressLine2: "PO BOX 14"
          },
          ContactDetails: {
            ContactSurname: "XXXXXX",
            Telephone: "1234512245",
            Email: "",
            ContactName: "XXXXXX"
          },
          PassportNumber: "076152204720",
          ForeignIDNumber: "",
          Name: "XXXXXX",
          DateOfBirth: "1953-04-08",
          PassportCountry: "LS",
          IDNumber: "0021",
          Gender: "F",
          AccountName: "XXXXXX",
          Surname: "XXXXXX",
          StreetAddress: {
            PostalCode: "500",
            Suburb: "hhs",
            Province: "BUTHA-BUTHE",
            AddressLine1: "ST JAMES HIGH SCHOOL",
            City: "LS",
            AddressLine2: "PO BOX 14"
          },
          TempResPermitNumber: "",
          AccountIdentifier: "RESIDENT OTHER"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "516",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "mishra",
              Name: "sandeep",
              Gender: "M",
              IDNumber: "33454445",
              TempResPermitNumber: "444444444",
              PassportNumber: "j665565665",
              PassportCountry: "US",
              IDType: "Identification number"
            },
            TaxNumber: "",
            VATNumber: "44444444",
            CustomsClientNumber: "70707070",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "st pauls",
              AddressLine2: "paulshof",
              Suburb: "rodepoort",
              City: "joburg",
              PostalCode: "2191"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "st peters",
              AddressLine2: "paulshof",
              Suburb: "roodepoort",
              City: "joburg",
              PostalCode: "2195"
            },
            ContactDetails: {
              ContactName: "rahul",
              ContactSurname: "das",
              Fax: "0785987812"
            }
          }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    },
    customData: {
      readOnly: true,
      disabledBopCats: ["101/01"],
      excludedBopCats: ["103"]
    }
  },

  {
    name: "Third Party OUT Flow with Resident as Entity",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Entity: {
          TradingName: "Verma",
          EntityName: "VERMA SVRINDER KUMAR",
          StreetAddress: {
            AddressLine1: "MNI Towers 2",
            AddressLine2: "11 Jalan Pinang",
            Suburb: "Kuala Lumpur",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          PostalAddress: {
            AddressLine1: "50200",
            AddressLine2: "Kuala Lumpur",
            Suburb: "POSTALSUBURB",
            City: "Johannesburg",
            PostalCode: "2196",
            Province: "GAUTENG"
          },
          ContactDetails: {
            ContactName: "CONTACTNAME",
            ContactSurname: "CONTACTSURNAME",
            Email: "CONTACTEMAIL@GMAIL.COM",
            Fax: "2761360578",
            Telephone: "2761305978"
          },
          AccountIdentifier: "RESIDENT OTHER",
          AccountName: "BoP Account 3",
          AccountNumber: "000090452046",
          RegistrationNumber: "12345",
          IndustrialClassification: "03",
          InstitutionalSector: "",
          TaxNumber: "1216516",
          VATNumber: "2468",
          LocationCountry: "US",
          TaxClearanceCertificateIndicator: "N"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "516",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "mishra",
              Name: "sandeep",
              Gender: "M",
              IDNumber: "33454445",
              TempResPermitNumber: "444444444",
              PassportNumber: "j665565665",
              PassportCountry: "US",
              IDType: "Identification number"
            },
            TaxNumber: "",
            VATNumber: "44444444",
            CustomsClientNumber: "70707070",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "st pauls",
              AddressLine2: "paulshof",
              Suburb: "rodepoort",
              City: "joburg",
              PostalCode: "2191"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "st peters",
              AddressLine2: "paulshof",
              Suburb: "roodepoort",
              City: "joburg",
              PostalCode: "2195"
            },
            ContactDetails: {
              ContactName: "rahul",
              ContactSurname: "das",
              Fax: "0785987812"
            }
          }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    }
  },

  {
    name: "Third Party OUT Flow with Individual as Entity",
    data: {
      TrnReference: "TestRef44414",
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      Resident: {
        Individual: {
          AccountNumber: "9080003139901",
          ForeignIDCountry: "",
          PostalAddress: {
            PostalCode: "500",
            Suburb: "hhs",
            Province: "BUTHA-BUTHE",
            AddressLine1: "ST JAMES HIGH SCHOOL",
            City: "LS",
            AddressLine2: "PO BOX 14"
          },
          ContactDetails: {
            ContactSurname: "XXXXXX",
            Telephone: "1234512245",
            Email: "",
            ContactName: "XXXXXX"
          },
          PassportNumber: "076152204720",
          ForeignIDNumber: "",
          Name: "XXXXXX",
          DateOfBirth: "1953-04-08",
          PassportCountry: "LS",
          IDNumber: "0021",
          Gender: "F",
          AccountName: "XXXXXX",
          Surname: "XXXXXX",
          StreetAddress: {
            PostalCode: "500",
            Suburb: "hhs",
            Province: "BUTHA-BUTHE",
            AddressLine1: "ST JAMES HIGH SCHOOL",
            City: "LS",
            AddressLine2: "PO BOX 14"
          },
          TempResPermitNumber: "",
          AccountIdentifier: "RESIDENT OTHER"
        }
      },
      NonResident: {
        Exception: {
          ExceptionName: "CFC RESIDENT NON REPORTABLE"
        }
      },
      MonetaryAmount: [
        {
          CategoryCode: "516",
          ForeignValue: "123",
          MoneyTransferAgentIndicator: "SANLAM",
          SequenceNumber: "1",
          LocationCountry: "US",
          ThirdParty: {
            Individual: {
              Surname: "mishra",
              Name: "sandeep",
              Gender: "M",
              IDNumber: "33454445",
              TempResPermitNumber: "444444444",
              PassportNumber: "j665565665",
              PassportCountry: "US",
              IDType: "Identification number"
            },
            TaxNumber: "",
            VATNumber: "44444444",
            CustomsClientNumber: "70707070",
            StreetAddress: {
              Province: "GAUTENG",
              AddressLine1: "st pauls",
              AddressLine2: "paulshof",
              Suburb: "rodepoort",
              City: "joburg",
              PostalCode: "2191"
            },
            PostalAddress: {
              Province: "GAUTENG",
              AddressLine1: "st peters",
              AddressLine2: "paulshof",
              Suburb: "roodepoort",
              City: "joburg",
              PostalCode: "2195"
            },
            ContactDetails: {
              ContactName: "rahul",
              ContactSurname: "das",
              Fax: "0785987812"
            }
          }
        }
      ],
      TotalForeignValue: "123",
      ValueDate: "2015-07-18",
      FlowCurrency: "ZAR",
      OriginatingBank: "SBZAZAJJ",
      OriginatingCountry: "ZA",
      ReceivingCountry: "ZA",
      ReceivingBank: "SBZAZAJJ"
    },
    customData: {
      disabledBopCats: ["101/01", "101/02", "101/03"],
      excludedBopCats: ["103/01"]
    }
  },
  {
    name: "ITT UCR Number Test",
    customData: {
      DealerType: "AD",
      msgHide: true,
      disabledBopCats: ["100/00", "200/00", "300/00", "400/00", "500/00", "600/00", "700/00", "800/00", "801/00", "802/00", "803/00", "804/00", "810/00", "815/00", "816/00", "817/00", "818/00", "819/00", "830/00", "832/00", "833/00"],
      excludedBopCats: [],
      inZAR: false,
      edited: true,
      LUClient: "N",
      isValid: false,
      docList: [],
      AuthIssuers: [null]
    },
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "IN",
      Resident: {
        Entity: {
          StreetAddress: {
            AddressLine1: "52 ANDERSON ST",
            Suburb: "MARSHALLTOWN",
            City: "JOHANNESBURG",
            Province: "GAUTENG",
            PostalCode: "2001",
            Country: "ZA"
          },
          PostalAddress: {
            AddressLine1: "52 ANDERSON ST",
            Suburb: "MARSHALLTOWN",
            City: "JOHANNESBURG",
            Province: "GAUTENG",
            PostalCode: "2001",
            Country: "ZA"
          },
          ContactDetails: {
            ContactSurname: "ABARASA",
            ContactName: "LEBO",
            Email: "Mathapelo.Molefe@standardbank.co.za",
            Telephone: "0842942915"
          },
          TaxClearanceCertificateIndicator: "N",
          AccountIdentifier: "RESIDENT OTHER",
          EntityName: "BANGS",
          TradingName: "TESTING",
          RegistrationNumber: "2000/381010/07",
          IndustrialClassification: "04",
          TaxNumber: "9257330846",
          VATNumber: "4920114990",
          CustomsClientNumber: "00209250"
        },
        Type: "Entity"
      },
      NonResident: {
          Entity: {
            EntityName: "Shaylen",
            Address: {
              Country: "US"
            }
          },
          Type: "Entity"
      },
      MonetaryAmount: [{
        CategoryCode: "101",
        SARBAuth: {},
        TravelMode: {},
        ThirdParty: {},
        CategorySubCode: "01",
        ForeignValue: "1970",
        MoneyTransferAgentIndicator: "AD",
        allowance: "N",
        SequenceNumber: "1",
      }],
      FlowCurrency: "USD",
      TotalForeignValue: "1970",
      ValueDate: "2021-04-10",
      AccountHolderStatus: "South African Resident",
      TrnReference: "08df7ab2-bc29-419f-8f3a-754a48",
      TransactionCurrency: "USD",
      RateConfirmation: "N"
    }
  },
  {
    name: "Moby Display Rules Tests",
    customData: { PaymentType: 'OPEN_ACCOUNT', DealerType: 'AD' },
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      NADEquivalent: "1234",
      Flow: "OUT",
      Resident: {
          Individual: {
              Surname: "ls",
              Name: "KAS",
              IDNumber: "1212321",
              DateOfBirth: "1991-05-19",
              Gender: "M",
              ContactDetails: {
                  ContactSurname: "LS",
                  Email: "abc@gmail.com",
                  ContactName: "KSS"
              },
              StreetAddress: {
                  AddressLine1: "",
                  Suburb: "",
                  Province: "",
                  City: ""
              },
              PostalAddress: {
                  AddressLine1: "",
                  Suburb: "",
                  City: "",
                  Province: ""
              },
              AccountIdentifier: "CASH"
          },
          Description: "KAS, ls"
      },
      NonResident: {
          Individual: {
              Address: {
                  Country: "AE"
              },
              AccountIdentifier: "CASH",
              Name: "kas",
              Surname: "ls",
              Gender: "M"
          }
      },
      MonetaryAmount: [{
              CategoryCode: "101",
              CategorySubCode: "01",
              ImportExport: [],
              SARBAuth: {},
              TravelMode: {},
              ThirdParty: {},
              ForeignCurrencyCode: "USD",
              SequenceNumber: "1",
              DomesticCurrencyCode: "NAD",
              IECurrencyCode: "USD",
              
              Description: "",
              Category: "",
              ForeignValue: "",
              LocationCountry: "",
              MoneyTransferAgentIndicator: "",
              CBAuth: {
                REInternalAuthNumber: "12345678",
                CBAuthRefNumber: "1234232323"
              }
          }
      ],
      ReplacementTransaction: "N",
      ReplacementOriginalReference: "",
      ValueDate: "2021-04-12",
      TrnReference: "MBYNA1201214554C",
      BranchCode: "060767",
      BranchName: "MOHALE'SHOEK",
      TotalForeignValue: "10",
      FlowCurrency: "USD"
    }
  },
  {
    name: "Moby Display Rules Tests 2",
    customData: {},
    data: {
        Version: "FINSURV",
        ReportingQualifier: "BOPCUS",
        Flow: "IN",
        Resident: {
            Individual: {
                Surname: "ls",
                Name: "KAS",
                IDNumber: "1212321",
                DateOfBirth: "1991-05-19",
                Gender: "M",
                ContactDetails: {
                    ContactSurname: "LS",
                    Email: "abc@gmail.com",
                    ContactName: "KSS"
                },
                StreetAddress: {
                    AddressLine1: "",
                    Suburb: "",
                    Province: "",
                    City: ""
                },
                PostalAddress: {
                    AddressLine1: "",
                    Suburb: "",
                    City: "",
                    Province: ""
                },
                AccountIdentifier: "CASH"
            },
            Description: "KAS, ls"
        },
        NonResident: {
            Individual: {
                Address: {
                    Country: "AE"
                },
                AccountIdentifier: "CASH",
                Name: "kas",
                Surname: "ls",
                Gender: "M"
            }
        },
        MonetaryAmount: [{
                CategoryCode: "101",
                ImportExport: [],
                SARBAuth: {},
                TravelMode: {},
                ThirdParty: {},
                ForeignCurrencyCode: "USD",
                SequenceNumber: "1",
                DomesticCurrencyCode: "NAD",
                IECurrencyCode: "USD",
                CategorySubCode: "01",
                Description: "",
                Category: "",
                ForeignValue: "",
                LocationCountry: "",
                MoneyTransferAgentIndicator: ""
            },
            {
              CategoryCode: "104",
              ImportExport: [],
              SARBAuth: {},
              TravelMode: {},
              ThirdParty: {},
              ForeignCurrencyCode: "USD",
              SequenceNumber: "1",
              DomesticCurrencyCode: "NAD",
              IECurrencyCode: "USD",
              CategorySubCode: "02",
              Description: "",
              Category: "",
              ForeignValue: "",
              LocationCountry: "",
              MoneyTransferAgentIndicator: ""
          },
          {
            CategoryCode: "103",
            ImportExport: [],
            SARBAuth: {},
            TravelMode: {},
            ThirdParty: {},
            ForeignCurrencyCode: "USD",
            SequenceNumber: "1",
            DomesticCurrencyCode: "NAD",
            IECurrencyCode: "USD",
            CategorySubCode: "02",
            Description: "",
            Category: "",
            ForeignValue: "",
            LocationCountry: "",
            MoneyTransferAgentIndicator: ""
        }
        ],
        ReplacementTransaction: "N",
        ReplacementOriginalReference: "",
        ValueDate: "2021-04-12",
        TrnReference: "MBYNA1201214554C",
        BranchCode: "",
        BranchName: "",
        TotalForeignValue: "10",
        FlowCurrency: "ZAR"
    }
  },
  {
    name: "ITT UCR Tests",
    customData: {
      DealerType:"AD",
      msgHide:true,
      disabledBopCats:[
      "104/01",
      "101/02"
      ],
      excludedBopCats:[
        "101/01"
      ],
      inZAR:false
    },
    data: {
      Version:"FINSURV",
      ReportingQualifier:"BOPCUS",
      Flow:"IN",
      FlowCurrency:"$",
      TotalForeignValue:"123.3",
      ValueDate:"2021-05-08",
      Resident:{
        Individual:{
          StreetAddress:{
            AddressLine1:"5 SIMMONDS ST",
            Suburb:"MARSHALLTOWN",
            City:"JOHANNESBURG",
            PostalCode:"2001",
            Country:"ZA"
          },
          PostalAddress:{
            AddressLine1:"5 SIMMONDS ST",
            Suburb:"MARSHALLTOWN",
            City:"JOHANNESBURG",
            PostalCode:"2001",
            Country:"ZA"
          },
          ContactDetails: {
            ContactSurname:"AMATRYONA",
            ContactName:"AYUSHMATI",
            Email:"kulani.shiviti@standardbank.co.za",
            Telephone:"0712345680"
          },
          TaxClearanceCertificateIndicator:"N",
          AccountIdentifier:"RESIDENT OTHER",
          Name:"AYUSHMATI",
          Surname:"AMATRYONA",
          IDNumber:"6707015227089",
          DateOfBirth:"01 July 1967",
          Gender:"MALE"
        },
        Type:"Individual"
      },
      MonetaryAmount:[{
        CategoryCode:101,
        CategorySubCode:"01"
      }],
      AccountHolderStatus:"Non South African Resident"
    }
  },
  {
    name: "ITT Investigation",
    customData: {
      DealerType: "AD",
      msgHide: true,
      disabledBopCats: [
        "100",
        "200",
        "300",
        "400",
        "500",
        "600",
        "700",
        "800",
        "801",
        "802",
        "803",
        "804",
        "810",
        "815",
        "816",
        "817",
        "818",
        "819",
        "830",
        "832",
        "833"
      ],
      excludedBopCats: [],
      inZAR: false
    },
    data: {
      Version: "FINSURV",
      ReportingQualifier: "BOPCUS",
      Flow: "IN",
      FlowCurrency: "ZAR",
      TotalForeignValue: "3607.5",
      ValueDate: "2021-08-22",
      Resident: {
        Entity: {
          StreetAddress: {
            AddressLine1: "52 ANDERSON ST",
            Suburb: "MARSHALLTOWN",
            City: "JOHANNESBURG",
            Province: "GAUTENG",
            PostalCode: "2001",
            Country: "ZA"
          },
          PostalAddress: {
            AddressLine1: "PO BOX 876",
            Suburb: "BURNSIDE",
            City: "UMHLANGA ROCKS",
            Province: "KWAZULU NATAL",
            PostalCode: "4319",
            Country: "ZA"
          },
          ContactDetails: {
            ContactSurname: "ABARASA",
            ContactName: "LEBO",
            Email: "Mathapelo.Molefe@standardbank.co.za",
            Telephone: "0842942915"
          },
          AccountName: "BUSINESS CURRENT ACCOUNT",
          AccountNumber: "302127615",
          TaxClearanceCertificateIndicator: "N",
          AccountIdentifier: "RESIDENT OTHER",
          EntityName: "BANGS",
          TradingName: "TESTING",
          RegistrationNumber: "2000/381010/07",
          IndustrialClassification: "02",
          InstitutionalSector: "02"
        },
        Type: "Entity"
      },
      MonetaryAmount: [
        {
          CategoryCode: "101",
          CategorySubCode: "01",
          ForeignValue: "3607.5",
          DomesticValue: "3607.5"
        }
      ],
      AccountHolderStatus: "South African Resident"
    }  
  },
  {
    name: "EE Reversal Test",
    customData: {
      DealerType: "AD",
      msgHide: true,
      inZAR: false
    },
    data: {
      Version: "FINSURV",
      TrnReference: "ReversalTest",
      ReportingQualifier: "BOPCUS",
      Flow: "OUT",
      FlowCurrency: "LSL",
      TotalForeignValue: "20000",
      ValueDate: "2021-10-30",
      Resident: {
        Entity: {
          StreetAddress: {
              AddressLine1: "52 ANDERSON ST",
              Suburb: "MARSHALLTOWN",
              City: "JOHANNESBURG",
              Province: "Gauteng",
              PostalCode: "2001",
              Country: "ZA"
          },
          PostalAddress: {
              AddressLine1: "52 ANDERSON ST",
              Suburb: "MARSHALLTOWN",
              City: "JOHANNESBURG",
              Province: "Gauteng",
              PostalCode: "2001",
              Country: "ZA"
          },
          ContactDetails: {
              ContactSurname: "ABARASA",
              ContactName: "LEBO",
              Email: "Mathapelo.Molefe@standardbank.co.za",
              Telephone: "0842942915"
          },
          AccountName: "BUSINESS CURRENT ACCOUNT",
          AccountNumber: "302127615",
          TaxClearanceCertificateIndicator: "N",
          AccountIdentifier: "RESIDENT OTHER",
          EntityName: "BANGS",
          TradingName: "TESTING",
          RegistrationNumber: "2000/381010/07",
          IndustrialClassification: "02",
          InstitutionalSector: "02"
        },
        Type: "Entity"
      },
      MonetaryAmount: [{
        
      }],
      AccountHolderStatus: "South African Resident"
    }  
  }
];

// Since this dummy data needs to be tested in the Dev source and the Built source,
// it needs to work with requirejs and without it. So, cannot assume requirejs' 'define' will be
// defined.

if (typeof define !== "undefined")
  define({
    data: testData
  });

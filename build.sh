#!/bin/bash 

npm install
#set the version based on git HEAD
#bash setVersion.sh

# Cleanup TestData Build Folder
rm -r build

mkdir -p build/testData
mkdir -p build/rules
mkdir -p build/rules/coreRules
mkdir -p build/rules/evaluationEx

node node_modules/requirejs/bin/r.js -o build.js

# Copy Test Data
cp testData/testData.js build/testData/

# Copy Core Rules
cd rules
npm run build
cd ..

cp -r rules/build/* build/rules/


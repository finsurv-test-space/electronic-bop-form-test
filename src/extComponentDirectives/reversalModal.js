define(['require', '../app', '../helpers', '../fieldMap'], function (require, app, helpers, fieldMap) {

  app.directive('reversalModal', ['$uibModal', '$log', '$rootScope', '$http', function ($modal, $log, $rootScope, $http) {
    return {
      restrict: 'E',
      scope: {
        section: '@',
        openDetail: '=',
        fields: '=',
        money: '='
      },
      templateUrl: 'externalComponents/reversalModal/' + 'reversalButton.html',
      link: function (scope, element, attrs) {

        var elem = element.find('button');
        elem.on('click', function (e) {
          e.preventDefault();
          e.stopPropagation();
        });

        var bopdata = scope.bopdata = app.contextLink.getData();
        //We need to check if the data has changed...
        scope.$watch(
          // This function returns the value being watched. It is called for each turn of the $digest loop
          function () { return app.contextLink.getData(); },
          // This is the change listener, called when the value returned from the above function changes
          function (newValue, oldValue) {
            if (newValue !== oldValue) {
              // Only increment the counter if the value changed
              bopdata = scope.bopdata = app.contextLink.getData();
            }
          }
        );

        var modalInstance = {};

        scope.showReversalModal = function () {
          scope.errorMessage = "";
          scope.successMessage = "";
          scope.infoMessage = "Type a minimum 16-digit TrnReference to search";
          scope.valuesValidatedSave = false;
          scope.hasSelectedTrn = false;
          scope.canSave = false;
          scope.reversalValuesSave = false;
          scope.reversalMonetaryAmounts = [];
          scope.trnRefrencesList = [];
          scope.originalTransactionDetails = {};
          scope.stopLoading = false;
          scope.reversalTRNNumber = scope.fields.ReversalTrnRefNumber.val;
          scope.currentSection = "search";
          scope.currentSelectedTrn = '';
          // var customPath = './../../rules/reg_rule_repo/coreSADC/partials/'

          //TODO: Change back
          scope.modalInstance = helpers.customModalHelper($modal, $log, app.config.partialsPath + 'externalComponents/reversalModal/modal.html', function () {
            return {
              heading: "Transaction Reversal",
              body: app.config.partialsPath + 'externalComponents/reversalModal/' + 'reversalBody.html',
              footer: app.config.partialsPath + 'externalComponents/reversalModal/' + 'reversalFooter.html',
              fetchOriginalTransaction: fetchData,
              showSection: showSection,
              saveMonetaryAmounts: handleMonetaries,
              toggleCheckboxes: toggleSelects,
              toggleIndSelect: toggleIndSelect,
              validateValues: validateValues,
              selectedTrn: selectedTRN,
              clearChoices: clearChoices,
              getMonetaryExtData: getMonetaryExtData
            }
          }, scope);
        }

        function getMonetaryExtData() {
          showLoader();
          choseTRN(scope.currentSelectedTrn);
        }

        function clearChoices() {
          scope.hasSelectedTrn = false;
          scope.currentSelectedTrn = '';
        }

        function selectedTRN(value) {
          scope.hasSelectedTrn = true;
          scope.currentSelectedTrn = value;
        }

        function showSection(value) {
          scope.currentSection = value;
        }

        function fetchData() {
          scope.hasSelectedTrn = false;
          scope.trnRefrencesList = [];
          scope.successMessage = "";
          scope.originalTransactionDetails = {};
          scope.errorMessage = "";
          scope.stopLoading = false;
          scope.reversalMonetaryAmounts = [];

          showLoader();
          var trnRef = document.getElementById("reversalTRN");
          app.contextLink.hitExtValidateButton("Retrieve_ReversalPossibleTrnData", trnRef.value, scope, app.rules.wrapContext,
            function (status, code, data) {
              if (!scope.stopLoading) {
                if (status == "pass" && data) {
                  getTrnReferences(JSON.parse(data));
                  hideLoader();
                } else {
                  if (data && status != "busy") {
                    scope.errorMessage = data;
                    hideLoader();
                  }
                }
              } else {
                hideLoader();
              }
            })
        }

        function choseTRN(value) {
          scope.stopLoading = false;
          app.contextLink.hitExtValidateButton("Retrieve_ReversalOriginalData", value, scope, app.rules.wrapContext,
            function (status, code, data) {
              if (!scope.stopLoading) {
                if (status == "pass" && data) {
                  getOriginalDetails(JSON.parse(data));
                  getMonetary(JSON.parse(data));
                  hideLoader();
                } else {
                  if (data && status != "busy") {
                    scope.errorMessage = data;
                    hideLoader();
                  } else {
                    if (data && status != "busy") {
                      scope.errorMessage = data;
                      hideLoader();
                    }
                  }
                }
              } else {
                hideLoader();
              }
            })
        }

        function getTrnReferences(data) {
          scope.trnRefrencesList = [];
          var counter = 0;
          data.forEach(function (element) {
            counter++;
            transactionDetails = {};
            transactionDetails.trnReference = element.TrnReference;
            transactionDetails.amount = element.Amount;
            transactionDetails.CurrencyCode = element.CurrencyCode;
            transactionDetails.cancelled = element.Cancelled;
            transactionDetails.cancelledMessage = element.Cancelled == "Canc" ? 'This transaction has been cancelled' : '';
            scope.trnRefrencesList.push(transactionDetails);
          });

          if (counter > 0) {
            if (counter == 1)
              scope.successMessage = "" + counter + " Potential Transaction Found";
            else if (counter > 1)
              scope.successMessage = "" + counter + " Potential Transactions Found";
          }
        }

        function toggleIndSelect(event, ind) {
          var checkbox = document.getElementById("cbxSelectAll");
          var reversals = scope.reversalMonetaryAmounts;
          var selectedCheckbox = event.currentTarget;
          var flag = false;

          if (!selectedCheckbox.checked) {
            checkbox.checked = false
            reversals[ind].isSelected = false;
          } else {
            reversals[ind].isSelected = true;
          }

          reversals.forEach(function (element) {
            if (!element.isSelected) {
              flag = true;
            }
          })

          if (!flag) {
            checkbox.checked = true;
          }

          validate();
        }

        function toggleSelects() {
          var checkbox = document.getElementById("cbxSelectAll");
          var reversals = scope.reversalMonetaryAmounts;

          reversals.forEach(function (element) {
            if (element.Reversed == "No") {
              if (checkbox.checked) {
                element.isSelected = true;
              } else {
                element.isSelected = false;
              }
            }
          });

          validate();
        }

        function showLoader() {
          var loader = document.getElementById("loader");
          loader.style.display = "block";

          setTimeout(function () {
            if (loader.style.display == "block") {
              scope.stopLoading = true;
              hideLoader();
              scope.errorMessage = "Something went wrong";
              scope.$apply();
            } else {
              scope.errorMessage = "";
            }
          }, 20000)
        }

        function validateValues(ind) {
          var sequence = scope.reversalMonetaryAmounts[ind];

          if (sequence.ForeignValue < sequence.newAmountValue) {
            sequence.inError = "true";
            sequence.errorMessage = "Warning: Reversal Amount is more than Original Amount"
          } else {
            sequence.inError = "false";
            sequence.errorMessage = "";
          }

          validate();
        }

        function validate() {
          var foriegnSum = 0;
          var reversalAmountFlag = true;
          var valuesValidatedSave = true;
          var hasSelected = false;
          var totalAmount = scope.originalTransactionDetails.TotalForeignValue;

          scope.reversalMonetaryAmounts.forEach(function (element) {
            if (parseInt(element.newAmountValue)) {
              foriegnSum += parseInt(element.newAmountValue);
            }

            if (element.isSelected) {
              if (!element.newAmountValue || element.newAmountValue == '') {
                element.errorMessage = "Error: Please fill in a reversal value"
                element.needsValue = true;
                reversalAmountFlag = false;
              }
              else {
                element.needsValue = undefined;
              }
              hasSelected = true;
            }
            else {
              element.needsValue = undefined;
            }
          })

          if (foriegnSum > totalAmount) {
            scope.errorMessage = "Sum of Reversal Amounts is over TotalForiegnValue";
            valuesValidatedSave = false;
          } else {
            scope.errorMessage = "";
            valuesValidatedSave = true;
          }

          scope.canSave = (reversalAmountFlag && valuesValidatedSave && hasSelected);
        }

        function hideLoader() {
          var loader = document.getElementById("loader");
          loader.style.display = "none";
          setTimeout(function () {
            if (!scope.$root.$$phase) {
              scope.$root.$digest();
            }
          }, 500);
        }

        function handleMonetaries() {
          var moneyArr = [];
          scope.reversalMonetaryAmounts.forEach(function (element) {
            if (element.isSelected) {
              moneyArr.push(element);
            }
          });

          if (moneyArr.length > 0) {
            // Pass addMonetaries as param because the dialog box needs to be accepted before doing anything,
            // and monetary amounts needs to be added before others are removed.

            // This will pass it in as a function to be executed by app.removeMultipleMonetaryAmountsExport -> scope.removeMultipleMonetaryAmountsExport if
            // the dialog box returns true. (I know it's hacky, but it works..)
            var ams = (function () { return addMonetaries(moneyArr) });

            removeMonetaries(ams).then(function (result) {
              if (result) {
                scope.modalInstance.close();
              }
            })
          }
        }

        function removeMonetaries(ams) {
          var MAsToRemove = [];
          allMonetaries = app.getData().transaction.MonetaryAmount;
          allMonetaries.forEach(function (money) {
            if (money.ReversalTrnRefNumber) {
              if (money.ReversalTrnRefNumber == scope.currentSelectedTrn || (money.CategoryCode == '')) {
                MAsToRemove.push(money)
              }
            }

            if (MAsToRemove.length == 0) {
              if (allMonetaries.length == 1) {
                if ((!allMonetaries[0].CategoryCode || allMonetaries[0].CategoryCode == '') && (!allMonetaries[0].ForeignValue || allMonetaries[0].ForeignValue == '')) {
                  MAsToRemove.push(allMonetaries[0])
                }
              }
            }
          })

          return new Promise(function (resolve, reject) {
            if (MAsToRemove.length > 0) {
              app.removeMultipleMonetaryAmountsExport(MAsToRemove, 'modalBopFormConfirmMultipleDel', true, ams).then(function (result) {
                resolve(result)
              });
            } else {
              ams();
              resolve(true);
            }
          })
        }

        function addMonetaries(moneyArr) {
          moneyArr.forEach(function (newMoney) {
            if (!newMoney.ReversalTRNNumber) {
              newMoney.ReversalTrnRefNumber = scope.originalTransactionDetails.TrnReference;
            }

            if (!newMoney.ReversalTRNNumber) {
              newMoney.ReversalTrnSeqNumber = newMoney.SequenceNumber;
            }

            if (newMoney.CategoryCode) {
              newMoney.CategoryCode = newMoney.CategoryCode[0] + '00';
              newMoney.CategorySubCode = "";
            }

            if (newMoney.newAmountValue > 0) {
              newMoney.ForeignValue = newMoney.newAmountValue;
              newMoney.DomesticValue = undefined;
            }

            app.addMonetaryAmountExport(newMoney);
          })
        }

        function getOriginalDetails(data) {
          scope.originalTransactionDetails.TrnReference = getValue(data.TrnReference);
          scope.originalTransactionDetails.Flow = getValue(data.Flow);
          scope.originalTransactionDetails.ValueDate = getValue(data.ValueDate);
          scope.originalTransactionDetails.CurrencyCode = getValue(data.CurrencyCode);
          scope.originalTransactionDetails.TotalForeignValue = getValue(data.TotalForeignValue);
        }

        function getValue(value) {
          return value ? value : "";
        }

        function getMonetary(data) {
          var values = [];
          if (data) {
            (data.MonetaryAmount).forEach(function (element) {
              if (element.Reversed) {
                element.errorMessage = "Sequence Number has been reversed previously"
              }
              values.push(element);
            });

            scope.reversalMonetaryAmounts = values;
            // scope.$digest()
            // $timeout(function() {
            //   // anything you want can go here and will safely be run on the next digest.
            //   scope.$apply();
            // })            
          }
        }

      }
    }
  }])


})
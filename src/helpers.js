/**
 * Created by petruspretorius on 13/02/2015.
 */
define(function () {

  function composeImportExportArray(pasteData) {
    // split into rows
    var clipRows = pasteData.split('\n');

    // split rows into columns
    for (i = 0; i < clipRows.length; i++) {
      clipRows[i] = clipRows[i].split('\t');
    }

    var ndxImportControlNumber = -1;
    var ndxTransportDocumentNumber = -1;
    var ndxUCR = -1;
    var ndxPaymentCurrencyCode = -1;
    var defaultPaymentCurrencyCode = '';
    var ndxPaymentValue = -1;
    var ndxIVS = -1;

    // process columns
    var i;
    var clipColumns = clipRows[0];
    for (i = 0; i < clipColumns.length; i++) {
      var colName = clipColumns[i].toLowerCase().trim();
      if (colName.indexOf('icn') > -1 || colName.indexOf('import') > -1 || (colName.indexOf('mrn') > -1 && (colName.indexOf('ivs') == -1)) || colName.indexOf('invoice') > -1) {
        ndxImportControlNumber = i;
      }
      if (colName.indexOf('tdn') > -1 || colName.indexOf('trans') > -1) {
        ndxTransportDocumentNumber = i;
      }
      if (colName.indexOf('ucr') > -1 || colName.indexOf('consign') > -1) {
        ndxUCR = i;
      }
      if (colName.indexOf('curr') > -1) {
          ndxPaymentCurrencyCode = i;
      }
      if (colName.indexOf('ivs') > -1) {
        ndxIVS = i;
      }
      if (colName.indexOf('zar') == 0 || colName.indexOf('gbp') == 0 || colName.indexOf('usd') == 0) {
        ndxPaymentValue = i;
        defaultPaymentCurrencyCode = colName.toUpperCase().substr(0, 3);
      }
      if (colName.indexOf('val') > -1 || colName.indexOf('amount') > -1 || colName.indexOf('amnt') > -1) {
        ndxPaymentValue = i;
      }
    }

    var result = [];
    for (i = 1; i < clipRows.length; i++) {
      var ieEntry = {};
      for (var j = 0; j < clipRows[i].length; j++) {
        var cell = clipRows[i][j];
        if (cell && cell.length > 0) {
          if (ndxImportControlNumber === j) {
            ieEntry.ImportControlNumber = cell;
          }
          if (ndxTransportDocumentNumber === j) {
            ieEntry.TransportDocumentNumber = cell;
          }
          if (ndxUCR === j) {
            ieEntry.UCR = cell;
          }
          if (ndxPaymentCurrencyCode === j) {
            ieEntry.PaymentCurrencyCode = cell;
          }
          if (defaultPaymentCurrencyCode.length > 0) {
            ieEntry.PaymentCurrencyCode = defaultPaymentCurrencyCode;
          }
          if (ndxPaymentValue === j) {
            ieEntry.PaymentValue = cell;
          }
          if (ndxIVS === j) {
              ieEntry.MRNNotOnIVS = cell;
          }
        }
      }

      if (ieEntry.ImportControlNumber || ieEntry.TransportDocumentNumber || ieEntry.UCR || ieEntry.PaymentCurrencyCode || ieEntry.PaymentValue)
        result.push(ieEntry);
    }
    return result;
  }


  function getTransactionObjType(obj) {
    if (obj) {
      if (!_.isUndefined(obj.Exception)) {
        return 'Exception'
      } else if (!_.isUndefined(obj.Individual)) {
        return 'Individual'
      } else if (!_.isUndefined(obj.Entity)) {
        return 'Entity'
      } else {
        return 'Individual'
      }
    } else {
      return 'Individual'
    }
  }


  //-----------------------------------------------------------------
  // Print Helpers From : http://stackoverflow.com/questions/2255291/print-the-contents-of-a-div

  function PrintElem(elem) {
    Popup($(elem).html());
  }

  function Popup(data) {


    var mywindow = window.open('', 'my div', 'height=400,width=600');
    var style =
      ".col-sm-9 {" +
      "border-collapse: collapse;" +
      "box-sizing: border-box;" +
      "color: rgb(51, 51, 51);" +
      "display: block;" +
      "float: left;" +
      "font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" +
      "font-size: 13.9333333969116px;" +
      "height: 20px;" +
      "line-height: 19.9047622680664px;" +
      "min-height: 1px;" +
      "padding-left: 15px;" +
      "padding-right: 15px;" +
      "position: relative;" +
      "visibility: visible;" +
      "width: 867px;" +
      "}" +
      ".col-sm-3{" +
      "border-collapse: collapse;" +
      "box-sizing: border-box;" +
      "color: rgb(51, 51, 51);" +
      "display: block;" +
      "float: left;" +
      "font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" +
      "font-size: 13.9333333969116px;" +
      "height: 20px;" +
      "line-height: 19.9047622680664px;" +
      "min-height: 1px;" +
      "padding-left: 15px;" +
      "padding-right: 15px;" +
      "position: relative;" +
      "visibility: visible;" +
      "width: 289px;" +
      "}" +
      ".row{" +
      "border-collapse: collapse;" +
      "box-sizing: border-box;" +
      "color: rgb(51, 51, 51);" +
      "display: block;" +
      "font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" +
      "font-size: 13.9333333969116px;" +
      "height: 20px;" +
      "line-height: 19.9047622680664px;" +
      "margin-left: -15px;" +
      "margin-right: -15px;" +
      "visibility: visible;" +
      "width: 1156px;" +
      "}" +
      ".indent {" +
      "box-sizing: border-box;" +
      "color: rgb(51, 51, 51);" +
      "display: block;" +
      "font-family: 'Helvetica Neue', Helvetica, Arial, sans-serif;" +
      "font-size: 13.9333333969116px;" +
      "height: 92px;" +
      "line-height: 19.9047622680664px;" +
      "margin-left: 10px;" +
      "margin-top: 5px;" +
      "padding-bottom: 5px;" +
      "padding-left: 5px;" +
      "padding-right: 5px;" +
      "padding-top: 5px;" +
      "visibility: visible;" +
      "width: 1116px;" +
      "}" +
      ".ng-hide{display:none}";
    mywindow.document.write('<html><head><style>' + style + '</style><title>Bop Form Transaction</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write(data);
    mywindow.document.write('</body></html>');


    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10
    //console.log(mywindow.document);
    mywindow.print();
    mywindow.close();

    return true;
  }



  return {
    // transformIn: transformIn,
    //  modelWalkFn: modelWalkFn,
    composeImportExportArray: composeImportExportArray,
    getTransactionObjType: getTransactionObjType,
    PrintElem: PrintElem,

    valBttRelatedFldId: function (model) {
      return model.property
        .replace("ResponseCodes", "Response")
        .replace("ResponseCode", "Response")
        ;
    },
    inputFldIdMaker: function (model) {
      var indxs = (model && model.getIndeces) ? model.getIndeces() : [-1, -1];

      if (indxs[0] === -1) {
        return model.property;
      } else if (indxs[1] === -1) {
        return model.property + '_' + indxs[0];
      }

      return model.property + '_' + indxs[0] + '_' + indxs[1];
    },
    modalHelper: function ($modal, $log, path, locals, resultCB) {

      var modalInstance = $modal.open({
        templateUrl: path + 'modalWrap.html',
        controller: 'modalCntrl',
        animation: false,
        //backdrop : false,
        // size       : 'md',
        resolve: {
          locals: locals
        }
      });

      modalInstance.result.then(function () {
        if (!_.isUndefined(resultCB) && _.isFunction(resultCB)) {
          resultCB.apply(resultCB, arguments);
        }
      }, function () {

        //mainScope.$digest();
        $log.info('Modal dismissed at: ' + new Date());
      });
      return modalInstance;
    },
    customModalHelper: function ($modal, $log, path, locals, scope) {

      var modalInstance = $modal.open({
        templateUrl: path,
        controller: 'modalCntrl',
        animation: false,
        scope: scope,
        //backdrop : false,
        // size       : 'md',
        resolve: {
          locals: locals
        }
      });

      modalInstance.result.then(function () {
      
      }, function () {

        //mainScope.$digest();
        $log.info('Modal dismissed at: ' + new Date());
      });
      return modalInstance;
    }
  }
})
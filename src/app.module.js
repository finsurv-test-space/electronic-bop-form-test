/**
 * Created by petruspretorius on 16/02/2016.
 */

 var config = {
  shim: {
    "angular-material": ['angular','angular-animate','angular-aria','angular-messages'],
    'angular-animate': ['angular'],
    'angular-aria': ['angular'],
    'angular-messages': ['angular'],
    'angular-sanitize': ['angular'],
    'angular-bootstrap': ['angular'],
    'angular-bootstrap-templates': ['angular','angular-bootstrap'],
    'ng-file-upload' : ['ng-file-upload-shim'],
    'ng-file-upload-shim' : ['angular'],
    angular: {deps: [ 'underscore'], exports: "angular"},
  },
  waitSeconds: 100

}

if(window['access_token']){
  config.urlArgs = 'access_token='+window['access_token'];
}

require.config(config);

define(['require', 'angular', './app',
  './sectionDirectives/errorList',
  './fieldDirectives/ccfield',
  './fieldDirectives/datefield',
  './fieldDirectives/ffield',
  './fieldDirectives/ifield',
  './fieldDirectives/mfield',
  './fieldDirectives/sfield',
  './extComponentDirectives/reversalModal'
], function (require, angular, app) {

  return app;

});

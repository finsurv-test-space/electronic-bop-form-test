/**
 * Created by petruspretorius on 15/12/2015.
 */


/*
 * Module which extracts data from a form and populates an object with properties which are the same
 * as the element IDs...
 *
 * eg, <input id="foo.bar.baz" value ="moo"/> extracted will produce {foo:{bar:{baz:'moo'}}}
 *
 * For this module to work, the html input needs to be well defined. Only 'input' and 'select'
 * components are supported and they need the IDs to match the data structure which will be returned
 *
 */
define(['require', './bopContext', './objContext', './fieldMap', './modelBuilder', 'coreRules'], function (require, bopContext, objContext, formFieldMap, modelBuilder, rules) {

  var genRules, rulesForPart;

   function initRuleParts(){
     genRules = [rules.stdTrans, rules.stdMoney, rules.stdImportExport];
     
     if(rules.documentRules){
      genRules = genRules.concat([rules.documentRules.trans, rules.documentRules.money, rules.documentRules.ie]);
     }

     rulesForPart = {
       All: {
         validation: genRules,
         detail: [
           rules.detailDisplay.detailTrans,
           rules.detailDisplay.detailMoney,
           rules.detailDisplay.detailImportExport,
           rules.lookupFilterRules.filterLookupTrans,
           rules.lookupFilterRules.filterLookupMoney,
           rules.lookupFilterRules.filterLookupImportExport
         ],
         summary: [rules.summaryDisplay.summaryTrans, rules.summaryDisplay.summaryMoney, rules.summaryDisplay.summaryImportExport]
       },
       Summary: {
         //validation: genRules,
         summary: [rules.summaryDisplay.summaryTrans, rules.summaryDisplay.summaryMoney, rules.summaryDisplay.summaryImportExport]
       },
       Data: {
         validation: genRules
       },
       Trans: {
         validation: [rules.stdTrans],
         detail: [rules.detailDisplay.detailTrans, rules.lookupFilterRules.filterLookupTrans]
       },
       Money: {
         validation: [rules.stdMoney],
         detail: [rules.detailDisplay.detailMoney, rules.lookupFilterRules.filterLookupMoney]
       },
       ImportExport: {
         validation: [rules.stdImportExport],
         detail: [rules.detailDisplay.detailImportExport, rules.lookupFilterRules.filterLookupImportExport]
       }
  };}

  return {
    rules: rules,
    ruleEngineVersion : rules.version,
    setMappings: rules.setMappings,
    getMappings: rules.getMappings,
    map: rules.map,
    loadValidationRules: rules.loadValidationRules,
    loadLookups: rules.loadLookups,
    loadFormDefinition: rules.loadFormDefinition,
    loadDisplayRules: rules.loadDisplayRules,
    loadCustomRules: rules.loadCustomRules,
    loadRulePack: rules.loadRulePack,
    loadRulesPackage: rules.loadRulesPackage,
    registerNewLookup: function (attr, lookupItems) {
      rules.lookups[attr] = lookupItems;
    },
    lookups: rules.lookups,
    evaluationEx: rules.evaluationEx,
    makeContextLink: function () {

      if(!genRules)
        initRuleParts();

      // the Data
      var bopdata = {};
      var context = null;
    //  var dataOnlyContext = null;
      var currentPart = '';
      var localContext = null;
   //   var rootObject = null;
      var lookupCallback = {};
      var mustClearHiddenFields = false;
      var clearHiddenFieldsOnErrorOnly = false;

      var lastHiddenCount = 0;


      var validate = function (part) {

        // if (numRules != 0)
        // return;

        if (!_.isUndefined(part))
          currentPart = part;
        else
          part = currentPart;

        // first, clear any previous validation stuff...
        bopdata.clearValidationArtefacts();

        bopdata.clearRaisedEvents();

        // custom ZZ2 handling.
        var mayValidate = true;
        if (bopdata.MonetaryAmount && (bopdata.resolveField('CategoryCode', 0) === 'ZZ2'))
          mayValidate = false;

        
        if (rulesForPart[part].detail)
          rules.runDisplayRules(context, rulesForPart[part].detail);
        
        if (rulesForPart[part].summary)
          rules.runDisplayRules(context, rulesForPart[part].summary);

        if (mayValidate && rulesForPart[part].validation)
          rules.runValidation(context, rulesForPart[part].validation);

        if ((part === 'All') || (part === 'Summary')) {
          // now highlight sections with errors...
          var errors = bopdata.getRaisedEvents();
          if (errors.length != 0) {
            var _sections = [];
            _.forEach(errors,function (error) {
              var section = formFieldMap.sectionFromField(error.field);
              if (section) {
                _sections.push({
                  summary: section.summary,
                  type: error.type,
                  moneyInd: error.event == 'money' ? error.instance : error.event == 'importexport' ? error.moneyInstance : undefined,
                  importExportInd: error.event == 'importexport' ? error.instance : undefined
                });
              }
            });
            // localContext.setCurrentModal($('#SARB_Summary'));
            _.forEach(_sections,function (_section) {
              var field = localContext.resolveOrCreateField(_section.summary, _section.moneyInd, _section.importExportInd);

              if (field) field.error = true;
              //field.displayOnly= true;

            });

          }
        }

        // Clear fields that should be hidden
        if (mustClearHiddenFields) {
          var cnt = localContext.clearHiddenFields(clearHiddenFieldsOnErrorOnly);
          
          if(cnt == lastHiddenCount) return digestFunction(); //This is for the case where a rule is hiding then setting a field.
          lastHiddenCount = cnt;

          if (cnt > 0) { // deal with cascading rules;
            validate(part);
          }
        }

        digestFunction?digestFunction():undefined;
      }

      function isMatch(obj, val, matchOnLen, depth) {
        var isExact = depth ? depth > 1 : false;

        if (typeof obj === 'string') {
          return (matchOnLen > 0 && isExact === false) ? 
          obj.indexOf(val.substr(0, matchOnLen))  >= 0 : obj == val;
        }
        var isMatchResolve = false;
        _.each(obj, function (itm, idx) {
          isMatchResolve = isMatchResolve || isMatch(itm, val, matchOnLen, depth ? depth + 1 : 1); //depth > 1 := match *exact*
        });

        return isMatchResolve;
      }

      function filterLookupDisplayRules(params) {
        var matchOnLen = params.prefixLen ? parseInt(params.prefixLen, 10) : undefined;
        var elID = params.id;
        var lookup = params.items;
        var valProp = params.prop;

        /*  //resolve the lookup we want to act on...
         _.each(bopContext.filteredLookups, function (key, item) {

         var _field = localContext.resolveField(key, item.moneyInstance, item.importExportInstance);
         if (_field)
         if (key.indexOf(model.property) != -1)
         elID = key;
         });*/

        // do the lookupFilterRules....
        if (bopContext.filteredLookups && bopContext.filteredLookups[elID]) {
            
          var filters = bopContext.filteredLookups[elID];
          _.each(filters, function ( filter, k) {
            lookup = lookup.filter(function (item) {
              var itemVal = "";

              if (valProp) {
                itemVal = item[valProp];
              } else {
                itemVal = item;
              }

              if (filter.exclude) {
                return !isMatch(filter.exclude, itemVal, matchOnLen);
              } else if (filter.limit) {
                return isMatch(filter.limit, itemVal, matchOnLen);
              } else if (filter.filter) {
                return filter.filter(itemVal, context);
              }
            });
          });
        }

        return lookup;
      }

      function logValidationEvent(scopeName, scopeProp, indxs, validationType, validationCode, validationMsg, isLifetime) {
        var isBusy = validationType == "BUSY" || validationType == "FAILBUSY";

        var isLifetimeEvent = isBusy === true && isLifetime;

        var doValid = isLifetimeEvent === true ? context.logTransactionEvent : context.logTransactionLifetimeEvent;

        var args = [validationType, scopeProp, validationCode, validationMsg];

        if (scopeName == "importexport") {
          doValid = isLifetimeEvent === true ? context.logImportExportEvent : context.logImportExportLifetimeEvent;

          args = [validationType, indxs[0], indxs[1], scopeProp, validationCode, validationMsg];
        } else if (scopeName == "money") {
          doValid = isLifetimeEvent === true ? context.logMoneyEvent : context.logMoneyLifetimeEvent;

          args = [validationType, indxs[0], scopeProp, validationCode, validationMsg];
        }

        doValid.apply(this, args);

        return isBusy;
      };

      var digestFunction;

      var validateCB = _.debounce(function () {
        validate('All');
        digestFunction();
        //$("body").removeClass("waiting");
      },1000);

      var _export = {
        getPotentialDocsForCategory : rules.getPotentialDocsForCategory,
        lookups: rules.lookups,
        setData: function (dataIn, customData, digestFn) {
          // TODO: extra processing for split transactions.
          digestFunction = digestFn;
          bopdata = dataIn;
          var model = modelBuilder.transformIn(dataIn);
          localContext = bopContext.produce(bopdata, model, digestFn, customData, rules.lookups);
          context = rules.wrapContext(localContext, rules.lookups, customData);
        //  dataOnlyContext = rules.wrapContext(objContext.produceDataContext(bopdata), rules.lookups, customData);

          //transactionSnapShotData = JSON.parse(JSON.stringify(customData.snapShot));
          //transactionSnapShotData.MonetaryAmount = dataIn.MonetaryAmount; // reference current monetary amount.
          //
          //snapShotContext = bopContext.produce(
          //  transactionSnapShotData,
          //  helpers.transformIn(transactionSnapShotData),
          //  digestFn);

          return model;
        },
        setClearHiddenFields: function (doClear, doClearOnErrorOnly) {
            mustClearHiddenFields = doClear;
            clearHiddenFieldsOnErrorOnly = doClearOnErrorOnly ? true : false;
        },
        validate: validate,
        isValid: function () {
          // validate('Data');
          return bopdata?bopdata.isValid():false;
        },
        hasWarning: function () {
          //validate('Data');
          return bopdata.hasWarning();
        },
        getData: function () {
          return bopdata;
        },
        getSnapshotData: function () {
          return context.getCustomValue('snapShot');
        },
        getRaisedEvents: function (dontValidate) {
          if (!dontValidate) {
            validate('Data');
          }
          return bopdata.getRaisedEvents();
        },
        buildCategories: function () {
          context.buildCategories();
        },
        getCategories: function () {
          return context.getCategories();
        },
        resolveField: function (field, moneyInd, ImportExportInd) {
          return localContext.resolveField(field, moneyInd, ImportExportInd);
        },
        setDataByField: function (scope, field, moneyInd, ImportExportInd, value) {
          return localContext.setDataByField(scope, field, moneyInd, ImportExportInd, value);
        },
        showResidentNonResident: function () {
          return context.section !== 'F';
        },
        showOldBopCodes: function () {
          var bool = context.getCustomValue('ShowOldCodes');
          if (typeof bool != 'undefined')
            return bool;
          return true;
        },
        priorityBopCodes: function () {
          return context.getCustomValue('PriorityCodes');
        },
        setValidationPart: function (part) {
          currentPart = part;
        },
        registerCallback: function (name, callback) {
          lookupCallback[name] = callback;
        },
        registerValidationCallback: function () {
          context.callbacks.registerValidationCallback.apply(null, arguments);
        },

        hitExtValidateButton: function (name, value, scope, context2, resultCallback) {
          context.callbacks.validate(name, value, scope, context, resultCallback)
        },

        filterLookupDisplayRules: filterLookupDisplayRules,
        isReadOnly: function () {
          var readOnly = context.getCustomValue('readOnly') ? context.getCustomValue('readOnly') : false;
          return readOnly;
        },
        getErrorCount: function () {
          return bopdata.eventCount('ERROR') + bopdata.eventCount('FAILBUSY');
        },
        getWarningCount: function () {
          return bopdata.eventCount('WARNING');
        },
        clearPersistedEvent: function (param) {
          bopdata.clearPersistedEvents([param]);
        },
        asyncCallback: function (scopeModel, callbackName) {

          var scopeName = scopeModel.scope;
          var scopeProp = scopeModel.property;
          var indxs = scopeModel.getIndeces();

          var callback = function (status, validationCode, validationMsg) {
            var validationType;

            if (status === "pass") {
              validationType = "SUCCESS";
            }
            else if (status === "fail") {
              validationType = "ERROR";
            }
            else if (status === "error" || status === "warning") {
              validationType = "WARNING";
            }
            else { // busy
              validationType = "BUSY";
            }

            logValidationEvent(scopeName, scopeProp, indxs, validationType, validationCode, validationMsg);
          };

          context.callbacks.validateButton(scopeModel, callbackName, scopeModel.val, scopeName, context, function () {
            return callback.apply(undefined, arguments);
          });
        },
        validationCallback: function (scopeModel, callbackName, fnCallback) {
          var scopeName = scopeModel.scope;
          var scopeProp = scopeModel.property;
          var indxs = scopeModel.getIndeces();

          delete scopeModel.pmsg;
          delete scopeModel.msg;
          //console.log(scopeModel);

          var logEvent = function (validationType, validationCode, validationMsg) {
            var isBusy = logValidationEvent(scopeName, scopeProp, indxs, validationType, validationCode, validationMsg, true);

            if (!isBusy) {
              scopeModel.val = undefined;
            }

            validateCB();
          };

          var callback = function (status, validationCode, validationMsg) {
            var validationType;
            if (status === "pass") {
              validationType = "SUCCESS";
            }
            else if (status === "fail") {
              validationType = "ERROR";
            }
            else if (status === "error" || status === "warning") {
              validationType = "WARNING";
            }
            else { // busy
              validationType = "BUSY";
            }
            //console.log([status, validationCode, validationMsg]);

            logEvent(validationType, validationCode, validationMsg);
          };

          scopeModel.val = Math.random().toString();

          context.callbacks.validateButton(scopeModel, callbackName, scopeModel.val, scopeName, context, function () {
            return !fnCallback
              ? callback.apply(undefined, arguments)
              : fnCallback.apply(undefined, [logEvent].concat(arguments));
          });
        },
        validateBtt: function (model, relId) {
          if (relId) {
            if (model.data[relId]) delete model.data[relId];
          }
          _export.validationCallback(model, "_ignore", function (logEvent, status, validationCode, validationMsg) {
            logEvent("SUCCESS");
          });
        },
        invalidateBtt: function (model, relId, logEvtArgs) {
          //console.log(["invalidateBtt", model]);
          if (relId) {
            if (model.data[relId]) delete model.data[relId];
          }
          _export.validationCallback(model, "_ignore", function (logEvent, status, validationCode, validationMsg) {
            if (logEvtArgs) logEvent.apply(this, logEvtArgs);
            else logEvent("ERROR", "-1", "Not validated");
          });
        },
        modelFromEvent: function (evt) {
          var vwMoneyIndx = (!_.isUndefined(evt.moneyInstance) ? evt.moneyInstance : evt.instance);
          var vwImpExIndx;
          if (!_.isUndefined(evt.moneyInstance))
            vwImpExIndx = (evt.instance);

          return _export.resolveField(evt.field, vwMoneyIndx, vwImpExIndx);
        },
        makeDelayedUpdate: function (params) {
            return _.debounce(function (scope, doValidation) {
                doValidation();

                if (params.onEnd) {
                    params.onEnd();
                }
                digestFunction();
            }, params.delayInMs);

            //return function (scope, doValidation) {
            //    setTimeout(function () {
            //        doValidation();

            //        if (params.onEnd) {
            //            params.onEnd();
            //        }

            //        digestFunction();
            //        //if (!scope.$$phase) {
            //        //    scope.$digest();
            //        //}
            //    }, params.delayInMs);
            //};
        }
      };

      return _export;

    }
  }


});

define(['require', 'coreRules'], function (require, rules) {   // default.js contains the default field mapping
  function detailForSectionGen(mainScope) {
    return function detailForSection(section, subSection) {
      var _section = _.find(rules.formDefinition.detail, function (item) {
        if (!subSection && typeof item.detail.subSection === 'undefined')
          return item.detail.section === section;
        else
          return ((item.detail.section === section) && (item.detail.subSection === subSection));
      });

      if (_section && _section.detail && _section.detail.detail) {
        var detailTemplate = _section.detail.detail;
        if(subSection && detailTemplate[detailTemplate.length-1]== '/'){
          if (_section.contextKey === 'Resident' && subSection === 'general') detailTemplate += mainScope.resType.toLowerCase();
          if (_section.contextKey === 'NonResident' && subSection === 'general') detailTemplate += mainScope.nonResType.toLowerCase();
        }
        return detailTemplate;
      }
      else
        throw('Definition file malformed or section or subSection incorrectly specified.');
    }
  }

  function allDetailsForSection(section) {
    var _sections = _.filter(rules.formDefinition.detail, function (item) {
      return item.detail.section === section;
    }).map(function (item) {
      return item.detail.detail;
    });

    if (_sections.length > 0)
      return _sections;
    else
      throw('Definition file malformed or section or subSection incorrectly specified.');
  }


  function hasScopedField(scope, field) {
    var section = sectionFromField(field);
    if (!_.isUndefined(section)) {
      if (section.scope == scope)
        return true;
    }
    return false;
  }

  function sectionFromField(field) {
    var _fieldMap = rules.formDefinition.detail;
    var _field = field.split('.');

    if(field=="ImportExport") return _fieldMap.ImportExport;

    return _.find(_fieldMap, function (item) {
      var fields = item.fields.map(function(item){
        return rules.map(item);
      })
      var checkcontextKey = false;
      if (fields && fields.indexOf(field) != -1)
        return true;//checkcontextKey = true;
      else {
        for (var i = _field.length - 1; i > 0; i--) {
          var back = _field.slice(i, _field.length);
          if (fields && fields.indexOf(back.join('.')) != -1) {
            checkcontextKey = true;
            break;
          }
        }
      }
      if (checkcontextKey) {
        // now check if the contextKeys match
        if (item.contextKey) {
          for (var j = 1; j < _field.length; j++) {
            var contextKey = _field.slice(0, j);
            if (item.contextKey == contextKey) {
              return true;
            }
          }
        } else {
          return true;
        }
      }
      // lastly, check if the field IS the base...
      if (item.base) {
        if (item.base == field)
          return true;
      }
    });
  }

  var _export = {
    sectionFromField: sectionFromField,
    detailForSectionGen: detailForSectionGen,
    allDetailsForSection: allDetailsForSection,
    hasScopedField: hasScopedField,
    customLookup: function () {
      return rules.formDefinition.customLookup;
    },
    allSections: function () {
      return rules.formDefinition.detail;
    },
    getSections: function (scopeStr) {
      return _.filter(rules.formDefinition.detail, {scope: scopeStr});
    },
    map: function(fieldName){
      if(fieldName.indexOf("{{")!=-1){
        return rules.map(fieldName);
      }
      return fieldName
    }
  };

  return _export;

});

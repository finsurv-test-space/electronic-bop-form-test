/**
 * Created by petruspretorius on 13/02/2015.
 */

define(['require', '../app', '../helpers', 'coreRules'], function (require, app, helpers, rules) {

//from http://jsfiddle.net/odiseo/dj6mX/


  app.service('CurrencyMaskUtils', function() {
    var CurrencyMaskUtils;
    return CurrencyMaskUtils = (function() {
      function CurrencyMaskUtils() {}

      CurrencyMaskUtils.clearSeparators = function(value) {
        if (value == null) {
          return;
        }
        if (typeof value === 'number') {
          value = value.toString();
        }
        return parseFloat(value.replace(/[^\d\.]+$/, '').replace(/[^\d]/g, '.').replace(/\.(?![^.]*$)/g, ''));
      };

      // CurrencyMaskUtils.toIntCents = function(value) {
      //   if (value != null) {
      //     return Math.abs(parseInt(CurrencyMaskUtils.clearSeparators(value) * 100));
      //   }
      // };

      // CurrencyMaskUtils.toFloatString = function(value) {
      //   if (value != null) {
      //     return (Math.abs(value / 100)).toFixed(2);
      //   }
      // };

      return CurrencyMaskUtils;

    })();
  });

  app.directive('currencyMask', ['$timeout', '$filter', 'CurrencyMaskUtils',function($timeout, $filter, CurrencyMaskUtils) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, elem, attrs, ctrl) {
        var Utils, applyCurrencyFilter, errorPrefix;
        errorPrefix = 'VTEX ngCurrencyMask';
        if (!ctrl) {
          throw new Error(errorPrefix + " requires ngModel!");
        }
        if (!/input/i.test(elem[0].tagName)) {
          throw new Error(errorPrefix + " should be binded to <input />.");
        }
        Utils = CurrencyMaskUtils;
        applyCurrencyFilter = function(value) {
          if (value == null) {
            value = ctrl.$viewValue || elem[0].value;
          }
          if (value != null) {
            return elem[0].value = $filter('currency')(Utils.clearSeparators(value), '');
          }
        };
        elem[0].addEventListener('blur', function() {
          return applyCurrencyFilter();
        });
        return $timeout(applyCurrencyFilter);
      }
    };
  }]);




  String.prototype.splice = function(idx, rem, s) {
    return (this.slice(0, idx) + s + this.slice(idx + Math.abs(rem)));
  };

  app.directive('productionQty', function() {
    return {
      priority: 1001,
      require: 'ngModel',
      link: function (scope, element, attr, ngModelCtrl) {
        function fromUser(text) {
          var transformedInput = text.replace(/[^0-9.,]/g, '');
          if(transformedInput !== text) {
              ngModelCtrl.$setViewValue(transformedInput);
              ngModelCtrl.$render();
          }
          return transformedInput;  // or return Number(transformedInput)
        }
        ngModelCtrl.$parsers.unshift(fromUser);
      }
    }; 
  });


  /* MONETARY Field */
  app.directive('mfield', function () {
    return {
      restrict: 'E',
      scope: {
        postValidFnName: '@postValid',
        label: '@label',
        model: '=model',
        alwaysShow: '=',
        currencyModel: '=currencyModel',
        inputCurrency: '@inputCurrency',
        clearBtnOnUpdate: '=clearbtt',
        tail: '=',
        tooltipTitle: '=',
        parallelModel: '=',
        isShowOverride: '='
      },
      link: function (scope, element, attrs, ctrl) {

        scope.currencies = rules.lookups.currencies;
        if(typeof scope.model.val == 'string'){
          // attempt to sanitise the string so Number() works...
          scope.model.val = scope.model.val.replace(/[^\d\.]/g,'');
        }
        scope.model.val = ((scope.model.val == "") || isNaN(Number(scope.model.val)))?undefined:Number(scope.model.val).toFixed(2);
        scope.Number = Number;

        scope.getCurrencies = function () {
            return app.contextLink.lookups.currencies;
        };

        scope.filterCurrencies = function (append, currencyCodes) {
            var currencies = scope.getCurrencies();

            var tmp = currencies.filter(function (curr) {
                return currencyCodes.indexOf(curr.code) >= 0;
            });

            if (append)
                tmp.push(append);

            return tmp;
        }

        scope.isSelected = false;

        if (_.isUndefined(scope.model)) throw new Error(scope.label + ': No Model... are you sure this was defined in the form definition file?');

        scope.currencyName = function () {
            var tmp = scope.currencies.filter(function (itm) { return itm.code == scope.currencyModel.val; });
            if (tmp.length)
                return tmp[0].name;
        };

        scope.inputFldId = function () { return helpers.inputFldIdMaker(scope.model); }

        //SS20150424: performance related - delay the model update digest (i.e. validation)
        //scope.update = helpers.update;
        var fnUpdate = app.contextLink.makeDelayedUpdate({
            delayInMs: 300,
            onEnd: function () {
                if (!scope.model.error) {
                    if (scope.postValidFnName) {
                        app.contextLink.asyncCallback(scope.model, scope.postValidFnName);
                    }
                }
            }
        });

        scope.update = function (regExMatch, dontDelay) {

            currencyRegEx = /^[0-9]+(?:\.[0-9]{1,2})?$/
            if(!scope.model.val.match(currencyRegEx) && scope.model.val.trim() != '') {
              scope.model.val = scope.model.val.replace(/[^\d\.]+$/, '').replace(/[^\d]/g, '.').replace(/\.(?![^.]*$)/g, '')
            }
            

            if (regExMatch && scope.model.val) {
                var rgX = new RegExp(regExMatch, "g");
                if (scope.model.val.match(new RegExp(rgX))) {
                    scope.model.val = scope.model.val.replace(rgX, "");
                }
            }
            if (scope.parallelModel) {
                scope.parallelModel.val = scope.model.val;
            }
            /* SS20150630: 'Validate' button clear on change */
            if (scope.clearBtnOnUpdate && !scope.clearBtnOnUpdate.hide) {
                //invalidate AND clear related field
                app.contextLink.invalidateBtt(scope.clearBtnOnUpdate, helpers.valBttRelatedFldId(scope.clearBtnOnUpdate));
            }
            /* ends::SS20150630 */
            if (dontDelay)
                // app.validate();
                app.setCustomData('edited',true);
            else fnUpdate(scope, function () {
                // app.validate();
                app.setCustomData('edited',true);
            });

            // app.setCustomData('edited',true);
        };
      },

        templateUrl: app.config.fieldPartialsPath
            ? app.config.fieldPartialsPath + 'mfield.html'
            : 'fields/mfield.html'
        //templateUrl: app.config.partialsPath + 'fields/mfield.html'
    };
  });
})









/**
 * Created by petruspretorius on 13/02/2015.
 */
define(['require', '../app', '../helpers', 'coreRules'], function (require, app, helpers, rules) {

  var storedLFieldValue = '';

  function lookupRuleFiltered(model, lookups, valPropName) {

    return (lookups && lookups.length) ? app.contextLink
      .filterLookupDisplayRules({
        id: model.property,
        items: lookups,
        prop: !lookups[0].value
          ? (valPropName ? valPropName : undefined)
          : 'value'
      }) : [];
  }

  /* SELECT field */

  // Helper to map lookup to standard 'label' 'value' pairs for the partial.
  //@param: lookup. The lookup array. Can be [String] or [{valuePropName:'foo',labelPropName:'bar'}]
  //@param: valueProp. The key used to denote the label in the lookup array.
  //@param: labelProp. The key used to denote the value in the lookup array.
  //@returns: the mapped lookup.
  var mapFldToOpts = function (lookup, valueProp, labelProp) {
    if(!lookup) {
      return console.log('Warning. Lookup not defined.');
    }
    // if lookup values is a function, just call it. must return an array of {label:,value:} objects
    if ((!_.isUndefined(lookup)) && (typeof lookup === 'function')) {
      return lookup();
    }

    if (_.isObject(lookup[0])) {
      return lookup.map(function (lc) {
        return {
          "label": lc[labelProp ? labelProp : 'label'],
          "value": lc[valueProp ? valueProp : 'value']
        }
      });
    } else {
      return lookup.map(function (lc) {
        return {
          "label": lc,
          "value": lc
        }
      });
    }

  };


  app.directive('typeaheadFocus', function () {
    return {
      require: 'ngModel',
      link: function (scope, element, attr, ngModel) {

        //trigger the popup on 'click' because 'focus'
        //is also triggered after the item selection
        element.bind('click', function () {
          var viewValue = ngModel.$viewValue;
          var regx = /^(?:\()?(\w{2})(?:\)\s+\-(?:\s\w+)*)?$/;

          //restore to null value so that the typeahead can detect a change
          if (ngModel.$viewValue == ' ') {
            ngModel.$setViewValue(null);
          }

          //force trigger the popup
          if(viewValue) {
            ngModel.$setViewValue(' ');

            var countryCode = viewValue.match(regx);
            if (countryCode && countryCode.length == 2){
              storedLFieldValue = countryCode[1];
            }
            
          }
          else {
            ngModel.$setViewValue(' ');
          }

          //select all text in input if field is selected
        element[0].setSelectionRange(0, element[0].value.length);
        });

        //compare function that treats the empty space as a match
        scope.emptyOrMatch = function (actual, expected) {
          if (expected == ' ') {
            return true;
          }
          return actual.indexOf(expected) > -1;
        };
      }
    };
  });

  // To declare a filter we pass in two parameters to app.filter
  // The first parameter is the name of the filter
  // second is a function that will return another function that does the actual work of the filter
  app.filter('lFieldFilter', ['$filter', function ($filter) {
    // In the return function, we must pass in a single parameter
    // which will be the data we will work on.
    // We have the ability to support multiple other parameters
    //that can be passed into the filter optionally
    return function (arr, lookupProp, input, comparator) {
      var value = input && input != ' ' ? input : null;//$.trim();//input.replace(/ /g, '');

      var output;

      // Do filter work here
      output = $filter('filter')(arr, value, comparator);

      var compareObj = {};
      compareObj[lookupProp] = value;

      var codesOnly = $filter('filter')(output, compareObj, comparator);

      if (codesOnly.length === 1) {
        return codesOnly;
      }
      else if (output.length === 0) {
        //var expandedSearchREsults = $filter('filter')(output, { description: value }, comparator);
        return arr;
      }
      return output;
    }
  }]);

  function getLookup(scope){
    if(!_.isUndefined(scope.lookupValues)){
      return mapFldToOpts(scope.lookupValues, scope.valueProp, scope.labelProp);
    }else if(!_.isUndefined(rules.lookups[scope.lookup])){
      return mapFldToOpts(rules.lookups[scope.lookup], scope.valueProp, scope.labelProp);
    }else {
      console.log('WARNING!: No lookup "' + scope.lookup + '"' ,(scope));
    }
  }

  //Scope mutator for common lookup setup...
  function addLookupFns(scope) {

      var lookups = getLookup(scope);

      scope.getLookup = function () {
        //we need to do this to keep the digest function happy :)
        var newLookups = getLookup(scope);
        var diff = _.find(lookups,function (item, ind) {
          return (item.value !== newLookups[ind].value)
            || (item.label !== newLookups[ind].label)
        })

        if (diff) lookups = newLookups;

        return lookupRuleFiltered(scope.model, lookups, 'value');
      }

      //(filteredLoookup = (getLookup() | lFieldFilter:lookupVal:$viewValue))

      scope.update = function (option) {    
        var selectedOption = _.find(lookups,function(item){
          return (item.value == option);
        })
        if (typeof scope.updateFn == 'function')scope.updateFn(scope.model, selectedOption);
        // app.validate();
        app.setCustomData('edited',true);
      }

      scope.handleChange = function (){
        if(scope.tempModel){
          if (scope.tempModel.value) {
            scope.model.val = scope.tempModel.value;
          } else if (scope.tempModel.trim() != ""){
            scope.model.val = scope.tempModel.trim();
          }
        }
        if (scope.tempModel == ""){
          scope.model.val = undefined;
        } 
        // app.validate();
        app.setCustomData('edited',true);

      }

      scope.updateListField = function (option){
        if(option){
          if (option.value) {
            // Here I set the option.value to a mix of upper and lower case letters so that when the User types in the value for a Country the value is not the exact same as the lookup therefor the 
            // value is updated. This was to avoid a bug where if the user types in the value that is the exact same as the lookup value then the value is not updated.
            scope.model.val = randomCase(option.value);
          } else if (option.trim() != ""){
            scope.model.val = swapCase(option.trim());
          }
        }
        if (!option.value && option == " "){
          // scope.model.val = undefined;
          scope.model.val = storedLFieldValue;
        } 
        // app.validate();
        app.setCustomData('edited',true);
      }

      scope.findMatch = function(searchText){
        return searchText?scope.getLookup().filter(function(item){
          var txt = item.value + ", " + item.label;
          return txt.toLowerCase().indexOf(searchText.toLowerCase())!=-1;
        }):scope.getLookup();
      }

      scope.updateModel = function(option){
        if(option) scope.update(option.value);
      }

      if(scope.model.val) {
        scope.tempModel =  scope.getLookup().find(function(item){
          return item.value == scope.model.val;
        })
      }

  }

  /*
   Directive factory for lookup-based directives.
   adds update() and getLookup() functions on the scope for use in the partial.
   */
  function makeLookupDirective(directiveName, scope, linkExtraFn) {
    return app.directive(directiveName, function () {
      return {
        restrict: 'E',
        scope: angular.extend({
          label: '@',
          model: '=',
          alwaysShow: '=',
          //Although disabling of fields should be handled by the display rules engine
          //there are fringe cases where for example the field exists twice on the page and one
          //of elements is for displaying and one is for use editing.
          disableField: '=disableField',
          lookup: '@',
          lookupValues: '=',
          valueProp: '@',
          labelProp: '@',
          squashNumerics: '@',
          optional: '@',
          infoTitle: "@",
          infoText: "@",
          isShowOverride: '=',
          updateFn: '=',
          tooltipTitle: '='
        }, scope),
        link: function (scope, element, attrs, tabsCtrl) {
          scope.inputElmt = angular.element(element.find('input')[0]);

          scope.formatLabel = function(model){
            if(model) {
              var lookup = _.find(scope.getLookup(),function(item){
                // First checking if the model has a value as if it doesn't then the comparison can never be true. This way when using toUpperCase on a null value it doesn't complain
                // Next we're setting the model value to upper case again as we changed it in updateListField to get around the capital letter bug
                if(!model.val)
                  return false;
                model.val = model.val.toUpperCase();
                return item.value.toUpperCase() == model.val.toUpperCase();
              });
              if(lookup){
                return (lookup.value == lookup.label) ? lookup.value : '('+lookup.value+') - ' + lookup.label;
              }
            }
          }
          

          if (_.isUndefined(scope.model)) throw new Error(scope.label + ': No Model... are you sure this was defined in the form definition file?');

          scope.inputFldId = function () {
            return helpers.inputFldIdMaker(scope.model);
          }

          scope.isOptional = (scope.optional == "no")
            ? false
            : true; //optional by default...

          addLookupFns(scope);

          if (linkExtraFn)linkExtraFn(scope, element, attrs, tabsCtrl);
        },

        templateUrl: app.config.fieldPartialsPath
          ? app.config.fieldPartialsPath + directiveName + ".html"
          : 'fields/' + directiveName + ".html"
        //templateUrl: app.config.partialsPath + 'fields/lfield.html'
      };
    })
  }

function swapCase(original) {
  isUpperReg = new RegExp("^[A-Z]+$");
  isLowerReg = new RegExp("^[a-z]+$");

  if(original.match(isUpperReg)) {
    return original.toLowerCase();
  }
  else if(original.match(isLowerReg)) {
    return original.toUpperCase();
  }
  else {
    return original;
  }
}

function randomCase(original) {
  if(original.trim() == '')
    return original;
  
  newValue = '';
  newValue += original[0].toLowerCase();
  upperCase = true;
  for(i = 1; i < original.length; i++) {
    if(upperCase)
      newValue += original[i].toUpperCase();
    if(!upperCase)
      newValue += original[i].toLowerCase();
    upperCase = !upperCase;
  }
  
  return newValue;
}

  makeLookupDirective('lfield');


  makeLookupDirective('sfield');


  makeLookupDirective('csfield');


  makeLookupDirective('rfield', {
    tooltipTitle: '=',
    aux: '@'
  });

});

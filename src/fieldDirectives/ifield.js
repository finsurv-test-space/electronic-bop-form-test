/**
 * Created by petruspretorius on 13/02/2015.
 */


define(['require', '../app', '../helpers'], function (require, app, helpers) {
    

    app.directive('ifield', function () {
        return {
            restrict: 'E',
            scope: {
                label: '@label',
                model: '=model',
                optional: '@optional',
                nonMandatory: '=nonMandatory',
                alwaysShow: '=',
                placeholder: '@',
                //Although disabling of fields should be handled by the display rules engine
                //there are fringe cases where for example the field exists twice on the page and one
                //of elements is for displaying and one is for use editing.
                disableField: '=disableField',
                clearBtnOnUpdate: '=clearbtt',
                tooltipTitle: '=',
                isShowOverride: '=',
                maxLen: '=',
                afterupdate: "=afterupdate",
                aux: '@',
                updateFn: '='
            },
            link: function (scope, element, attrs, tabsCtrl) {
                scope.isOptional = (scope.optional == "yes")
                    ? true
                    : false;

                scope.notMandatory = function () {
                    return (scope.nonMandatory == "yes");
                };

                if (_.isUndefined(scope.model)) {
                    throw new Error(scope.label + ': No Model... are you sure this was defined in the form definition file?');
                }
                scope.inputFldId = function () { return helpers.inputFldIdMaker(scope.model); }


                var fnUpdate = app.contextLink.makeDelayedUpdate({
                    delayInMs: 500
                });

                scope.update = function () {
                    /* SS20150630: 'Validate' button clear on change */
                    if (scope.clearBtnOnUpdate && !scope.clearBtnOnUpdate.hide) {
                        //invalidate AND clear related field
                        app.contextLink.invalidateBtt(scope.clearBtnOnUpdate, helpers.valBttRelatedFldId(scope.clearBtnOnUpdate));
                    }
                    /* ends::SS20150630 */

                    if (typeof scope.updateFn == 'function')
                        scope.updateFn(scope.model);

                    fnUpdate(scope, function () {
                        app.setCustomData('edited',true);
                        // app.validate();
                    });

                    // app.setCustomData('edited',true);

                };
            },

            templateUrl: app.config.fieldPartialsPath
                ? app.config.fieldPartialsPath + 'ifield.html'
                : 'fields/ifield.html'
            //templateUrl: app.config.partialsPath + 'fields/ifield.html'
        };
    });


    app.directive('dfield', function () {
        return {
            restrict: 'E',
            scope: {
                value: '=value',
                isMoneyField: '@isMoneyField',
                currencyModel: '=currencyModel'
            },
            //link    : function (scope, element, attrs, tabsCtrl) {

            //},

            templateUrl: app.config.fieldPartialsPath
                ? app.config.fieldPartialsPath + 'dfield.html'
                : 'fields/dfield.html'
            //templateUrl: app.config.partialsPath + 'fields/dfield.html'
        };
    });
    

    app.directive('pfield', function () {
        return {
            restrict: 'E',
            scope: {
                label: '@label',
                value: '=value',
                isMoneyField: '@isMoneyField',
                currencyModel: '=currencyModel'
            },
            //link    : function (scope, element, attrs, tabsCtrl) {

            //},

            templateUrl: app.config.fieldPartialsPath
                ? app.config.fieldPartialsPath + 'pfield.html'
                : 'fields/pfield.html'
            //templateUrl: app.config.partialsPath + 'fields/pfield.html'
        };
    });


    app.directive('afield', function () {
        return {
            restrict: 'E',
            scope: {
                label: '@label',
                value: '=value',
                isMoneyField: '@isMoneyField',
                currencyModel: '=currencyModel'
            },
            //link    : function (scope, element, attrs, tabsCtrl) {

            //},

            templateUrl: app.config.fieldPartialsPath
                ? app.config.fieldPartialsPath + 'afield.html'
                : 'fields/afield.html'
            //templateUrl: app.config.partialsPath + 'fields/pfield.html'
        };
    });
    

    app.directive('bfield', function () {//TODO: save for later....
        return {
            restrict: 'E',
            scope: {
                label: '@label',
                model: '=model',
                validateWith: '@validation',
                view: '@view',
                onhidden: '@onhidden',
                onshown: '@onshown',
                clearBtnOnUpdate: '=clearbtt'
            },
            link: function (scope, element, attrs, tabsCtrl) {
                if (_.isUndefined(scope.model)) {
                    throw new Error(scope.label
                    + ': No Model... are you sure this was defined in the form definition file?');
                }
                scope.isView = (scope.view == "yes")
                    ? true
                    : false;

                scope.related = helpers.valBttRelatedFldId(scope.model);

                scope.defErr = 'ERROR -1 : Not validated';

                scope.validate = function (relId) {
                    //console.log(app.contextLink);
                    app.contextLink.validateBtt(scope.model, relId ? relId : undefined);
                };
                scope.validate();

                scope.triggerClick = function() {
                    scope.model.clicked = true;
                    scope.update();
                };

                scope.$watch(
                    // This function returns the value being watched. It is called for each turn of the $digest loop
                    function () { return scope.model.doclick; },
                    // This is the change listener, called when the value returned from the above function changes
                    function (newValue, oldValue) {
                        //console.log(["newValue", newValue, "oldValue", oldValue]);
                        //if (newValue !== oldValue)
                        // Only if value changed
                        if(scope.model.doclick)
                        {
                            delete scope.model.doclick;
                            //scope.foodCounter = scope.foodCounter + 1;
                            scope.triggerClick();
                        }
                    }
                );

                //attrs.$observe("hiddenstate", function (arg) { });

                attrs.$observe("hiddenstate", function (arg) {
                    if (arg !== "") {
                        scope.validate(scope.related);
                    } else {
                        var relFld = scope.model.data[scope.related];
                        if (relFld) {
                            if (relFld.status == "pass") {
                                // passed
                                //do NOT invalidate 
                            } else {
                                //invalidate only
                                app.contextLink.invalidateBtt(scope.model, undefined, [relFld.status == "warning" ? "WARNING" : "ERROR", relFld.errorCode, relFld.errorMessage]);
                            }
                        } else {
                            //invalidate AND clear related field
                            app.contextLink.invalidateBtt(scope.model, scope.related);
                        }
                    }
                });

                scope.viewResponse = function () {
                    var errMsg = (scope.model.pmsg ? scope.model.pmsg : scope.model.msg);

                    return !errMsg ? ""
                        : errMsg
                        .map(function (itm) { return itm.replace('ERROR ', '').replace('FAILBUSY ', '').replace('WARNING ', ''); })
                        .join('\n\n');
                }

                scope.isValid = function() {
                    return !(scope.perror() || scope.pwarn() || scope.model.busy || scope.model.error || scope.model.warning);
                };

                scope.perror = function () {
                    return _(scope.model.pmsg).any(function (msg) { return msg.indexOf("ERROR") > -1 || msg.indexOf("FAILBUSY") > -1; }) | scope.model.error;
                };

                scope.pwarn = function () {
                    return _(scope.model.pmsg).any(function (msg) { return msg.indexOf("WARNING") > -1; }) | scope.model.warning;
                };

                scope.prompt = function(initialMsg, busyMsg, validMsg, errMsg) {
                   return !scope.isValid()
                    ? (scope.model.busy
                        ? busyMsg
                        : (scope.model.pmsg.toString() != scope.defErr
                                ? errMsg
                                : initialMsg)
                        )
                    : validMsg;
                };

                scope.update = function () {
                    if (scope.clearBtnOnUpdate && !scope.clearBtnOnUpdate.hide) {
                        //invalidate AND clear related field
                        app.contextLink.invalidateBtt(scope.clearBtnOnUpdate, helpers.valBttRelatedFldId(scope.clearBtnOnUpdate));
                    }

                    app.contextLink.validationCallback(scope.model, scope.validateWith);

                    //helpers.update();
                    // app.validate();
                    app.setCustomData('edited',true);
                };
            },

            templateUrl : function(tElement,tAttrs){
                var template = (tAttrs.templateurl)? tAttrs.templateurl:'bfield.html';
                return app.config.fieldPartialsPath
                    ? app.config.fieldPartialsPath + template
                    : 'fields/'+template;

            }

            //templateUrl: app.config.fieldPartialsPath
            //    ? app.config.fieldPartialsPath + 'bfield.html'
            //    : 'fields/bfield.html'
        };

    });
})
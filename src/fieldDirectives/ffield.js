define(["require", "../app"], function(require, app) {
  app.directive("ffield", [
    "Upload",
    "$timeout",
    function(Upload, $timeout) {
      return {
        restrict: "E",
        scope: {
          label: "@label",
          model: "=model",
          optional: "@optional",
          nonMandatory: "=nonMandatory",
          alwaysShow: "=",
          placeholder: "@",
          //Although disabling of fields should be handled by the display rules engine
          //there are fringe cases where for example the field exists twice on the page and one
          //of elements is for displaying and one is for use editing.
          disableField: "=disableField",
          clearBtnOnUpdate: "=clearbtt",
          tooltipTitle: "=",
          isShowOverride: "=",
          maxLen: "=",
          aux: "@"
        },
        link: function(scope, element, attrs, tabsCtrl) {
          scope.isOptional = scope.optional == "yes" ? true : false;

          scope.notMandatory = function() {
            return scope.nonMandatory == "yes";
          };
       

          var fnUpdate = app.contextLink.makeDelayedUpdate({
            delayInMs: 700
          });

          scope.removeDocument = function(doc){
            scope.$parent.$parent.removeDocument(doc);
          }

          scope.uploadFile = function() {
            var doc = scope.model;

            if(!(doc && doc.file)){
              doc.hasError = true;
              return;
            }


            doc.hasError = false;
            doc.uploading = true;
            var file = doc.file;
            var adhoc = doc.event == 'adhoc';
 
            // The unique reference for the required document - comprised of trnReference, monetary amount index, import-export index and document type
            var docHandle = [scope.$parent.bopdata.TrnReference];
            if(doc.moneyInstance!=null) docHandle.push(doc.moneyInstance);
            if(doc.instance!=null) docHandle.push(doc.instance);
            docHandle.push(doc.code);

            docHandle = docHandle.join('.');

            var uploadProps = {
              method: 'POST',
              data: {
                file: file
              },
              withCredentials: true,
              url: (app.config.fileUploadURL ? app.config.fileUploadURL :
                  app.config.baseURL + 'producer/api/document' + (adhoc? '/adhoc':''))  +
                '?reportingSpace=SARB' +
                (adhoc? 
                  '&adhoc=true&documentId=' + ('adhoc_' + (new Date()).getTime()).replace(/#/g, '%23') +
                '&description=' +
                (doc.msg?doc.msg:'adhoc').replace(/#/g, '%23') +
                '&trnReference=' + scope.$parent.bopdata.TrnReference
                :
                '&documentHandle=' +
                docHandle.replace(/#/g, '%23'))
                
            }


            file.upload = Upload.upload(uploadProps);

            file.upload.then(
              function(response) {
                $timeout(function() {
                  if(response.data && (response.data.DocumentHandle || response.data.documentHandle)) doc.handle = response.data.DocumentHandle || response.data.documentHandle;
                  delete doc.uploading;
                  delete file.progress;
                  delete file.errorMsg;
                  delete file.$error;
                  file.result = response.data;
                  file.success = true;
                  app.validate();
                });
              },
              function(response) {
                $timeout(function() {
                  if (response.status > 0){
                    doc.hasError = true;
                    delete doc.handle;
                    delete doc.uploading;
                    delete file.progress;
                    file.errorMsg = scope.errorMsg = response.status + ": " + response.data;
                    file.success = false;
                    app.validate();
                  }
                });
              },
              function(evt) {
                // Math.min is to fix IE which reports 200% sometimes
                file.progress = Math.min(
                  100,
                  parseInt((100.0 * evt.loaded) / evt.total)
                );
              }
            );
          };
        },

        templateUrl: app.config.fieldPartialsPath
          ? app.config.fieldPartialsPath + "ifield.html"
          : "fields/ffield.html"
        //templateUrl: app.config.partialsPath + 'fields/ifield.html'
      };
    }
  ]);
});

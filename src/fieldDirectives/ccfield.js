/**
 * Created by petruspretorius on 13/02/2015.
 */

define(['require', '../app', '../helpers', 'coreRules'], function (require, app, helpers, rules) {

  function makeCCString(cat, subCat) {
    return cat ? (cat + ( subCat ? '/' + subCat : '' )) : "";
  }

    // ====================================================================================================================================================================================
    // Custom override of uibTypeaheadMatch because AngularJS is broken and they probably won't fix it
    // (The way they handled custom templateURLs does nothing)
    // ====================================================================================================================================================================================

    app.directive('uibTypeaheadMatchCustom', ['$templateRequest', '$compile', '$injector', function ($templateRequest, $compile, $injector) {
      return {
        scope: {
          index: '=',
          match: '=',
          query: '='
        },
        link: function (scope, element, attrs) {
          var tplUrl = attrs.templateUrl || 'uib/template/typeahead/typeahead-match.html';
          $templateRequest(tplUrl).then(function (tplContent) {
            var tplEl = angular.element(tplContent.trim());
            element.replaceWith(tplEl);
            $compile(tplEl)(scope);
          });
        }
      };
    }])

    // ====================================================================================================================================================================================


  app.directive('ccfield', function () {
    return {
      restrict: 'E',
      scope: {
        label: '@label',
        current: '=',
        update: '=',
        //Although disabling of fields should be handled by the display rules engine
        //there are fringe cases where for example the field exists twice on the page and one
        //of elements is for displaying and one is for use editing.
        disableField: '=disableField'
      },

      link: function (scope, element, attrs, tabsCtrl) {

        var emptyCat = {code:"",description:""}

        scope.getCategories = function () {

          return app.contextLink
            .filterLookupDisplayRules({
              id: 'CompoundCategoryCode',
              items: rules.lookups.categories,
              prop: 'code',
              prefixLen: 3
            })
            .filter(function (item) {
              return (item.flow == app.contextLink.getData().Flow);
            }).map(function (itm) {
              return {
                code: itm.code, 
                section: itm.section, 
                subsection: itm.subsection, 
                description: itm.description, 
                tags: itm.tags
              }
            });
        }

        getCategory = function(cc){
          var cats = app.contextLink.lookups.categories.filter(function (item) {
            return (item.flow == app.contextLink.getData().Flow);
          });//scope.getCategories();
          return cats.find(function(cat){
            return cat.code == cc;
          })
        }

        scope.$watch('current.money',function(oldVal,newVal){
          scope.tempModel = getCategory(makeCCString(scope.current.money.CategoryCode.val, scope.current.money.CategorySubCode.val));

        });


        scope.tempModel = getCategory(makeCCString(scope.current.money.CategoryCode.val, scope.current.money.CategorySubCode.val));


        scope.makeCCLabel = function (category) {
          if (category && category.disabled) 
            category = getCategory(makeCCString(scope.current.money.CategoryCode.val, scope.current.money.CategorySubCode.val))

          if (category && category.code)
            return category.code + ', ' + category.description;
          else if (category && category.code == '') {
            return category.description;
          }
          return category;
        }

        scope.handleChange = function (validCats) {
          var moneyData = scope.current.money;
          var oldCCcode = moneyData.CategoryCode.val;
          var code = scope.tempModel.code?
          scope.tempModel.code
          :scope.tempModel.split(',')[0];

          if (scope.tempModel.disabled) 
            return

          if (code != ' ') {
            moneyData.CategoryCode.val = undefined;
            moneyData.CategorySubCode.val = undefined;

            var isValidCat = true;
            if (validCats && !scope.tempModel.match(/[a-zA-Z]+/g)) {
                //console.log(["cats", cats]);
                isValidCat = validCats.map(function(itm) {
                        return itm.code; //.split('/')[0];
                    }).filter(function(itm) {
                        return itm.indexOf(code) >= 0;
                    })
                    .length > 0;
                    //.indexOf(code) >= 0;

                if (!isValidCat) {
                    emptyCat.code = '';
                    emptyCat.description = code;
                    scope.tempModel = scope.makeCCLabel(emptyCat);
                    code = undefined;
                }
            }
          }

          if (!(!code || code == ' ') && isValidCat)
          {

            var filteredCat = scope.getCategories().filter(function (item) {
              return item.code == code;
            })

            if(filteredCat.length == 1){
              var codes = code.trim().split('/');
              moneyData.CategoryCode.val = codes[0] === "" ? undefined : codes[0];
              moneyData.CategorySubCode.val = codes.length > 1 ? codes[1] : undefined;
              scope.tempModel = scope.makeCCLabel(filteredCat[0]);

              if (moneyData.CategoryCode.val == 'ZZ1') {
                app.appState.data.transaction.ReportingQualifier = 'NON REPORTABLE'
              } else if (oldCCcode == 'ZZ1' && moneyData.CategoryCode.val != 'ZZ1') {
                app.appState.data.transaction.ReportingQualifier = 'BOPCUS'
              }

            }else{
              emptyCat.code = '';
              emptyCat.description = code;
              scope.tempModel = scope.makeCCLabel(emptyCat);
            }

          }
          //app.contextLink.buildCategories();
          // app.validate();
          if (scope.update)
            scope.update(scope);

          if (!scope.$root.$$phase) {
            scope.$root.$digest();
          }

          if(oldCCcode == 'ZZ1' && code != 'ZZ1' && code != ' ') {
            app.setData(app.appState.data);
          }

          if(oldCCcode != 'ZZ1' && code == 'ZZ1') {
            app.setData(app.appState.data);
          }

          app.setCustomData('edited',true);
          
          return false;
        };

        

        if (scope.update)
            scope.update(scope);
      },
      templateUrl: app.config.fieldPartialsPath
        ? app.config.fieldPartialsPath + 'ccfield.html'
        : 'fields/ccfield.html'
      //templateUrl: app.config.partialsPath + 'fields/ccfield.html'
    };
  });

})

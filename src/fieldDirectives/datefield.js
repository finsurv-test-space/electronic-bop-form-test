/**
 * Created by petruspretorius on 26/02/2015.
 */
define(['require', '../app', '../helpers'], function (require, app, helpers) {
  //var config = app.config;

  app.directive('datefield', ['$filter', function ($filter) {

    return {
      restrict: 'E',
      scope: {
        label: '@label',
        model: '=model',
        alwaysShow: '=',
        clearBtnOnUpdate: '=clearbtt',
        dt: '=',
        mode: '@',
        isShowOverride: '=',
        aux: '@',
        max: '=',
        updateFn: '='
      },
      link: function (scope, element, attrs, ngModel) {
        if (_.isUndefined(scope.model)) throw new Error(scope.label + ': No Model... are you sure this was defined in the form definition file?');


        scope.inputFldId = function () {
          return helpers.inputFldIdMaker(scope.model);
        }

        scope.toDateStr = function (dt) {
          return dt
            ? $filter('date')(new Date(dt), 'yyyy-MM-dd')
            : "";
        };

        scope.date = scope.model.va?new Date(scope.model.val):"";

        scope.$watch("model.val", function (arg) {
          if (arg && arg.length == 10)
            scope.date = new Date(arg);
        });

        scope.updateDate = function () {
          scope.model.val = scope.toDateStr(scope.date);

          if (typeof scope.updateFn == 'function')
              scope.updateFn(scope.model);

          if(!scope.model.val)
            element.find('input')[0].value = '';

          // app.validate();
          //SS20150630: 'Validate' button clear on change
          if (scope.clearBtnOnUpdate && !scope.clearBtnOnUpdate.hide) {
            //invalidate AND clear related field
            app.contextLink.invalidateBtt(scope.clearBtnOnUpdate, helpers.valBttRelatedFldId(scope.clearBtnOnUpdate));
          }
          app.setCustomData('edited', true);
        };

        scope.updateToDate = function (model) {
          scope.model.val = scope.toDateStr(model);

          if(!scope.model.val)
            element.find('input')[0].value = '';

          // app.validate();
          //SS20150630: 'Validate' button clear on change
          if (scope.clearBtnOnUpdate && !scope.clearBtnOnUpdate.hide) {
            //invalidate AND clear related field
            app.contextLink.invalidateBtt(scope.clearBtnOnUpdate, helpers.valBttRelatedFldId(scope.clearBtnOnUpdate));
          }
          app.setCustomData('edited', true);
        };

        scope.openDate = function ($event) {
          $event.preventDefault();
          $event.stopPropagation();
          scope.Opened = true;
        };
      },

      templateUrl: app.config.fieldPartialsPath
        ? app.config.fieldPartialsPath + 'datefield.html'
        : 'fields/datefield.html'
      //templateUrl: app.config.partialsPath + 'fields/datefield.html'
    };
  }]);

});

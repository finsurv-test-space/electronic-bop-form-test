define(function () {
  function getDataByField(scope, obj, field) {
    var fieldParts = field.split('.');
    var localObj = obj;
    if (localObj) {
      for (var i=0; i<fieldParts.length; i++) {
          var fieldPart = fieldParts[i];
          if (fieldPart in localObj) {
              localObj = localObj[fieldPart];
          }
          else {
              localObj = null;
          }
          if (!localObj) {
              if (i >= fieldParts.length-1)
                  return null;
              else {
                  if (scope === 'transaction' &&
                          (field.indexOf('Resident.') === 0 || field.indexOf('NonResident.') === 0) &&
                          i < 2)
                      return undefined;
                  else
                      return null;
              }
          }
      }
    }
    return localObj;
  }

  function setDataByField(scope, obj, field, value) {
    var fieldParts = field.split('.');
    var localObj = obj;
    for (var i=0; i<fieldParts.length; i++) {
        var fieldPart = fieldParts[i];
        if (fieldPart in localObj) {
          if (i === fieldParts.length-1)
            localObj[fieldPart] = value;
          else
            localObj = localObj[fieldPart];
        }
        else {
          localObj = null;
        }
        if (!localObj) {
          if (scope === 'transaction' &&
                (field.indexOf('Resident.') === 0 || field.indexOf('NonResident.') === 0) &&
                i < 2)
            return;
          else
            return;
        }
    }
  }

  function setField(scope, obj, field, composedSetting) {
    setDataByField(scope, obj, field, composedSetting);
  }

  function clearField(scope, obj, field) {
    setDataByField(scope, obj, field, undefined);
  }

  function appendField(scope, obj, field, composedSetting) {
    var fieldVal = getDataByField(scope, obj, field);
    setDataByField(scope, obj, field, fieldVal + composedSetting);
  }

  function produceDataContext(obj) {
    var dataContext = { objData: obj };
    var displays = {
      SET    : function (scope, instance, field, composedSetting) {
        setField(scope, instance, field, composedSetting);
      },
      APP    : function (scope, instance, field, composedSetting) {
        setField(scope, instance, field, composedSetting);
      },
      CLEAR  : function (scope, instance, field) {
        clearField(scope, instance, field);
      }
    };


    dataContext.getTransactionField = function(field) {
        return getDataByField('transaction', obj, field);
    };

    dataContext.getMoneySize = function() {
        var moneyList = obj.MonetaryAmount;
        if (moneyList)
            return moneyList.length;
        return 0;
    };

    dataContext.getMoneyField = function(instance, field) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList)
            return getDataByField('money', moneyList[instance], field);
        return null;
    };

    dataContext.getImportExportSize = function(moneyInstance) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return importList.length;
            }
        }
        return 0;
    };

    dataContext.getImportExportField = function(moneyInstance, instance, field) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList) {
            var money = moneyList[moneyInstance];
            var importList = money.ImportExport;
            if (importList) {
                return getDataByField('importexport', importList[instance], field);
            }
        }
        return null;
    };

    dataContext.logTransactionEvent = function(type, field, code, msg) {
    };

    dataContext.logMoneyEvent = function(type, instance, field, code, msg) {
    };

    dataContext.logImportExportEvent = function(type, moneyInstance, instance, field, code, msg) {
    };

    dataContext.logTransactionDisplayEvent = function (type, field, composedSetting) {
      var displayFunc = displays[type];
      if (displayFunc)
        displayFunc('transaction', obj, field, composedSetting);
    };

    dataContext.logMoneyDisplayEvent = function (type, instance, field, composedSetting) {
      var displayFunc = displays[type];
      if (displayFunc) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList)
          displayFunc('money', moneyList[instance], field, composedSetting);
      }
    };

    dataContext.logImportExportDisplayEvent = function (type, moneyInstance, instance, field, composedSetting) {
      var displayFunc = displays[type];
      if (displayFunc) {
        var moneyList = obj.MonetaryAmount;
        if (moneyList) {
          var money = moneyList[moneyInstance];
          var importList = money.ImportExport;
          if (importList) {
            displayFunc('importexport', importList[instance], field, composedSetting);
          }
        }
      }
    };
    return dataContext;
  }

  return {
    produceDataContext: produceDataContext
  };
});

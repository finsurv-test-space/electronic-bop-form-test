/**
 * Created by petruspretorius on 29/01/2015.
 */



define(['require','jquery', 'angular', './app.module'], function (require, $, angular, app) {

  $.fn.bopForm = function (options) {

    var currElement = this;

    app.initOptions(options, function () {

      $(currElement).html('<bopform></bopform>');

      angular.bootstrap(currElement, ['bopForm']);

      if (options.doneCB)options.doneCB(app);

    });

  }

  // intefaces....
  $.fn.bopForm.setData = function (data) {
    app.setData(data);
  }

  $.fn.bopForm.setCustomData = function (key, data) {
    app.setCustomData(key, data);
  }

  $.fn.bopForm.isValid = function () {
    return app.isValid();
  }

  $.fn.bopForm.getData = function (isTransactionOnly) {
    return app.getData(isTransactionOnly ? true : false);
  }

  $.fn.bopForm.getRaisedEvents = function () {
    return app.getRaisedEvents();
  }

  $.fn.bopForm.hasWarning = function () {
    return app.hasWarning();
  }

  $.fn.bopForm.getErrorCount = function () {
    return app.getErrorCount();
  }

  $.fn.bopForm.getWarningCount = function () {
    return app.getWarningCount();
  }

  $.fn.bopForm.showErrors = function () {
    return app.showErrors();
  }


  return {
    async: function () {
      var validationRefs = {};

      return {
        validationMaker: function (refId, resultFunc) {
          //var refId =
          var defaultResult = ["fail", "-99", "Failure (unknown cause)"];

          var jsObj = {asyncStatus: "pending", asyncValues: defaultResult, intervalId: -1};

          jsObj.intervalId = setInterval(function () {
            if (jsObj.asyncStatus === "done") {
              clearInterval(jsObj.intervalId);

              if (jsObj.asyncValues.length) {
                if (jsObj.asyncValues.length === 3) {
                  resultFunc(jsObj.asyncValues[0].toString(), jsObj.asyncValues[1].toString(), jsObj.asyncValues[2].toString());
                  return;
                }
              }
              resultFunc(defaultResult[0].toString(), defaultResult[1].toString(), defaultResult[2].toString());
            }
          }, 100);

          return jsObj;
        }
      };
    }
  }
});

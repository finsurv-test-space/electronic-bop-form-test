/**
 * Created by petruspretorius on 29/01/2015.
 */
/// <reference path="./app.module.d.ts"/>


/*
 The below is to get IE9 to not complain about custom events. Since no IE9-based system relies
 on the events, we just provide NOP implementations to keep the console quiet.
*/
if (typeof Event == 'undefined') Event = function () { };
if (typeof document.dispatchEvent == 'undefined') document.dispatchEvent = function () { };



//'use strict';

define(['require', 'angular', './contextLinker', './fieldMap', './helpers', './version', 'es6-promise',
  'angular-bootstrap-templates', 'ng-file-upload', 'angular-material', 'angular-animate', 'angular-sanitize'], function (require, angular, contextLinker, fieldMap, helpers, version, PromisePolyfill) {

    if (typeof Promise == "undefined")
      PromisePolyfill.polyfill();
    //================================================================================
    // Application specific stuff...
    //================================================================================

    function falseIfUndefined(x) {
      return x ? x : false;
    }

    var addCss = function (href, rel, type, cssId, media) {
      var head = document.getElementsByTagName('head')[0];
      var link = document.createElement('link');
      link.href = href;
      link.rel = rel ? rel : 'stylesheet';
      link.type = type ? type : 'text/css';
      if (cssId) link.id = cssId;
      if (media) link.media = media;
      head.appendChild(link);
    }

    var appState = {},
      mainScope = {
        $digest: function () {
        }
      },
      config;


    // Declare app level module
    var app = angular.module('bopForm', ['ui.bootstrap', 'ngFileUpload', 'ngMaterial', 'ngSanitize', 'ngAnimate']).config(['$mdDateLocaleProvider',
      function ($mdDateLocaleProvider) {
        $mdDateLocaleProvider.formatDate = function (date) {

          var d = new Date(date),
            month = '' + (d.getMonth() + 1),
            day = '' + d.getDate(),
            year = d.getFullYear();

          if (month.length < 2)
            month = '0' + month;
          if (day.length < 2)
            day = '0' + day;

          return date ? [year, month, day].join('-') : '';
        };

        $mdDateLocaleProvider.parseDate = function (dateString) {
          var d = new Date(dateString);
          return d == 'Invalid Date' ? new Date(NaN) : d;
        };
      }]
    );
    a = app;//global-hack to make debugging at clients easier.

    app.rules = contextLinker.rules;
    app.appState = appState;

    app.getConfig = function () {
      return config;
    };

    app.getEvaluator = function () {
      if (app.rules.evaluator) return app.rules.evaluator;
      throw new Error('evaluator has not been initialised yet!')
    }


    app.config = config = {
      baseURL: '',
      useTabs: true,
      // clearHidden: true,
      // css: './src/css/app',
      //formDefinitionURL:'./src/forms/default',
      partialsPath: "",//"./src/partials/",
      fieldPartialsPath: "",//"./src/partials/fields/",
      rootTemplate: 'SARB_Summary.html',
      rootTabTemplate: 'tabMain.html',
      transaction_summary: 'transaction_summary.html',
      resident_summary: 'resident_summary.html',
      non_resident_summary: 'non_resident_summary.html',
      monetary_summary: 'monetary_summary.html',
      importexport_summary: 'monetary/importexport_summary.html',
      //TRADESUITE ONLY
      resident_entity: 'resident/entity.html',
      resident_individual: 'resident/individual.html',
      resident_account: 'resident/account.html',
      resident_address: 'resident/address.html',
      resident_exceptions: 'resident/exceptions.html',
      non_resident_entity: 'nonresident/entity.html',
      non_resident_individual: 'nonresident/individual.html',
      non_resident_account: 'nonresident/account.html',
      non_resident_address: 'nonresident/address.html',
      non_resident_exceptions: 'nonresident/exceptions.html',
      transaction: 'transaction/transaction.html',
      monetary: 'monetary/main.html',
      thirdparty: 'monetary/thirdparty.html'
    }

    app.initOptions = function (container, options, callBack) {
      if (options) {
        app.config = config = angular.extend(config, options.config);
        if ((typeof config.noCss !== 'undefined') && (!config.noCss)) {
          if (typeof less !== 'undefined' && (config.css.indexOf('.css') == -1)) {
            addCss(config.css + '.less', "stylesheet/less");
            require(['less'], function () {
            });
          } else {
            if ((config.css.indexOf('.css') == -1)) {
              addCss(config.css + '.css');
            } else {
              addCss(config.css);
            }
          }
        }

        if (app.config.mappingsOverride) {
          contextLinker.setMappings(config.mappingsOverride, true);
        }

        appState.initData = options.initData;
        appState.validationCallBacks = options.validationCallBacks;
        appState.customLookups = options.customLookups;

        // Load the custom Rules
        if (config.rulePack) { // from a packed rules object
          config.rulePack.partials();
          config.rulePack.css(container);
          //if we are obtaining rules from any other URL, don't load those features...
          var features = {
            display: !config.displayRulesURL,
            document: !config.documentRulesURL,
            evaluations: !config.evaluationRulesURL,
            validations: !config.validationRulesURL,
            data: !config.lookupsURL,
            formDef: !config.formDefinitionURL
          }
          contextLinker.loadRulePack(config.rulePack, function (rules) {
            if (rules.customFns && (rules.customFns.length)) {
              rules.customFns.forEach(function (fn) {
                fn(app, app.config);
              })
            }
            loadConfigURLs(config, callBack);
          }, features);
        } else if (config.rulesPackage) { // From a Package path


          contextLinker.loadRulesPackage(config.rulesPackage, function (rules) {
            if (rules.customFns) rules.customFns(app, app.config);
            callBack();
          });

          //TODO: dynamically load partials and css with less.
          config.partialsPath = "";//'./rules/build/crules/'+config.rulesPackage+"/partials/";
          config.fieldPartialsPath = "";//'./rules/build/crules/'+config.rulesPackage+"/partials/fields/";
          require(['rules/build/crules/' + config.rulesPackage + "/css/css"], function (cssFn) {
            cssFn(container)
          });
          require(['rules/build/crules/' + config.rulesPackage + "/partials/partials"], function (partials) {
            partials();
          });
          // }
        } else if (config.customRulesPath) { // From a AMD Package
          contextLinker.loadCustomRules(config.customRulesPath, callBack);
        } else { // from URLs
          loadConfigURLs(config, callBack);
        }

      }

    }
    //----------------------------------------------------------------------------------------------------
    // helper functions
    //----------------------------------------------------------------------------------------------------
    function loadConfigURLs(config, callBack) {
      // load the rules from various REST end-points
      var loadingContext = [];

      if (config.documentRulesURL) {
        loadingContext.push(new Promise(function (resolve, reject) {
          contextLinker.loadValidationRules(config.documentRulesURL, resolve)
        }));
      }


      if (config.validationRulesURL) {
        loadingContext.push(new Promise(function (resolve, reject) {
          contextLinker.loadValidationRules(config.validationRulesURL, resolve)
        }));
      }

      if (config.displayRulesURL) {
        loadingContext.push(new Promise(function (resolve, reject) {
          contextLinker.loadDisplayRules(config.displayRulesURL, resolve)
        }));
      }

      if (config.lookupsURL) {
        loadingContext.push(new Promise(function (resolve, reject) {
          contextLinker.loadLookups(config.lookupsURL, resolve)
        }));
      }

      if (config.formDefinitionURL) {
        loadingContext.push(new Promise(function (resolve, reject) {
          contextLinker.loadFormDefinition(config.formDefinitionURL, resolve)
        }));
      }

      Promise.all(loadingContext)
        .then(function () {
          callBack();
        })

    }


    function keys(obj) {
      var out = [];
      for (var i in obj) {
        if (obj.hasOwnProperty(i)) out.push(i);
      }
      return out;
    }

    function derefAngostic(propList) {
      return function (data) {
        for (var prop, i = 0; prop = propList[i]; i++) {
          if (data[prop]) return data[prop];
        }
        data[propList[0]] = {};//default to 1st property...
        return data[propList[0]];
      }
    }

    var IndivEnt = derefAngostic(['Individual', 'Entity']);

    // mutates data to match decision. - defaults to the 1st customData decision.
    function applyDecisions(dataIn, decision) {

      decision = decision ? decision :
        ((dataIn.customData.decisions && Array.isArray(dataIn.customData.decisions)) ? dataIn.customData.decisions[0] : undefined);

      if (decision) {

        if (keys(contextLinker.evaluationEx.rep).indexOf(decision.reportable) != -1) {
          dataIn.transaction.ReportingQualifier = contextLinker.evaluationEx.repMap[decision.reportable].Qualifier;
        }

        if (decision.nonResAccountType) {
          if (mainScope.nonResType == 'Exception') {
            dataIn.transaction.NonResident = { Individual: {} };
            mainScope.nonResType = 'Individual';
          };
          IndivEnt(dataIn.transaction.NonResident).AccountIdentifier = decision.nonResAccountType;
        } else if (decision.nonResException) {
          dataIn.transaction.NonResident = {
            Exception: {
              ExceptionName: Array.isArray(decision.nonResException) ? decision.nonResException[0] : decision.nonResException
            }
          }
          mainScope.nonResType = 'Exception';
        }

        if (decision.resAccountType) {
          if (mainScope.nonResType == 'Exception') {
            dataIn.transaction.Resident = { Individual: {} };
            mainScope.resType = 'Individual';
          }
          IndivEnt(dataIn.transaction.Resident).AccountIdentifier = decision.resAccountType;
        } else if (decision.resException) {
          dataIn.transaction.Resident = {
            Exception: {
              ExceptionName: Array.isArray(decision.resException) ? decision.resException[0] : decision.resException
            }
          }
          mainScope.resType = 'Exception';
        }

        if (decision.flow) {
          dataIn.transaction.Flow = decision.flow;
        }

        if (decision.category) {
          dataIn.transaction.MonetaryAmount[0].CategoryCode = decision.category[0];
        }

      }
      return dataIn;
    }


    var initProvided = function (dataIn) {
      var out;
      if (typeof dataIn.transaction === 'undefined') {
        out = { transaction: dataIn, customData: { snapShot: JSON.parse(JSON.stringify(dataIn)) } };
      } else {
        if (typeof dataIn.customData === 'undefined') {
          out = { transaction: dataIn.transaction, customData: { snapShot: JSON.parse(JSON.stringify(dataIn.transaction)) } };
        } else {
          if (!dataIn.customData.snapShot) {
            var customData = _.extend(dataIn.customData, { snapShot: JSON.parse(JSON.stringify(dataIn.transaction)) });
            out = { transaction: dataIn.transaction, customData: customData };
          } else {
            out = dataIn;
          }
        }
      }

      // decisions, decisions, decisions...
      if (out.customData.decisions && (out.customData.decisions.length > 0)) {
        if (!out.customData.side)
          throw new Error('When providing "decisions", you need to also provide the "side" property as "CREDIT" or "DEBIT"');

        // filter decisions by "side"
        var side = out.customData.side;
        out.customData.decisions = out.customData.decisions.filter(function (item) {
          return !item.reportingSide || (item.reportingSide == side);
        })

        if (out.customData.decisions.length == 0)
          throw new Error('None of the "decisions" provided match the "side" property...');

        //now that we have one or more decisions, build the category filter...

        // returns reduce function which only pushes items not in arr, while removing items from arr that match
        function reduceFnExcludeFrom(arr) {
          return function (memo, item) {
            var ind = arr.indexOf(item);
            if (ind != -1) {
              arr.splice(ind, 1); // duplicate, remove from arr, don't store
            } else {
              memo.push(item); // no duplicate, store
            }
            return memo;
          }
        }

        var blanketFilter = false;
        var catFilters = out.customData.decisions.reduce(function (memo, item) {
          if (!blanketFilter) { // while we don't have a blanket filter, we can attempt to determine cat filters.
            if (item.category) {
              memo.category = memo.category.concat(item.category);
              // check if we have notCategory entries to cancel these ones out...
              memo.notCategory = memo.notCategory.reduce(reduceFnExcludeFrom(memo.category), []);
            }
            if (item.notCategory) {
              memo.notCategory = memo.notCategory.concat(item.notCategory);
              // check if we have category entries to cancel these ones out...
              memo.category = memo.category.reduce(reduceFnExcludeFrom(memo.notCategory), []);
            }
            if (!(item.category || item.notCategory)) {
              blanketFilter = true;
              memo = { category: [], notCategory: [] };//reset
            }
          }

          return memo;

        }, { category: [], notCategory: [] });

        //TODO!!!!! Do something with the catFilters.
      }

      out.transaction = _.extend({
        "Version": "FINSURV",
        "ReportingQualifier": "BOPCUS",
        "Flow": "OUT",
        "Resident": {
          "Entity": {}
        },
        "NonResident": {
          "Entity": {}
        },
        "MonetaryAmount": [{}]
      }, out.transaction);

      return applyDecisions(out);
    };

    var isReadOnly = false;
    var mainContextLink = undefined;
    var snapShotContextLink = undefined;
    var currentDecision;


    function localDigest() {
      var decisions = mainScope.customData.decisions;
      if (decisions && decisions.length > 0) {
        var moneyCats = mainScope.data.MonetaryAmount.map(function (money) {
          return money.CategoryCode.val + (money.CategorySubCode.val ? '/' + money.CategorySubCode.val : '');
        });
        var filteredDecisions = decisions.filter(function (decision) {
          return (!(decision.category || decision.notCategory))
            ||
            (decision.category && moneyCats.filter(function (cat) {
              return (decision.category.indexOf(cat) != -1)
            }).length > 0)
            ||
            (decision.notCategory && moneyCats.filter(function (cat) {
              return (decision.notCategory.indexOf(cat) == -1)
            }).length > 0)
        });

        if (filteredDecisions.length != 1) throw new Error('Decisions ambigious... are the Decisions correctly configured?')
        var filteredDecision = filteredDecisions[0];

        if (currentDecision != filteredDecision) {
          currentDecision = filteredDecision;
          applyDecisions(appState.data, currentDecision);
          initData(appState.data.transaction, appState.data.customData);
        }
      }
      if (!mainScope.$$phase) {
        mainScope.$digest();
      }
    }

    function initData(data, customData) {
      //expose mappings to the main scope.
      mainScope.map = contextLinker.map;

      app.contextLink = mainContextLink = contextLinker.makeContextLink();

      mainScope.isValid = mainContextLink.isValid;
      mainScope.hasWarning = mainContextLink.hasWarning;

      //load custom lookups after the fact, otherwise they get overriden
      if (appState.customLookups && appState.customLookups.length) {
        registerCustomLookups(appState.customLookups);
      }

      var model = mainContextLink.setData(data, customData, localDigest);

      if (config.onChange)
        model.onChange(function () {
          config.onChange(data, model, customData);
        });

      m = model;

      if (app.config.snapShotValidate) {
        var snapShotData = JSON.parse(JSON.stringify(data));

        snapShotData.MonetaryAmount = data.MonetaryAmount;

        snapShotContextLink = contextLinker.makeContextLink();

        var snapShotModel =
          sm =
          snapShotContextLink.setData(snapShotData, customData, function () {
            mainScope.$digest();
          });

        mainScope.snapShotModel = snapShotModel;

      }

      mainScope.resType = helpers.getTransactionObjType(data.Resident);
      mainScope.nonResType = helpers.getTransactionObjType(data.NonResident);
      mainScope.data = model;
      mainScope.Resident = (mainScope.resType == 'none' ? {} : model.Resident[mainScope.resType]);
      mainScope.NonResident = (mainScope.nonResType == 'none' ? {} : model.NonResident[mainScope.nonResType]);
      mainScope.customData = customData;

      mainContextLink.setClearHiddenFields(app.config.clearHidden, app.config.clearHiddenOnErrorOnly);

      // Register Validation Callbacks
      _.each(appState.validationCallBacks, function (validationCallbackParams) {
        mainContextLink.registerValidationCallback.apply(undefined, validationCallbackParams);
      });

      isReadOnly = mainContextLink.isReadOnly();

      mainScope.current = { moneyInd: 0 };
      mainScope.current.money = model.MonetaryAmount[0];

      app.validate();

      return model;
    }

    function cLookupMap(keyName, customLookup, lookupItem) {
      var ruleScope = customLookup.ruleScope;
      var fldVal = lookupItem[keyName];
      var fldMap, fldMapId;
      fldMap = customLookup.mapping[keyName];

      if (!fldMap) {
        return null;
      }
      if (!Array.isArray(fldMap)) {
        fldMapId = fldMap;
      } else {
        ruleScope = fldMap[0];
        fldMapId = fldMap[1];
      }

      //SS20150520: if fldVal is JSON handle separately
      if ((typeof fldVal === "object") || (typeof fldVal === "string")) {
        return { key: keyName, id: fldMapId, val: fldVal, ruleScope: ruleScope };
      }

      var ret = [].concat(resolveCLookupMapVals({ parentId: fldMapId, propertyMap: fldVal }));

      for (var idx = 0; idx < ret.length; idx++) {
        ret[idx]['key'] = keyName;
        ret[idx]['ruleScope'] = ruleScope;
      }

      return ret;
    }

    function resolveCLookupMapVals(params) {
      var parentFldId, flds, fldObj;
      parentFldId = params.parentId;
      flds = params.flds ? params.flds : [];
      fldObj = params.propertyMap;

      for (var fld in fldObj) {
        var prop = fldObj[fld];
        if (prop) {
          var fldMapId = parentFldId + "." + fld;
          if (typeof prop === "string") {
            flds.push({ id: fldMapId, val: prop })
          } else {
            flds.concat(resolveCLookupMapVals({ parentId: fldMapId, flds: flds, propertyMap: prop }));
          }
        }
      }

      return flds;
    }

    function registerCustomLookups(customLookups) {//, mappings) {
      _.each(customLookups, function (customLookup) {
        var items = customLookup.lookupItems ? customLookup.lookupItems : [];

        var lookupItems = (customLookup.loadLookupItems)
          ? items.concat(customLookup.loadLookupItems())
          : items;

        contextLinker.registerNewLookup(customLookup.name, lookupItems);

      });
    }

    function setData(data) {
      d = appState.data = initProvided(data);
      // run pre-init-transform
      if (config.dataPreTransformFn) {
        config.dataPreTransformFn(appState.data);
      }

      var zz1 = appState.data.transaction.ReportingQualifier == "NON REPORTABLE";
      //init the MonetaryAmounts
      appState.data.transaction.MonetaryAmount =
        appState.data.transaction.MonetaryAmount.map(function (m) {
          // fix for ccfield
          if ([null, ''].indexOf(m.CategoryCode) != -1)
            delete m.CategoryCode;
          if ([null, ''].indexOf(m.CategorySubCode) != -1)
            delete m.CategorySubCode;

          return _.extend({
            "CategoryCode": zz1 ? "ZZ1" : undefined,
            "ImportExport": [],
            "SARBAuth": {},
            "TravelMode": {},
            "ThirdParty": {
            }
          }, m);
        })

      mainScope.Flow = appState.data.transaction.Flow;

      var model = initData(appState.data.transaction, appState.data.customData);

      if (model.MonetaryAmount.length > 200)
        alert("This transaction has more than 200 monetary amounts.\n Real-time validation will be disabled for perfomance reasons.")

      if (config.initializationFn) {
        config.initializationFn(model, mainScope);
      }
    }

    app.setData = setData;

    app.setCustomData = function (key, data) {
      if (_.isUndefined(appState.data.customData)) {
        appState.data.customData = {}
      }
      appState.data.customData[key] = data;
      app.validate();
    }

    function sequenceNumberHandler(money, fldName, fldNameMoney) {
      if (money) {
        for (var i = 0; i < money.length; i++) {
          money[i][fldNameMoney] = (i + 1).toString();
          var ie = money[i].ImportExport;

          if (ie) {
            for (var j = 0; j < ie.length; j++) {
              money[i].ImportExport[j][fldName] = (j + 1).toString();
            }
          }
        }
      }

      return money;
    };

    app.getData = function (isTransaction) {
      var ret = null;
      if (appState.data && appState.data.transaction) {
        var purified = JSON.stringify(appState.data.transaction);

        var data = ret = JSON.parse(purified);

        ret.MonetaryAmount = sequenceNumberHandler(data.MonetaryAmount, "SubsequenceNumber", "SequenceNumber");
      }

      if (isTransaction === true) {
        return ret;
      }

      var newDocList = [], customData = (appState.data && appState.data.customData)
        ? appState.data.customData
        : null;

      if (customData && customData.docList && customData.docList.length) {
        for (var doc, i = 0; doc = customData.docList[i]; i++) {
          var docHandle = [appState.data.transaction.TrnReference];
          if (doc.moneyInstance != null) docHandle.push(doc.moneyInstance);
          if (doc.instance != null) docHandle.push(doc.instance);
          docHandle.push(doc.code);

          docHandle = docHandle.join('.');

          if (doc.file && doc.file.success) {
            newDocList.push({
              success: true,
              code: doc.code,
              event: doc.event,
              field: doc.field,
              instance: doc.instance,
              moneyInstance: doc.moneyInstance,
              documentHandle: doc.handle ? doc.handle : docHandle,
              msg: doc.msg,
              type: "DOCUMENT",
              value: doc.value
            })
          }

        }
      }

      customData.isValid = mainContextLink.isValid();

      return { transaction: ret, customData: Object.assign({}, customData, { docList: newDocList }) };
    }

    app.validate = _.debounce(function () {
      if (!isReadOnly) {
        if (appState.data.transaction.MonetaryAmount.length > 200) return;

        if (app.config.snapShotValidate) {
          snapShotContextLink.buildCategories();
          snapShotContextLink.validate('All');
        }
        mainContextLink.buildCategories();
        mainContextLink.validate('All');

        if (app.config.postValidate) {
          app.config.postValidate(app.isValid());
        }

      } else {
        mainContextLink.validate('Summary');
      }
    }, 100);

    /** Arbitrary data validation function. This is only for use before the form has been displayed. */
    app.validateData = function (data) {
      var contextLink = contextLinker.makeContextLink();
      if (data.transaction) {
        contextLink.setData(data.transaction, data.customData);
      } else {
        contextLink.setData(data);
      }
      contextLink.validate("Data");

      return contextLink.getRaisedEvents();
    }


    app.isValid = function () {
      var validationsValid = mainContextLink ? mainContextLink.isValid() : false;
      if (validationsValid) {
        if (mainScope.hasRequiredDocs()) {
          return mainScope.hasUploadedDoc();
        }
      }
      return validationsValid;
    }

    app.hasWarning = function () {
      return mainContextLink ? mainContextLink.hasWarning() : false;
    }

    app.getRaisedEvents = function () {
      return mainContextLink ? mainContextLink.getRaisedEvents(true) : [];
    }

    app.getCategories = function () {
      return mainContextLink ? mainContextLink.getCategories() : [];
    }

    app.getErrorCount = function () {
      return mainContextLink ? mainContextLink.getErrorCount() : -1;
    }

    app.getWarningCount = function () {
      return mainContextLink ? mainContextLink.getWarningCount() : -1;
    }

    app.getPotentialDocsForCategory = function (cat, flow, section) {
      return mainContextLink ? mainContextLink.getPotentialDocsForCategory(cat, flow, section) : [];
    }

    // TODO: do we still use these?
    //app.cLookupMappings = function (objId) {
    //  return appState.customLookupMappings[objId];
    //};

    //================================================================================
    // Angular specific stuff...
    //================================================================================


    app.run(['$rootScope',
      function ($rootScope) {
        $rootScope.config = config; //.transaction_summary = 'app/src/bopForm/partials/' + config.transaction_summary;
      }]);

    app.controller('modalCntrl', ['$scope', '$uibModalInstance', 'locals', function ($scope, $modalInstance, locals) {

      $scope.cancel = function () {
        initData(appState.data.transaction, appState.data.customData);
        $modalInstance.dismiss('cancel');
      };
      $scope.closeModal = function () {

        $modalInstance.dismiss();
      }
      _.each(locals, function (local, localName) {
        $scope[localName] = local;
      })
    }])

    function getDetailTemplate(section, subsection) {
      var detailTemplate = fieldMap.detailForSectionGen(mainScope)(section, subsection);
      return detailTemplate + ".html";
    }

    function sumLocalValue(money) {
      return ((money[contextLinker.map('LocalValue')] && money[contextLinker.map('LocalValue')].val) ? Number(money[contextLinker.map('LocalValue')].val) : 0)
    }

    function sumMoney(money) {
      return (money.ForeignValue && money.ForeignValue.val)
        ? Number(money.ForeignValue.val)
        : ((money[contextLinker.map('LocalValue')] && money[contextLinker.map('LocalValue')].val) ? Number(money[contextLinker.map('LocalValue')].val) : 0);
    }

    function totalMoney(dataMonetaryAmount) {
      return _.reduce(dataMonetaryAmount, function (memo, item) {
        memo += sumMoney(item);
        return memo;
      }, 0);
    }

    function sumMoneyCurrency(data, currency) {
      return (currency ? currency : data.FlowCurrency.val) + " ";
    }

    function targetTotalMoney(data, targetTttlOveride) {
      var total = totalMoney(data.MonetaryAmount);

      var transTtl = targetTttlOveride ? targetTttlOveride : data.TotalForeignValue.val;

      return (transTtl ? transTtl : 0) - (total ? total : 0);
    }

    app.filter('filterInArray', ['$filter', function ($filter) {
      return function (list, lookInArr, propName) {
        var arrayFilter = lookInArr ? lookInArr : [];

        return $filter("filter")(list, function (listItem) {
          return arrayFilter.indexOf(listItem[propName]) != -1;
        });
      };
    }]);

    app.directive('bopform', ['$uibModal', '$log', '$filter', function ($modal, $log, $filter) {

      return {
        scope: {
          options: '=',
          linkedData: '=*?data'
        },
        link: function (scope, element, attrs, tabsCtrl) {

          mainScope = scope;
          scope.bopFormVersion = version.version.substr(0, 10) + '(' +
            contextLinker.ruleEngineVersion.major + '.' + contextLinker.ruleEngineVersion.minor + ')';

          scope.config = config;




          scope.util = {
            customData: function () {
              return scope.customData;
            },
            date: {
              now: function () {
                return new Date();
              },
              addYear: function (year, dt) {
                var dt1 = dt ? dt : new Date();
                return new Date(dt1.getYear() + year, dt1.getMonth(), dt1.getDate());
              }
            }
          };

          scope.filterLookup = function (itmExpr, coll) {
            return coll.filter(function (itm) {
              return itmExpr(itm);
            });//currencyFilter
          };

          scope.updateNonResSurname = function (data) {
            var firstName = data.NonResident.Individual.Name.val;
            var wholeName = appState.data.customData.OrderingCustomerName
            data.NonResident.Individual.Surname.val = wholeName.replace(',', ' ').replace(firstName, "").trim();
            app.validate();
          }

          scope.updateNonResName = function (data) {
            var lastName = data.NonResident.Individual.Surname.val;
            var wholeName = appState.data.customData.OrderingCustomerName
            data.NonResident.Individual.Name.val = wholeName.replace(',', ' ').replace(lastName, "").trim();
            app.validate();
          }


          scope.PrintAuth = function () {

            helpers.PrintElem('#BopAuth');

          }

          scope.revalidate = function () {
            app.validate();
          }

          scope.getLookups = function () {
            return mainContextLink.lookups
          };

          scope.unitTypes = ['Individual', 'Entity', 'Exception'];

          //scope.current.moneyInd = 0;
          //scope.current.money = null;
          scope.currentImportExportInstance = 0;

          scope.tab = {
            Transaction_Summary: {
              active: true
            }
          };


          scope.bopdataFn = function () {
            return mainContextLink.getData();
          }

          scope.setConfig = function (__config) {
            _config.setConfig(__config);
          }

          scope.setCurrentMoneyInd = function (ind) {

            scope.current.moneyInd = ind;
            scope.current.money = scope.data.MonetaryAmount[ind];

            //scope.tab = {Money:{active:true}};
          }

          scope.getDetailTemplate = getDetailTemplate;

          app.addMonetaryAmountExport = function (stub) {
            scope.addMonetaryAmount(stub)
          }

          scope.addMonetaryAmount = function (stub) {

            var locationCountry = undefined;
            var zz1 = appState.data.transaction.ReportingQualifier == "NON REPORTABLE";

            var data = appState.data.transaction;
            if (!data.MonetaryAmount) {
              data.MonetaryAmount = [];
            }

            if (data.MonetaryAmount && data.MonetaryAmount[data.MonetaryAmount.length - 1].LocationCountry) {
              locationCountry = data.MonetaryAmount[data.MonetaryAmount.length - 1].LocationCountry;
            }

            // var newSequenceNumber = data.MonetaryAmount.length + 1;
            data.MonetaryAmount.push(stub ? JSON.parse(JSON.stringify(stub)) : {
              //SequenceNumber: data.MonetaryAmount.length + 1,
              CategoryCode: zz1 ? "ZZ1" : undefined,
              //CategorySubCode: null,

              //FIN-1084, default any new monetary sections with AD for the transfer agent
              MoneyTransferAgentIndicator: "AD",
              LocationCountry: locationCountry,
              ImportExport: [],
              SARBAuth: {},
              TravelMode: {},
              ThirdParty: {
                // Individual: {}
              }
            });



            scope.data.addMoneyModel(scope.data, data);

            data.MonetaryAmount = sequenceNumberHandler(data.MonetaryAmount, "SubsequenceNumber", "SequenceNumber");

            scope.openDetail('Money', 'general', scope.data.MonetaryAmount[data.MonetaryAmount.length - 1]);

            app.validate();
          }

          scope.modalOpen = function (modal, parentScope) {
            scope.dialog = $modal.open({
              animation: true,
              templateUrl: modal + '.html',
              controller: ["$scope", "$uibModalInstance", "localScope", function ($scope, $uibModalInstance, localScope) {
                scope.modParent = localScope;
              }],
              resolve: {
                localScope: function () {
                  return parentScope;
                }
              }
            });

            // scope.dialog.rendered.then(function () {
            //   $(".modal:visible").each(function () {
            //     var modalDialog = $(this).find(".modal-dialog");

            //     // Applying the top margin on modal dialog to align it vertically center
            //     modalDialog.css("margin-top", Math.max(0, ($(window).height() - modalDialog.height()) / 2));
            //   });
            // });
          };

          scope.modalOk = function () {
            if (scope.dialog) {
              scope.dialog.close(arguments);
            }
          };

          scope.modalCancel = function () {
            if (scope.dialog) {
              scope.dialog.dismiss(arguments);
            }
          };

          app.removeMultipleMonetaryAmountsExport = function (money, confirmMsg, seqNumProvided, addMons) {
            if(!seqNumProvided) {seqNumProvided = false;}
            return scope.removeMultipleMonetaryAmounts(money, confirmMsg, seqNumProvided, addMons)
          }

          scope.removeMultipleMonetaryAmounts = function (moneys, confirmMsg, seqNumProvided, addMons) {
            if (moneys.length > 0) {
              return new Promise(function (resolve, reject) {
                if (!confirmMsg) {
                  moneys.forEach(function(money) {
                    scope.removeMonetaryAmount(money, null, false, seqNumProvided)
                  })
                  resolve(true);
                } else {
                  ShowConfirmModalRemoveMessage(confirmMsg).then(function(modalResult) {
                    if (modalResult) {
                      if (typeof addMons === 'function') {
                        addMons();
                      }

                      moneys = _.sortBy(moneys, function (item) {
                        return item.SequenceNumber;
                      }).reverse();

                      moneys.forEach(function(money) {
                        scope.removeMonetaryAmount(money, null, false, seqNumProvided)
                      })
                      resolve(true);
                    } else {
                      resolve(false);
                    }
                  })
                }
              })
            } else {
              return Promise.resolve("true");
            }
          }

          scope.removeMonetaryAmount = function (money, ie_DNE, confirmMsg, seqNumProvided) {
            if(!seqNumProvided) {seqNumProvided = false;}
            if (!confirmMsg) {
              removeMonAmnt(money, ie_DNE, seqNumProvided);
            } else {
              ShowConfirmModalRemoveMessage(confirmMsg).then(function(modalResult) {
                if (modalResult) {
                  removeMonAmnt(money, ie_DNE, seqNumProvided);
                }
              })
            }
          }

          function ShowConfirmModalRemoveMessage(confirmMsg) {
            scope.dialog = $modal.open({
              animation: true,
              size: 'sm',
              windowClass: 'inv-modal bopMod',
              backdropClass: 'bopModBg',
              templateUrl: confirmMsg + '.html',

              controller: ["$uibModalInstance", function ($uibModalInstance) {
                var $ctrl = this;
                $ctrl.ok = function () {
                  $uibModalInstance.close({ $value: "OK" });
                };

                $ctrl.cancel = function () {
                  $uibModalInstance.dismiss({ $value: 'cancel' });
                };
              },

              ],
              controllerAs: '$ctrl',
            });

            return new Promise(function (resolve, reject) {
              scope.dialog.result.then(function() {
                //console.log('success at:' + new Date());
                resolve(true);
              }, function () {
                //console.log('dismissed at: ' + new Date());
                resolve(false);
              });
            })
          }

          function removeMonAmnt(money, ie_DNE, seqNumProvided) {
            var data = appState.data.transaction;
            var model = scope.data;
            if (!data.MonetaryAmount) {
              return;
            }
            var moneyInd = seqNumProvided ? (money.SequenceNumber - 1) : model.MonetaryAmount.indexOf(money);

            var monetary = model.MonetaryAmount[moneyInd];

            for (var obj in monetary) {
              var prop = monetary[obj];

              if (typeof prop === "object" && obj == "ADInternalAuthResponseCode") {
                //console.log([obj, prop]);
                mainContextLink.validateBtt(prop);
              }
            }

            var ie = model.MonetaryAmount[moneyInd].ImportExport
              ? model.MonetaryAmount[moneyInd].ImportExport
              : [];
            for (var i = 0; i < ie.length; i++) {
              scope.removeImportExport(model.MonetaryAmount[moneyInd], ie[i]);
            }
            delete model.MonetaryAmount[moneyInd].ImportExport;

            model.MonetaryAmount.splice(moneyInd, 1);
            data.MonetaryAmount.splice(moneyInd, 1);

            if (scope.data.MonetaryAmount.length > 0) {
              scope.openDetail('Money', 'general', scope.data.MonetaryAmount[0]);
            }

            if (model.MonetaryAmount && model.MonetaryAmount.length > 0)
              model.MonetaryAmount[model.MonetaryAmount.length - 1].isTabOpen = true;

            data.MonetaryAmount = sequenceNumberHandler(data.MonetaryAmount, "SubsequenceNumber", "SequenceNumber");

            app.validate();
          }

          scope.addImportExport = function (money, dontOpenModal) {
            var moneyInd = scope.data.MonetaryAmount.indexOf(money);
            var moneyData = appState.data.transaction.MonetaryAmount[moneyInd];

            if (!moneyData.ImportExport) {
              moneyData.ImportExport = [];
            }
            moneyData.ImportExport.push({});

            scope.data.addIEModel(money, moneyData);

            //Set the selected import/export amount to the newly added one
            scope.currentImportExportInstance = moneyData.ImportExport.length - 1;
            if (!dontOpenModal) {
              money = scope.data.MonetaryAmount[moneyInd];
              scope.openImportExport(money, money.ImportExport[money.ImportExport.length - 1]);
            }
            appState.data.transaction.MonetaryAmount = sequenceNumberHandler(appState.data.transaction.MonetaryAmount, "SubsequenceNumber", "SequenceNumber");
            app.validate();
          }

          scope.removeImportExport = function (money, ie) {
            var moneyInd = scope.data.MonetaryAmount.indexOf(money);
            var ieIndex = money.ImportExport.indexOf(ie);
            var moneyData = appState.data.transaction.MonetaryAmount[moneyInd];

            // if there are uploaded docs, remove all the relevant ones.
            if (scope.customData.docList && scope.customData.docList.length) {
              var relatedDocs = scope.customData.docList.filter(function (doc) {
                return (doc.instance == ieIndex && doc.moneyInstance == moneyInd)
              })
              relatedDocs.forEach(function (relatedDoc) {
                var docInd = scope.customData.docList.indexOf(relatedDoc);
                scope.customData.docList.splice(docInd, 1);
              })
              // now we need to re-sequence the documents for this ie
              scope.customData.docList.forEach(function (doc) {
                if (doc.moneyInstance == moneyInd && doc.instance > ieIndex) {
                  doc.instance--;
                }
              })

            }

            moneyData.ImportExport.splice(ieIndex, 1);
            money.ImportExport.splice(ieIndex, 1);

            appState.data.transaction.MonetaryAmount = sequenceNumberHandler(appState.data.transaction.MonetaryAmount, "SubsequenceNumber", "SequenceNumber");
            app.validate();
          }

          scope.openPasteMonetaryAmounts = function (money) {

            scope.iePaste = undefined;
            var modalInstance = helpers.modalHelper($modal, $log, config.partialsPath, function () {
              return {
                heading: "Paste monetary amounts",
                body: config.partialsPath + 'monetary/pasteImportExport.html',
                money: money,
                processPaste: processPaste,
                AddPastedImportExportData: AddPastedImportExportData,
                data: scope.data,
                close: function () {
                  modalInstance.close();
                }
              };
            });

          }

          function processPaste(scope, textContent) {
            var currency = scope.data.FlowCurrency.val;
            var data = helpers.composeImportExportArray(textContent);
            scope.iePaste = undefined;

            var invalidCurrency = false;
            for (var i = 0; i < data.length; i++) {
              var itm = data[i];
              if (!!itm.PaymentCurrencyCode && itm.PaymentCurrencyCode !== currency) {
                invalidCurrency = true;
                break;
              }
            }
            if (invalidCurrency === true) {
              var modalInstance = helpers.modalHelper($modal, $log, config.partialsPath, function () {
                return {
                  heading: "Invalid Currency",
                  body: config.partialsPath + 'monetary/processPasteModal.html',
                  close: function () {
                    modalInstance.close();
                  }
                };
              });
              return;
            }
            scope.iePaste = data;
          }

          function AddPastedImportExportData(money, iePaste) {

            setTimeout(function () {
              var moneyInd = scope.data.MonetaryAmount.indexOf(money);

              var moneyData = appState.data.transaction.MonetaryAmount[moneyInd];

              if (_.isUndefined(moneyData.ImportExport)) moneyData.ImportExport = [];
              _.each(iePaste, function (item) {
                moneyData.ImportExport.push(item);
              });

              initData(appState.data.transaction, appState.data.customData);

            }, 100)


          }

          scope.selectImportExportAmount = function (selectedIndex) {
            scope.currentImportExportInstance = selectedIndex;
          }

          scope.copyAddress = function (sourceAddress, destAddress) {
            _.each(destAddress, function (item, key) {
              destAddress[key].val = sourceAddress[key].val;
            })
            app.validate();
          }

          /*
           We need to keep a cache of resident and nonResident data for the resType switch since validation existence checks rely on the data model.
           */
          var resTypeCache = {
            resident: {},
            nonResident: {}
          }

          function accountHolderTypeSwapper(cacheFieldName, dataFieldName, strCurrentType, strChangeType) {
            var model = scope.data;
            var data = mainContextLink.getData();

            var returnType = strCurrentType;

            if (strChangeType != returnType) {
              try {
                if (strCurrentType != 'none') {
                  // update the cache
                  data[dataFieldName] = data[dataFieldName] ? data[dataFieldName] : {};
                  resTypeCache[cacheFieldName][strCurrentType] = data[dataFieldName][strCurrentType];
                }

                if (strChangeType != 'none') {
                  returnType = strChangeType;

                  // update the data model
                  data[dataFieldName] = {};

                  data[dataFieldName][strChangeType] = resTypeCache[cacheFieldName][strChangeType]
                    ? resTypeCache[cacheFieldName][strChangeType]
                    : {};

                  // point to correct model
                  scope[dataFieldName] = model[dataFieldName][strChangeType];


                }

                initData(appState.data.transaction, appState.data.customData);
              } catch (e) {
                alert(e);
              }
            }

            return returnType;
          }

          scope.updateResType = function (resType) {
            scope.resType = accountHolderTypeSwapper('resident', 'Resident', scope.resType, resType);
          }

          scope.updateNonResType = function (nonResType) {
            var data = appState.data.transaction;

            var oldType = scope.nonResType;

            if (oldType == nonResType) return;

            scope.nonResType = accountHolderTypeSwapper('nonResident', 'NonResident', scope.nonResType, nonResType);

            //copy addess and everything else we can...
            data.NonResident[nonResType].Address = {};
            if (resTypeCache.nonResident[oldType]) {
              _.extend(data.NonResident[nonResType].Address, resTypeCache.nonResident[oldType].Address);

              //name and account number
              data.NonResident[nonResType].AccountNumber = resTypeCache.nonResident[oldType].AccountNumber;

              //Account Identifier
              data.NonResident[nonResType].AccountIdentifier = resTypeCache.nonResident[oldType].AccountIdentifier;

            }

            if (nonResType == 'Entity') {
              data.NonResident[nonResType].EntityName = appState.data.customData.OrderingCustomerName;
            }

            app.validate();
          }

          scope.getThirdPartyType = function (money) {
            var moneyInd = scope.data.MonetaryAmount.indexOf(money);
            var moneyData = appState.data.transaction.MonetaryAmount[moneyInd];

            return moneyData.ThirdParty ? (
              moneyData.ThirdParty.Entity ? "Entity" :
                (moneyData.ThirdParty.Individual ? "Individual" : "None")
            ) : "None";
          }

          scope.updateThirdPartyType = function (money, type) {
            var moneyInd = scope.data.MonetaryAmount.indexOf(money);
            var moneyData = appState.data.transaction.MonetaryAmount[moneyInd];
            if (type != 'None') {
              moneyData.ThirdParty = {};
              moneyData.ThirdParty[type] = {};
            } else {
              delete moneyData.ThirdParty;
            }
          }

          scope.importExportTotal = function (ie, currency) {
            var total = _.reduce(ie, function (memo, item) {
              if (item.PaymentValue && item.PaymentValue.val)
                memo += Number(item.PaymentValue.val);
              return memo;
            }, 0.0);
            return $filter('currency')(total, sumMoneyCurrency(scope.data, currency));
          }

          scope.importExportOutstanding = function (money, ie, overaCopy, underaCopy, currency) {
            var flowCurrency = scope.bopdata.getTransactionField('FlowCurrency');
            var useLocalValue = currency == contextLinker.map('LocalCurrency') && flowCurrency != contextLinker.map('LocalCurrency');

            var total = _.reduce(ie, function (memo, item) {
              if (item.PaymentValue && item.PaymentValue.val)
                memo += Number(item.PaymentValue.val);
              return memo;
            }, 0.0);
            var out = '';

            var calcTotal = sumMoney(money) - total;
            var sum = useLocalValue ? sumLocalValue(money) : sumMoney(money);
            var calcTotal = sum - total;

            if (calcTotal != 0)
              out = (calcTotal < 0 ? overaCopy : underaCopy) + $filter('currency')(Math.abs(calcTotal), sumMoneyCurrency(scope.data, currency));

            return out;
          }

          scope.importExportOutstandingTotal = function (money, ie, prefixCopy, overaCopy, underaCopy, currency) {

            var out = (prefixCopy ? prefixCopy : 'Total: ') + $filter('currency')(sumMoney(money), sumMoneyCurrency(scope.data, currency));

            var append = scope.importExportOutstanding(money, ie, (overaCopy ? overaCopy : 'Overallocation: '), (underaCopy ? underaCopy : 'Unallocated: '));

            out += (append.length > 0) ? ', ' + append : '';

            return out;
          }

          scope.moneyTotal = function (data, currency) {
            var total = totalMoney(data.MonetaryAmount);
            return $filter('currency')(total, sumMoneyCurrency(data, currency));
          }

          scope.moneyOutstandingTotal = function (data, trgtTtl, currency) {
            var out = scope.moneytOutstanding(data, ', Overallocation: ', ', Unallocated: ', trgtTtl, currency);

            if (out === '')
              out = 'Total: ' + $filter('currency')(trgtTtl ? trgtTtl : data.TotalForeignValue.val, sumMoneyCurrency(data, currency));

            return out;
          }

          scope.moneytOutstanding = function (data, overaCopy, underaCopy, trgtTtl, currency) {
            var out = '';

            var calcTotal = targetTotalMoney(data, trgtTtl);

            if (calcTotal.toFixed(2) != 0)
              out = (calcTotal < 0 ? overaCopy : underaCopy) + $filter('currency')(Math.abs(calcTotal), sumMoneyCurrency(data, currency));

            return out;
          }

          var docProps = ['code', 'event', 'field', 'instance', 'moneyInstance']
          function _compareDocs(doc1, doc2) {
            for (var i in docProps) {
              var propName = docProps[i];
              if (doc1[propName] != doc2[propName])
                return false;
            }
            return true;
          }

          function _removeAtInd(arr, i) {
            return arr.slice(0, i).concat(arr.slice(i, arr.length - 1));
          }

          scope.addDocument = function () {
            if (!scope.customData.docList) {
              scope.customData.docList = [];
            }
            scope.customData.docList.push({
              'code': "Adhoc document", 'event': "adhoc", 'type': "DOCUMENT",
            })
          }

          scope.removeDocument = function (doc) {
            if (scope.customData.docList) {
              var ind = scope.customData.docList.indexOf(doc);
              if (ind != -1) {
                if (scope.customData.docList[ind].event == 'adhoc') {
                  scope.customData.docList.splice(ind, 1);
                } else {
                  delete scope.customData.docList[ind].file;
                }
              }
            }
          }


          scope.hasRequiredDocs = function () {
            var requiredDocs = mainContextLink.getData().getRaisedEvents(true).filter(function (itm) {
              return itm.type === "DOCUMENT";
            });
            return !!requiredDocs.length;
          }

          scope.hasUploadedDoc = function () {
            if (!mainScope.customData.docList) {
              return false;
            } else {
              var uploaded = mainScope.customData.docList.filter(function (itm) {
                return itm.file ? itm.file.success : false;
              })
              return uploaded.length ? true : false;
            }
          }

          scope.getRequiredDocuments = function () {
            var list = mainContextLink.getRaisedEvents(true).filter(function (item) {
              return item.type == "DOCUMENT";
            });

            var context = mainContextLink.getData();
            var list = list.map(function (item) {

              item.value =
                (item.event == "transaction" ? context.getTransactionField(item.field) :
                  item.event == "money" ? context.getMoneyField(item.instance, item.field) :
                    item.event == "importexport" ? context.getImportExportField(item.moneyInstance, item.instance, item.field) : undefined);

              return item;
            })


            if (!scope.customData.docList) {
              scope.customData.docList = [];
            }

            // remove
            for (var doc1, docInd = scope.customData.docList.length - 1; doc1 = scope.customData.docList[docInd]; docInd--) {
              var found = list.find(function (doc2) {
                return _compareDocs(doc1, doc2)
              })
              if (!found && scope.customData.docList[docInd].event != "adhoc") {
                scope.customData.docList =
                  _removeAtInd(scope.customData.docList, docInd);
              }
            }

            //add and update
            for (var doc1, docInd = 0; doc1 = list[docInd]; docInd++) {
              var found = scope.customData.docList.find(function (doc2) {
                return _compareDocs(doc1, doc2)
              })
              if (!found) {
                scope.customData.docList.push(doc1);
              } else {
                //update value
                found.value = doc1.value;
              }
            }



            return scope.customData.docList;


          }

          scope.isSectionInError = function (section, subSection, money, ie) {

            var errors = mainContextLink.getRaisedEvents(true).filter(function (item) {
              return item.type == "ERROR" || item.type == "FAILBUSY";
            });

            var foundSection = _.find(errors, function (error) {
              if (!_.isUndefined(money)) {
                if (!_.isUndefined(ie)) {
                  if (_.isUndefined(error.instance) || _.isUndefined(error.moneyInstance)) return false;
                  if (_.isUndefined(scope.data.MonetaryAmount[error.moneyInstance])) return false;
                  if (error.moneyInstance !== scope.data.MonetaryAmount.indexOf(money)) return false;
                  if (error.instance !== scope.data.MonetaryAmount[error.moneyInstance].ImportExport.indexOf(ie)) return false;
                } else {
                  var instance = error.moneyInstance ? error.moneyInstance : error.instance;
                  if (_.isUndefined(instance)) return false;
                  if (_.isUndefined(scope.data.MonetaryAmount[instance])) return false;
                  if (instance !== scope.data.MonetaryAmount.indexOf(money)) return false;
                }
              }

              var errorSection = fieldMap.sectionFromField(error.field);
              return (!!errorSection
                && (errorSection.detail.section == section)
                && (errorSection.detail.subSection == subSection)
              )
            });

            return (!!foundSection);
          }

          scope.hasFieldValue = function (field) {
            processed = '';
            path = typeof (field) == 'string' ? field.split('.') : null;
            ctx = appState.data;
            for (branch in path) {
              if (processed != field) {
                try {
                  if (typeof (path[branch]) == 'string') {
                    processed += (processed ? '.' : '') + path[branch];
                    ctx = ctx[path[branch]];
                  } else if (!ctx[path[branch]]) {
                    return false;
                  }
                  else {
                    return false;
                  }
                } catch (err) {
                  console.warn("Failed to follow branch (" + path[branch] + ") of path provided (" + field + ") - " + err.message);
                  return false;
                }
              } else if (typeof (ctx) == "string") {
                return true;
              }
              else {
                return false;
              }
            }
          }

          scope.isFieldInError = function (field) {
            var errors = mainContextLink.getRaisedEvents(true).filter(function (item) {
              return item.type == "ERROR" || item.type == "FAILBUSY";
            });

            var found = _.find(errors, function (error) {

              if (error.field == field) {
                return true;
              }
            });

            return found;
          }

          scope.anyMoneyErrors = function () {
            return scope.data.MonetaryAmount.reduce(function (memo, money) {
              memo = memo || scope.isSectionInError('Money', 'general', money)
                || scope.isSectionInError('Money', 'thirdparty', money)
                || scope.isSectionInError('Money', 'contact', money)
                || scope.isSectionInError('Money', 'contactStreet', money)
                || scope.isSectionInError('Money', 'contactPostal', money);
              if (money.ImportExport) {
                memo = memo || money.ImportExport.reduce(function (_memo, ie) {
                  _memo = _memo || scope.isSectionInError('Money', 'importExport', money, ie);
                  return _memo;
                }, false);
                memo = memo || money.ImportExport.error;
              }
              return memo;
            }, false)
          }

          var sectionMap = {
            'Summary': 0,
            'Transaction': 1,
            'Resident': 2,
            'NonResident': 3,
            'Money': 4
          }
          var subSectionMap = {
            'Summary': { general: 0 },
            'Transaction': { general: 0 },
            'Resident': {
              general: 0,
              account: 1,
              contact: 2
            },
            'NonResident': {
              general: 0,
              account: 1,
              contact: 2
            },
            'Money': {
              general: 0,
              thirdparty: 1,
              contact: 2,
              importExport: 3
            }
          }

          scope.tab = 0;
          scope.tabNonResident = 0;
          scope.tabResident = 0;
          scope.tabMoney = 0;


          scope.openDetail = function (section, subsection, money, ie) {
            // this should be the only difference between tabbed and modal approaches.
            if (scope.config.useTabs) {
              mainScope.data.MonetaryAmount.forEach(function (itm) {
                itm.isTabOpen = false;
              });
              if (money) {
                money.isAccGrpOpen = true;
                scope.current.moneyInd = mainScope.data.MonetaryAmount.indexOf(money);
                scope.current.money = money;
              }
              scope.tab = sectionMap[section];

              setTimeout(function () { // this is a hack to get the angular-ui accordion to display.
                scope["tab" + section] = subSectionMap[section][subsection];
                scope.$digest();
              }, 100);
            } else {
              // var detailTemplate = getDetailTemplate(section, subsection);

              var sectionPath = config.partialsPath + section.toLowerCase() + '/';

              var modalInstance = helpers.modalHelper($modal, $log, config.partialsPath, function () {
                var locals = {
                  header: subsection == 'general' ? sectionPath + 'header.html' : null,
                  body: config.partialsPath + 'modalTypeInner.html',
                  footer: config.partialsPath + 'modalFooter.html',
                  partialsPath: config.partialsPath,
                  heading: subsection + " Details",
                  section: section,
                  subsection: subsection,
                  dataCopy: JSON.parse(JSON.stringify(appState.data.transaction)),
                  getDetailTemplate: getDetailTemplate,
                };

                var moneyInd = money ? scope.data.MonetaryAmount.indexOf(money) : -1;
                var ieInd = ie ? money.ImportExport.indexOf(ie) : -1;

                initData(locals.dataCopy, appState.data.customData);

                locals.money = money ? scope.data.MonetaryAmount[moneyInd] : undefined;
                locals.ie = ie ? locals.money.ImportExport[ieInd] : undefined;

                return locals;
              });
            }
          }

          scope.openImportExport = function (money, ie) {
            scope.openDetail('Money', 'importExport', money, ie);
          }

          scope.openImportExportSummary = function (money) {

            if (scope.config.useTabs) {
              scope.current.moneyInd = scope.data.MonetaryAmount.indexOf(money);
              scope.current.money = money;
              scope.tab = { Money: { active: true } };
              setTimeout(function () { // this is a hack to get the angular-ui accordion to display.
                scope.tab.Money.importExport = true;
                scope.$digest();
              }, 100);
            } else {
              var sectionPath = config.partialsPath;

              var modalInstance = helpers.modalHelper($modal, $log, sectionPath, function () {
                var locals = {
                  header: sectionPath + 'header.html',
                  body: sectionPath + config.importexport_summary,
                  footer: config.partialsPath + 'modalFooter.html',
                  dataCopy: JSON.parse(JSON.stringify(appState.data.transaction)),
                  openImportExport: function (money, ie) {
                    var detailTemplate = getDetailTemplate('Money', 'importExport');

                    var sectionPath = config.partialsPath;

                    helpers.modalHelper($modal, $log, sectionPath, function () {
                      return {
                        header: sectionPath + 'header.html',
                        body: sectionPath + detailTemplate,
                        footer: config.partialsPath + 'modalFooter.html',
                        ie: ie
                      };
                    });
                  },
                  ok: function () {
                    appState.data.transaction = locals.dataCopy;
                    modalInstance.close();
                  }
                };

                var moneyInd = money ? scope.data.MonetaryAmount.indexOf(money) : -1;

                initData(locals.dataCopy, appState.data.customData);

                locals.money = money ? scope.data.MonetaryAmount[moneyInd] : undefined;

                return locals;
              });
            }
          };

          scope.isReadOnly = function () {
            return isReadOnly;
          };

          //error cache - caching mechanism to  
          scope.isModelInError = function (model) {
            if (app.config.snapShotValidate && model && model.scope == 'transaction') {
              var snapShotField = snapShotContextLink.getData().resolveField(model.property);
              if (snapShotField)
                return snapShotField.error || model.error || (model.hadError && (!_.isUndefined(model.val)));
            }

            return model ?
              (model.error ? model.error : (model.hadError && (!_.isUndefined(model.val))))
              : false;
          }

          scope.errMsgCopyOnly = function (msg) {
            if (!msg) {
              return "";
            }

            var msgs = _.map(msg, function (str) {
              var errMsgCopyPrfx = new RegExp(/[^\:]*\:/);

              if (str) {
                //strip 'ERROR XX:', 'WARNING YY:' etc.
                return str.replace(errMsgCopyPrfx, "");
              }
              return "";
            });

            return msgs.filter(function (value, index, self) {
              return self.indexOf(value) === index;
            });
          };

          /*=================================================================
          Start here....
          ==================================================================*/
          if (scope.linkedData) {
            setData(scope.linkedData);
            scope.$watch('linkedData', function (newValue, oldValue) {
              if (newValue != oldValue)
                setData(scope.linkedData);
            });
          } else if (appState.initData) {

            setData(appState.initData);

          } else {
            setData({});
          }

          scope.bopdata = mainContextLink.getData();


        },
        templateUrl: config.partialsPath + (config.useTabs ? config.rootTabTemplate : config.rootTemplate)
      };
    }]);

    return app;
  })


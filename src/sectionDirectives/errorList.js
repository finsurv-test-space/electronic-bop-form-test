/**
 * Created by petruspretorius on 03/12/2015.
 */
define(['require', '../app', '../helpers', '../fieldMap'], function (require, app, helpers, fieldMap) {

  app.directive('errorList', ['$uibModal', '$log', '$rootScope', function ($modal, $log, $rootScope) {
    return {
      restrict: 'E',
      scope: {
        section: '@',
        openDetail : '='
      },
      templateUrl: app.config.fieldPartialsPath
        ? app.config.partialsPath + 'errorSummary.html'
        : 'errorSummary.html',
      link: function (scope, element, attrs) {

        var elem = element.find('a');
        elem.on('click', function(e){
          e.preventDefault();
          e.stopPropagation();
        });

        var bopdata = scope.bopdata = app.contextLink.getData();

        //We need to check if the data has changed...
        scope.$watch(
          // This function returns the value being watched. It is called for each turn of the $digest loop
          function() { return app.contextLink.getData(); },
          // This is the change listener, called when the value returned from the above function changes
          function(newValue, oldValue) {
            if ( newValue !== oldValue ) {
              // Only increment the counter if the value changed
              bopdata = scope.bopdata = app.contextLink.getData();
            }
          }
        );

        scope.openErrors = function (section) {
          bopdata = scope.bopdata;// = app.contextLink.getData();
          var errors = _.filter(bopdata.getRaisedEvents(), function (item) {
            return !((item.type === 'BUSY') || (item.type === 'SUCCESS') || (item.type === 'DOCUMENT'));
          });
          if (errors.length == 0)
            return;
          var replaceStr = [
            {
              match: "^Resident",
              replace: "Account Holder"
            },
            {
              match: "^NonResident",
              replace: bopdata.Flow == "IN" ? "Remitter" : "Beneficiary"
            }
          ];
          var _errors = section ? _.filter(errors, function (error) {
            var matchSection = fieldMap.sectionFromField(error.field);
            return matchSection? matchSection.detail.section == section : false;
          }) : errors;

          var _errors = _errors.map(function (obj) {
            var pClass = ((obj.type == 'WARNING') || (obj.type == 'DEPRECATED'))
              ? 'text-warning'
              : 'text-danger';

            var vwMoneyIndx = (!_.isUndefined(obj.moneyInstance) ? obj.moneyInstance : obj.instance) + 1;
            var vwImpExIndx = (obj.instance) + 1;


            function getFieldLabels(fieldName, mInd, iInd) {
              var fieldParts = fieldName.split('.');
              var str = "", out = [];
              _.forEach(fieldParts,function (fieldPart) {
                str += fieldPart;
                var field = app.contextLink.resolveField(str, mInd, iInd);
                //If we cannot resolve it, it doesn't exist, so just pass the fieldName
                if (_.isUndefined(field) || field == null)
                  return fieldName;
                out.push(field.label ? field.label : fieldPart);
                str += ".";
              })
              return out.join('/')
            }

            if (obj.event == 'transaction') {
              var fieldName = getFieldLabels(obj.field);
              var e = {
                obj: obj,
                pClass: pClass,
                type: obj.type,
                msg: obj.msg,
                fieldName: fieldName
              };
              return e;
            } else if (obj.event == 'money') {
              var fieldName = getFieldLabels(obj.field, obj.instance);
              fieldName = " MonetaryAmount[" + vwMoneyIndx + "]/" + fieldName;
              var e = {
                obj: obj,
                pClass: pClass,
                type: obj.type,
                msg: obj.msg,
                fieldName: fieldName,
                moneyInd: obj.instance
              }

              return e;
            } else if (obj.event == 'importexport') {
              var fieldName = getFieldLabels(obj.field, obj.moneyInstance, obj.instance);
              fieldName = " MonetaryAmount[" + vwMoneyIndx + "]/ImportExport[" + vwImpExIndx + "]/" + fieldName;
              var e = {
                obj: obj,
                pClass: pClass,
                type: obj.type,
                msg: obj.msg,
                fieldName: fieldName,
                moneyInd: obj.moneyInstance,
                importExportInd: obj.instance
              };
              return e;
            }
          });

          if (_errors.length == 0) return;

          _errors = _.sortBy(_errors, function (item) {
            return item.type;
          });


          var modalInstance = helpers.modalHelper($modal, $log, app.config.partialsPath, function () {
            return {
              heading: "Error/Warning List",
              body: app.config.partialsPath + 'errors.html',
              footer: app.config.partialsPath + 'modalFooter.html',
              errors: _errors,
              focusOnField: function (error) {
                var section = fieldMap.sectionFromField(error.obj.field);
                if (section) {
                  modalInstance.close();
                  var money = !_.isUndefined(error.moneyInd) ? scope.$parent.data.MonetaryAmount[error.moneyInd] : undefined;
                  var ie = !_.isUndefined(error.importExportInd) ? scope.$parent.data.MonetaryAmount[error.moneyInd].ImportExport[error.importExportInd] : undefined;
                  scope.$parent.openDetail(section.detail.section, section.detail.subSection, money, ie);
                }

              }
            };
          });
        };

        // expose this functionality to the API
        app.showErrors = function () {
          scope.openErrors();
        };
      }
    }
  }])


})
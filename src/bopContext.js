Array.prototype.count = function (fn) {
  return _.reduce(this, function (prev, itm) {
    var cnt = prev;
    if (fn(itm)) {
      cnt++;
    }
    return cnt;
  }, 0);
}

Array.prototype.any = function (fn) {
  return this.count(fn) > 0;
};
Array.prototype.all = function (fn) {
  return this.count(fn) === this.length;
};
Array.prototype.none = function (fn) {
  return this.all(function (itm) {
    return !fn(itm);
  });
};

define(['require', './fieldMap'], function (require, formFieldMap) {


  function produceContext(obj, model, _digestFn, _customData, lookups) {

    var customData = _customData;
    var globalContext = obj;
    var modelContext = model;
    var digestFn = _digestFn;

    var raisedEvents = [];
    var persistedEvents = [];

    var isValid = true;

    var hasWarning = false;


    function _getDataByField(scope, obj, field) {
      if (!obj)
        return null;
      var fieldParts = field.split('.');
      var localObj = obj;
      for (var i = 0; i < fieldParts.length; i++) {
        var fieldPart = fieldParts[i];
        if (fieldPart in localObj) {
          localObj = localObj[fieldPart];
        } else {
          localObj = null;
        }

        if (localObj === '')
          return localObj;

        if (_.isUndefined(localObj) || (localObj === null)) {
          if (i >= fieldParts.length - 1)
            return null;
          else {
            if (scope === 'transaction' && (field.indexOf('Resident.') == 0 || field.indexOf('NonResident.') == 0) && i < 2)
              return undefined;
            else
              return null;
          }
        }
      }
      if (_.isNumber(localObj))
        return localObj.toString();
      else
        return localObj;
    }

    function getDataByField(scope, obj, field) {
      var val = _getDataByField(scope, obj, field);
      return val;// ? val : undefined;
    }

    function getScopedObject(rootContext, scope, moneyInd, ImportExportInd) {
      var obj = null;
      var moneyList;
      if (scope === "transaction") {
        obj = rootContext;
      }
      else if (scope === "money" && !_.isUndefined(moneyInd)) {
        moneyList = rootContext.MonetaryAmount;
        if (moneyList) {
          obj = moneyList[moneyInd];
        }
      }
      else if (scope === "importexport" && !_.isUndefined(moneyInd) && !_.isUndefined(ImportExportInd)) {
        moneyList = rootContext.MonetaryAmount;
        if (moneyList) {
          var money = moneyList[moneyInd];
          if (money) {
            var importList = money.ImportExport;
            if (importList) {
              obj = importList[ImportExportInd];
            }
          }
        }
      }
      return obj;
    }

    function setDataByField(scope, field, moneyInd, ImportExportInd, value) {
      if (formFieldMap.hasScopedField(scope, field)) {
        var obj = getScopedObject(globalContext, scope, moneyInd, ImportExportInd);

        if (!obj)
          return null;

        var fieldParts = field.split('.');
        var localObj = obj;
        for (var i = 0; i < fieldParts.length; i++) {
          var fieldPart = fieldParts[i];
          if (fieldPart in localObj) {
            if (i === fieldParts.length - 1) {
              localObj[fieldPart] = value;
              return;
            }
          } else {
            if (i === fieldParts.length - 1) {
              localObj[fieldPart] = value;
              return;
            }
            else {
              localObj[fieldPart] = {};
            }
          }
          localObj = localObj[fieldPart];
        }
      }
    }


    function resolveField(field, moneyInd, ImportExportInd) {
      if (!_.isUndefined(ImportExportInd) && ImportExportInd >= 0) {
        var scopeObj = getScopedObject(modelContext, 'importexport', moneyInd, ImportExportInd);
        return _getDataByField('importexport', scopeObj, field);
      }
      if (!_.isUndefined(moneyInd) && moneyInd >= 0) {
        var scopeObj = getScopedObject(modelContext, 'money', moneyInd, ImportExportInd);
        return _getDataByField('money', scopeObj, field);
      }

      return _getDataByField('transaction', modelContext, field);
    }


    function resolveOrCreateField(field, moneyInd, ImportExportInd) {
      var _field = resolveField(field, moneyInd, ImportExportInd);
      if (_.isUndefined(_field) || (_field == null)) {
        var scopeModel;
        if (!_.isUndefined(ImportExportInd)) {
          scopeModel = getScopedObject(modelContext, 'importexport', moneyInd, ImportExportInd);
        } else if (!_.isUndefined(moneyInd)) {
          scopeModel = getScopedObject(modelContext, 'money', moneyInd, ImportExportInd);
        } else {
          scopeModel = modelContext;
        }

        if (!scopeModel)
          return;
        var fieldParts = field.split('.');
        // var localObj = scopeObj;
        var localModel = scopeModel;
        for (var i = 0; i < fieldParts.length; i++) {
          var fieldPart = fieldParts[i];
          if (fieldPart in localModel) {
            if (i === fieldParts.length - 1) {
              localModel[fieldPart] = {val: null};//makeModelLeaf(scope,scopeObj,field,moneyInd,ImportExportInd);
              return;
            }
            // localObj = localObj[fieldPart];
            localModel = localModel[fieldPart];
          } else {
            if (i === fieldParts.length - 1) {
              localModel[fieldPart] = {val: null};//makeModelLeaf(scope,scopeObj,field,moneyInd,ImportExportInd);
            }
            else {
              //localObj[fieldPart] = {};
              //localObj = localObj[fieldPart];
              localModel[fieldPart] = {};
              localModel = localModel[fieldPart];
            }
          }
        }
        _field = localModel[fieldPart];

      }
      return _field;
    }

    function disableField(scope, field, moneyInd, ImportExportInd) {
      var _field = resolveOrCreateField(field, moneyInd, ImportExportInd);
      if (_field) {
        _field.disabled = true;
      }
    }

    function enableField(scope, field, moneyInd, ImportExportInd) {
      var _field = resolveField(field, moneyInd, ImportExportInd);
      if (_field && _field.disabled) {
        delete _field.disabled;
      }
    }


    function setField(scope, field, composedSetting, moneyInd, ImportExportInd) {
      var _field = resolveOrCreateField(field, moneyInd, ImportExportInd);
      if (_field) {
        _field.val = typeof composedSetting == 'function'?
          composedSetting( globalContext, moneyInd, ImportExportInd ,modelContext, _field)
          : composedSetting;
      }
    }

    function setFieldLabel(scope, field, composedSetting, moneyInd, ImportExportInd) {
      var _field = resolveOrCreateField(field, moneyInd, ImportExportInd);
      if (_field) {
        _field.label = composedSetting;
      }
    }

    function clearField(scope, field, moneyInd, ImportExportInd) {
      var _field = resolveField(field, moneyInd, ImportExportInd);
      if (_field) {
        _field.val = undefined;
      }
    }

    function appendField(scope, field, composedSetting, moneyInd, ImportExportInd) {
      var _field = resolveOrCreateField(field, moneyInd, ImportExportInd);
      if (_field) {
        _field.val = _field.val + ", " + composedSetting;
      }

    }

    function showHide(type, scope, field, moneyInd, ImportExportInd) {

      var fn = type.toLowerCase();
      var _field = fn === 'hide' ? resolveField(field, moneyInd, ImportExportInd) : resolveOrCreateField(field, moneyInd, ImportExportInd);

      if (_field) {
        // don't hide fields with values... meaning "hard-hidden" fields should just not be in
        // templates.
        /*if (fn === 'hide' && _field.val) {
         if (_field.hide) delete _field.hide;
         return;
         } else*/
        if (fn === 'hide') {
          _field.hide = true;
        } else {
          delete _field.hide;
        }
      }
    }

    function addError(field, t, persistent) {
      if (!field)
        return;
      var type = t.toLowerCase();

      if(type=="failbusy"){
        field.error=true;
        type="busy";
      }

      if (field.busy && (type !== 'busy')) {
          delete field.busy;
          delete field.error;

        var msgs = persistent
          ? field.pmsg
          : field.msg;

        var collection = _.filter(msgs, function (msgItem) {
          return msgItem.indexOf("ERROR") == 0 || msgItem.indexOf("WARNING") == 0 /*|| msgItem.indexOf("FAILBUSY")==0*/;
        });

        if (persistent) {
          field.pmsg = collection;
        } else {
          field.msg = collection;
        }

        // digestFn ? digestFn() : null;
      }

      if (_.isUndefined(field[type])) field[type] = true;

      // persist error state for complex display requirements.
      if (field.error){
        field.hadError = true;
      }
    }

    function addMsg(field, msg, persistent) {
      if (!field)
        return;

      if (persistent)
        field.pmsg = fldPropArrVal(field.pmsg, msg);
      else
        field.msg = fldPropArrVal(field.msg, msg);
    }

    function fldPropArrVal(fieldprop, val) {
      if (_.isUndefined(fieldprop)) {
        fieldprop = [];
      }

      if (!(val in fieldprop)) {
        fieldprop.push(val);
      }

      return fieldprop;
    }

    function formatString(format) {
      var str = format;
      for (var i = 1; i < arguments.length; i++) {
        str = str.replace('{' + (i - 1) + '}', (arguments[i]?arguments[i]:""));
      }
      return str;
    }


    function transactionMsg(persist, type, field, code, msg, hintStr) {
      var msgStr = formatString(hintStr, type, code, msg);
      var _field = resolveOrCreateField(field);
      if (!_field)
        return;
      if(type!=='MANDATORY')
        addMsg(_field, msgStr, persist);
      addError(_field, type, persist);
    }


    function monetaryMsg(persist, type, instance, field, code, msg, hintStr) {
      var msgStr = formatString(hintStr, type, code, msg);
      var _field = resolveOrCreateField(field, instance);
      if (!_field)
        return;
      if(type!=='MANDATORY')
        addMsg(_field, msgStr, persist);
      addError(_field, type, persist);
    }

    function importExportMsg(persist, type, moneyInstance, instance, field, code, msg, hintStr) {

      var msgStr = formatString(hintStr, type, code, msg);

      var _field = resolveOrCreateField(field, moneyInstance, instance);
      if (!_field)
        return;

      if(type!=='MANDATORY')
        addMsg(_field, msgStr, persist);
      addError(_field, type, persist);
    }

    function excludeFieldLookup(scope, field, composedSetting, moneyInstance, importExportInstance) {
      if (_.isUndefined(_exports.filteredLookups[field])) _exports.filteredLookups[field] = [];
      _exports.filteredLookups[field].push({
        exclude: composedSetting,
        moneyInstance: moneyInstance,
        importExportInstance: importExportInstance
      });
    };

    function limitFieldLookup(scope, field, composedSetting, moneyInstance, importExportInstance) {
      if (_.isUndefined(_exports.filteredLookups[field])) _exports.filteredLookups[field] = [];
      _exports.filteredLookups[field].push({
        limit: composedSetting,
        moneyInstance: moneyInstance,
        importExportInstance: importExportInstance
      });
    };

    function filterFieldLookup(scope, field, composedSetting, moneyInstance, importExportInstance) {
      if (_.isUndefined(_exports.filteredLookups[field])) _exports.filteredLookups[field] = [];
      _exports.filteredLookups[field].push({
        filter: composedSetting,
        moneyInstance: moneyInstance,
        importExportInstance: importExportInstance
      });
    };

    //m = modelContext;
    //d = globalContext;

    var hintStr = formFieldMap.errorMask;
    if (!hintStr) {
      hintStr = "{0} {1} : {2}";
    }

    obj.buttonIsClicked = function (field, currentMoneyInstance, currentImportExportInstance) {
      var _field = resolveField(field, currentMoneyInstance, currentImportExportInstance);

      var isClicked = _field && ("clicked" in _field) && _field.clicked;

      if (_field) {
        _field.clicked = false;
      }

      return isClicked;
    };

    obj.fieldHasFocus = function (field, currentMoneyInstance, currentImportExportInstance) {
      var _field = resolveField(field, currentMoneyInstance, currentImportExportInstance);
      return _field && ("focus" in _field) && _field.focus;
    };

    obj.getTransactionField = function (field) {
      return getDataByField('transaction', obj, field);
    };

    obj.getMoneySize = function () {
      var moneyList = obj.MonetaryAmount;
      if (moneyList)
        return moneyList.length;
      return 0;
    };

    obj.getMoneyField = function (instance, field) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList)
        return getDataByField('money', moneyList[instance], field);
      return null;
    };

    obj.getImportExportSize = function (moneyInstance) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList) {
        var money = moneyList[moneyInstance];
        var importList = money.ImportExport;
        if (importList) {
          return importList.length;
        }
      }
      return 0;
    };

    obj.getImportExportField = function (moneyInstance, instance, field) {
      var moneyList = obj.MonetaryAmount;
      if (moneyList) {
        var money = moneyList[moneyInstance];
        var importList = money.ImportExport;
        if (importList) {
          return getDataByField('importexport', importList[instance], field);
        }
      }
      return null;
    };

    function setEventValidAndWarning(type) {
      if (type === "ERROR" || type === "FAILBUSY")
        isValid = false;
      if (type === "WARNING")
        hasWarning = true;
    }

    obj.logTransactionEvent = function (type, field, code, msg, persist) {
      var unmanaged = persist ? persist : false;
      setEventValidAndWarning(type);

      transactionMsg(unmanaged, type, field, code, msg, hintStr);

      
      if(type!=='MANDATORY') {
        var evt = {
          event: 'transaction',
          type: type,
          field: field,
          code: code,
          msg: msg
        };
        pushEvent(evt, unmanaged);
      }
    };

    obj.logMoneyEvent = function (type, instance, field, code, msg, persist) {
      var unmanaged = persist ? persist : false;
      setEventValidAndWarning(type);

      monetaryMsg(unmanaged, type, instance, field, code, msg, hintStr);

      if(type!=='MANDATORY'){
        var evt = {
          event: 'money',
          instance: instance,
          type: type,
          field: field,
          code: code,
          msg: msg
        };
        pushEvent(evt, unmanaged);
      }

    };

    obj.logImportExportEvent = function (type, moneyInstance, instance, field, code, msg, persist) {
      var unmanaged = persist ? persist : false;
      setEventValidAndWarning(type);

      importExportMsg(unmanaged, type, moneyInstance, instance, field, code, msg, hintStr);

      if(type!=='MANDATORY') {
        var evt = {
          event: 'importexport',
          moneyInstance: moneyInstance,
          instance: instance,
          type: type,
          field: field,
          code: code,
          msg: msg
        };
        pushEvent(evt, unmanaged);
      }
    };

    function pushEvent(evt, unmanaged) {
      //raisedEvents.push(evt);
      if (!unmanaged) {
        raisedEvents.push(evt);
        if (["ERROR", "WARNING", "SUCCESS"].indexOf(evt.type) > -1){
          raisedEvents = _.filter(raisedEvents, function(itm){
            return !(itm.field === evt.field 
              && itm.type == "FAILBUSY" 
              && itm.instance == evt.instance
              && itm.moneyInstance == evt.moneyInstance);
          });
        }
      } else {
        var comparer = {
          code: evt.code,
          event: evt.event,
          field: evt.field
        };
        if (evt.instance !== undefined) {
          comparer["instance"] = evt.instance;
        }
        if (evt.moneyInstance !== undefined) {
          comparer["moneyInstance"] = evt.moneyInstance;
        }

        persistedEvents = _.filter(persistedEvents, function (itm) {
          return !(itm.field === comparer.field
          && itm.event === comparer.event
          && itm.instance === comparer.instance
          && itm.moneyInstance === comparer.moneyInstance);
        });

        if (["ERROR", "WARNING", "SUCCESS"].indexOf(evt.type) > -1){
          persistedEvents = _.filter(persistedEvents, function(itm){
            if (itm.field === evt.field 
              && itm.type == "FAILBUSY"
              && evt.moneyInstance == itm.moneyInstance){
            }
            return !(itm.field === evt.field 
              && itm.type == "FAILBUSY"
              && evt.moneyInstance == itm.moneyInstance);
          });
        }

        persistedEvents.push(evt);
      }
    };

    var displays = {
      EXTEND: function(){},
      EXCLUDE: excludeFieldLookup,
      LIMIT: limitFieldLookup,
      FILTER: filterFieldLookup,
      SET: setField,
      SETLABEL: setFieldLabel,
      APP: appendField,
      CLEAR: function (scope, field, composedSetting, moneyInstance, instance) {
        return clearField(scope, field, moneyInstance, instance);
      },
      ENABLE: function (scope, field, composedSetting, moneyInstance, instance) {
        return enableField(scope, field, moneyInstance, instance);
      },
      DISABLE: function (scope, field, composedSetting, moneyInstance, instance) {
        return disableField(scope, field, moneyInstance, instance);
      },
      SHOW: function (scope, field, composedSetting, moneyInstance, instance) {
        return showHide('SHOW', scope, field, moneyInstance, instance);
      },
      HIDE: function (scope, field, composedSetting, moneyInstance, instance) {
        return showHide('HIDE', scope, field, moneyInstance, instance);
      }
    }

    obj.logTransactionDisplayEvent = function (type, field, composedSetting) {
      displays[type]("transaction", field, composedSetting);
    };

    obj.logMoneyDisplayEvent = function (type, instance, field, composedSetting) {
      displays[type]("money", field, composedSetting, instance);
    };

    obj.logImportExportDisplayEvent = function (type, moneyInstance, instance, field, composedSetting) {
      displays[type]("importexport", field, composedSetting, moneyInstance, instance);
    };

    function allRaisedEvents() {
      return raisedEvents.concat(persistedEvents);
    }

    obj.getRaisedEvents = function () {
      return allRaisedEvents();
    };

    obj.clearPersistedEvents = function (props) {
      //console.log(persistedEvents);

      for (var j = 0; j < props.length; j++) {
        var pmod = props[j].model;
        var indxs = props[j].indxs;

        var i = persistedEvents.length;
        while (i--) {
          var evt = persistedEvents[i];
          //console.log([evt, "prop", pmod]);

          var isEvtIe = false;
          var isEvtMoney = false;

          if (('moneyInstance' in evt)) {
            isEvtIe = true;
          } else if (('instance' in evt)) {
            isEvtMoney = true;
          }

          var isIndexOfMoney = true;
          var isIndexOfIe = true;

          if (isEvtMoney === true) {
            isIndexOfMoney = !('instance' in evt) || (evt.instance === indxs[0]);
          } else if (isEvtIe === true) {
            isIndexOfMoney = !('moneyInstance' in evt) || (evt.moneyInstance === indxs[0]);
            isIndexOfIe = !('instance' in evt) || (evt.instance === indxs[1]);
          }

          //console.log([indxs, isIndexOfMoney, isIndexOfIe]);

          if (evt.field === pmod.property
            && evt.event === pmod.scope
            && isIndexOfMoney
            && isIndexOfIe) {
            persistedEvents.splice(i, 1);
          }
        }
      }
    };

    obj.clearRaisedEvents = function (isPersistedToo) {
      if (isPersistedToo) {
        persistedEvents = [];
      }
      var hasPeristEvts = persistedEvents.length > 0;
      var isError = function (itm) {
        return itm.type === "ERROR" || itm.type === "FAILBUSY";
      }
      var isWarning = function (itm) {
        return itm.type === "WARNING";
      }

      isValid = hasPeristEvts
        ? !persistedEvents.all(isError)
        : true;

      hasWarning = hasPeristEvts
        ? persistedEvents.any(isWarning)
        : false;

      //isValid = true;
      //hasWarning = false;

      raisedEvents = [];

      _exports.filteredLookups = {};
    };

    obj.clearValidationArtefacts = function (data) {
      data = _.isUndefined(data) ? modelContext : data;
      for (var property in data) {
        if (property.substr(0, 1) === '$') continue; // deal with Angular properties.
        if (['msg', 'hide', 'disabled', 'error', 'warning', 'success', 'fail', 'mandatory', 'failbusy' ].indexOf(property) != -1) {
          delete data[property];
          continue;
        }// delete all meta data TODO: This is starting to smell bad
        else if (['data', 'val'].indexOf(property) != -1) {
          continue;
        }// deal with Internals. TODO: This is starting to smell bad
        var val = data[property];
        if (_.isArray(val) || (_.isObject(val))) {
          if (_.isFunction(val)) continue;
          obj.clearValidationArtefacts(val);
        }
      }
    }

    obj.isValid = function (section) {
      return obj.eventCount('ERROR', section) == 0 && obj.eventCount('FAILBUSY', section) == 0;
    };

    obj.hasWarning = function (section) {
      return section ? (obj.eventCount('WARNING', section) > 0) : hasWarning;
    };

    obj.eventCount = function (t, section) {
      var type = t ? t : 'All';
      var evts = allRaisedEvents();

      //console.log(["eventCount", "evts", evts]);

      return evts
        .reduce(function (memo, itm) {
          if (itm.type == type || type == 'All') {
            if (!section) {
              memo++;
            } else {
              if (formFieldMap.sectionFromField(itm.field))
                if ((section == formFieldMap.sectionFromField(itm.field).detail.section))
                  memo++;
            }
          }
          return memo;
        }, 0);
    };

    obj.resolveField = resolveField;

    obj.setDataByField = setDataByField;

    obj.resolveOrCreateField = resolveOrCreateField;

    var notInternal = function(property) {
      if (property.substr(0, 1) === '$') return true; // deal with Angular properties.
      if (['msg', 'hide', 'disabled', 'error', 'warning', 'success', 'fail'].indexOf(property) != -1) {
        return true;
      }// delete all meta data TODO: This is starting to smell bad
      else if (['data', 'val'].indexOf(property) != -1) {
        return true;
      }// deal with Internals. TODO: This is starting to smell bad

      return false;
    }

    var _clearHiddenFields = function (isErrOnly, data, model, parentHidden) {
      if (_.isUndefined(data)) return 0;

      var cnt = 0;

      for (var property in model) {
        if (notInternal(property)) {
          continue;
        }

        var val = data[property];
        var mod = model[property];

        if (_.isArray(val) && mod.hide) {
          delete data[property];
          mod.splice(0, mod.length);
          cnt++;
        } else if (_.isObject(val)) {
          if (_.isFunction(val)) continue;
          cnt += _clearHiddenFields(isErrOnly, val, mod, mod.hide);
        } else {
          var isDoClear = (mod.hide || parentHidden)
              //&& mod.val
            && mod.data
            && (isErrOnly ? mod.error || mod.warning : true);

          if (isDoClear && mod.val) {
            cnt++;
          }
          if (isDoClear) {
            mod.val = undefined;
          }
        }
      }
      return cnt;
    }

    obj.clearHiddenFields = function (onErrorOnly) {
      return _clearHiddenFields(onErrorOnly, globalContext, modelContext);
    }

    obj.getCustomValue = function(field) {
      if (!(customData && (typeof (customData[field]) !== 'undefined')))
        return undefined;
      return customData[field];
    };

    obj.setCustomValue = function(field, value) {
      if (!(customData && (typeof (customData[field]) !== 'undefined')))
        return undefined;
      customData[field] = value;
    };

    obj.getLookups = function(){
      return lookups;
    }

    return obj;
  };

  var _exports = {
    produce: produceContext//,
    //addError: addError,
    //getDataByField: getDataByField,
    //setDataByField: setDataByField,
    //filteredLookups: {}
  };

  return _exports;

});

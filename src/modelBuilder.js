/**
 * Created by petruspretorius on 15/12/2015.
 */
define(['require', './helpers', './fieldMap'], function (require, helpers, fieldMap) {

  //-----------------------------------------------------------------------------------------
  //function initialises the display model
  //
  //  This function creates a display model tied to the transaction data model, using the
  //   form field map.
  //
  //  This enables us to set display states that angular can react to without adversely affecting
  //   the data model. The display model exposes the data model trough getters and setters on the
  //   leaf node's 'val' property. Any fields affected by the display rules get added
  //   as primitives to the display model and not the data model.
  //
  // This method relies on bopContext's getDataByField and setDataByField.
  //-----------------------------------------------------------------------------------------
  function transformIn(data) {


    function getIEInd(ieData) {

      var money = _.find(data.MonetaryAmount, function (m) {
        if (!(m.ImportExport && m.ImportExport.length > 0)) return false;
        return m.ImportExport.indexOf(ieData) !== -1;
      });

      return [data.MonetaryAmount.indexOf(money), !money ? -1 : money.ImportExport.indexOf(ieData)];

    }

    function getMoneyInd(moneyData) {
      return [data.MonetaryAmount.indexOf(moneyData), -1];
    }

    function getIndeces(model) {
      return model.scope == 'importexport'
        ? getIEInd(model.data)
        : getMoneyInd(model.data);
    }

    function section(model) {
        var sect = fieldMap.sectionFromField(model.property);

        return sect && sect.detail && sect.detail.section
          ? sect.detail.section
          : 'none';
    }

    function setModelValue(localObj,fieldPart,x){
      var oldValue = localObj[fieldPart];
      if(oldValue!=x){

        localObj[fieldPart] = x;
        if (_modelOut.onChangeHandler)_modelOut.onChangeHandler(this, oldValue);
      }
    }

    // This is the function that ties the display model to the data model through the 'val' property's get and set.
    function makeModelLeaf(scope, scopeObj, field) {
      var model = {
        scope: scope,
        data: scopeObj,
        property: fieldMap.map(field),
        getIndeces: function () {
            return getIndeces(model);
        },
        section: function () {
            return section(model);
        },
        get val() {
          var fieldParts = this.property.split('.');
          if ((_.isUndefined(this.data)) || (_.isNull(this.data)))
            return;
          // capture the this.data object reference
          var localObj = this.data;
          //traverse fieldParts to find the actual value
          return fieldParts.reduce(function (localObj, fieldPart) {
            if (!localObj) return;

            return !_.isUndefined(localObj[fieldPart]) ? localObj[fieldPart] : undefined;
          }, localObj);
        },
        set val(x) {
          var fieldParts = this.property.split('.');
          // capture the this.data object reference
          var localObj = this.data;
          if (!localObj) return;//cannot work without a base reference object...

          //traverse fieldParts to find the actual value and set it, autovivificating along the way...
          return fieldParts.reduce(function (localObj, fieldPart, i) {

            //autovivificate if not at the end..
            if ((i !== fieldParts.length - 1) && !localObj[fieldPart]) {
              localObj[fieldPart] = {};
            }
            //at the end set the value
            if (i === fieldParts.length - 1) {
              if ((_.isUndefined(localObj[fieldPart]) && !_.isUndefined(x)) || (localObj[fieldPart] != x)) {//set if different, may set to any type
                setModelValue(localObj, fieldPart, x);
              }
            }
            return localObj[fieldPart];

          }, localObj);//passing in localObj as base ref

        }
      }
      return model;
    }


    // function to autovivificate the display model and initialise the leaf nodes.
    /*
     localObj : the display Model being populated.
     */
    function MakeModelPath(localObj, fieldName, context) {
      var fieldParts = fieldMap.map(fieldName).split('.');
      for (var i = 0; i < fieldParts.length; i++) {
        var fieldPart = fieldParts[i];
        if (fieldPart in localObj) {
          if (i === fieldParts.length - 1) {
            localObj[fieldPart] = makeModelLeaf(context.scope, context.scopeObj, fieldName);
          }
          localObj = localObj[fieldPart];
        } else {
          if (i === fieldParts.length - 1) {
            localObj[fieldPart] = makeModelLeaf(context.scope, context.scopeObj, fieldName);
          }
          else {
            localObj = localObj[fieldPart] = {};
          }
        }
      }
    }


    function addMoneyModel(model, _data) {

      model.MonetaryAmount.push({ImportExport: []});

      var moneyInd = model.MonetaryAmount.length - 1;

      var sections = fieldMap.getSections('money');

      _.each(sections, function (section) {
        _.each(section.fields, function (fieldName) {
          MakeModelPath(model.MonetaryAmount[moneyInd], fieldName, {
            scope: 'money',
            scopeObj: _data.MonetaryAmount[moneyInd]
          });
        });
      });
    }


    function addIEModel(moneyModel, moneyData) {

      moneyModel.ImportExport.push({});

      var ieInd = moneyModel.ImportExport.length - 1;

      var sections = fieldMap.getSections('importexport');

      _.each(sections, function (section) {
        _.each(section.fields, function (fieldName) {
          MakeModelPath(moneyModel.ImportExport[ieInd], fieldName, {
            scope: 'importexport',
            scopeObj: moneyData.ImportExport[ieInd]
          });
        })
      })
    }


    var scopes = ['transaction', 'money', 'importexport'];
    var resType = helpers.getTransactionObjType(data.Resident);
    var nonResType = helpers.getTransactionObjType(data.NonResident);
    var moneyCount = data.MonetaryAmount ? data.MonetaryAmount.length : 0;

    // initialise the display model structure.
    var _modelOut = {
      addMoneyModel: addMoneyModel,
      addIEModel: addIEModel,
      Resident: {},
      NonResident: {},
      MonetaryAmount: [],
      onChange: function (handler) {
        _modelOut.onChangeHandler = handler;
      }
    };

    _modelOut.Resident[resType] = {};
    _modelOut.NonResident[nonResType] = {};
    for (var i = 0; i < moneyCount; i++) {
      _modelOut.MonetaryAmount.push({ImportExport: []});
      if (data.MonetaryAmount[i].ImportExport) {
        for (var j = 0; j < data.MonetaryAmount[i].ImportExport.length; j++) {
          _modelOut.MonetaryAmount[i].ImportExport.push({});
        }
      }
    }

    // Build up the transaction model from the field definition file..
    var sections = fieldMap.allSections();

    _.each(sections, function (section) {

      _.each(section.fields, function (fieldName) {

        

        if (section.scope === 'transaction' && !section.contextKey) {
          // The transaction section
          MakeModelPath(_modelOut, fieldName, {scope: 'transaction', scopeObj: data});
        } else if (((section.contextKey === 'Resident') || (section.contextKey === 'NonResident')) && section.detail.subSection === 'general') {
          // Resident and NonResident general sections.
          MakeModelPath(_modelOut, fieldName, {scope: 'transaction', scopeObj: data});
        } else if (((section.contextKey === 'Resident') || (section.contextKey === 'NonResident')) && section.detail.subSection !== 'general') {
          // Resident and NonResident sections.
          MakeModelPath(_modelOut,
            // need to add the field resolution part (e.g. "NonResident.Entity.")
            [section.contextKey, section.contextKey === 'Resident' ? resType : nonResType, fieldName].join('.'),
            {
              scope: 'transaction',
              scopeObj: data
            });
        } else if (section.scope === 'money') {
          // the money
          for (var i = 0; i < moneyCount; i++) {
            MakeModelPath(_modelOut.MonetaryAmount[i], fieldName, {scope: 'money', scopeObj: data.MonetaryAmount[i]});
          }
        } else if (section.scope === 'importexport') {
          // imports & exports
          for (var i = 0; i < moneyCount; i++) {
            if (_modelOut.MonetaryAmount[i].ImportExport) {
              for (var j = 0; j < _modelOut.MonetaryAmount[i].ImportExport.length; j++) {

                MakeModelPath(_modelOut.MonetaryAmount[i].ImportExport[j], fieldName, {
                  scope: 'importexport',
                  scopeObj: data.MonetaryAmount[i].ImportExport[j]
                });
              }
            }

          }
        }
      })

    })

    return _modelOut;

  }




  return {
    transformIn: transformIn
  }
})
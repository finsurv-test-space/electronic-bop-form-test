///**
// * Created by nishaal on 2/15/16.
// */
//
//
//
//define(['require',
//        'src/app',
//        'angularMocks',
//        'src/fieldDirectives/ccfield',
//        'src/fieldDirectives/datefield',
//        'src/fieldDirectives/ifield',
//        'src/fieldDirectives/mfield',
//        'src/fieldDirectives/sfield',
//
//
//        'src/partials/fields/bfield.html'
//
//
//    ],
//    function (require,app) {
//
//        console.log("Loading bfield_spec.js");
//
//        describe('bField', function () {
//
//
//            var $compile,
//                $rootScope;
//
//            // Load the myApp module, which contains the directive
//            beforeEach(module('bopForm'));
//            beforeEach(module('src/partials/fields/bfield.html'));
//
//            // Store references to $rootScope and $compile
//            // so they are available to all tests in this describe block
//            beforeEach(inject(function (_$compile_, _$rootScope_) {
//                // The injector unwraps the underscores (_) from around the parameter names when matching
//
//                app.initOptions({
//                    config:{
//                        fieldPartialsPath : "src/partials/fields/"
//                    }
//                },function(){})
//                $compile = _$compile_;
//                $rootScope = _$rootScope_;
//            }));
//
//
//            //afterEach(inject(function(){
//            //
//
//
//            it('that bfield compiles', inject(function ($compile, $rootScope) {
//
//                $rootScope.data = {
//                    data : {
//                      foo : 'bar'
//                    },
//                    val: "hello",
//                   property : "foo"
//                };
//                // Compile element and run $digest
//                var htmlTxt = '<bfield label="foo" model="data"></bfield>';
//                var element = $compile(htmlTxt)($rootScope);
//                $rootScope.$digest();
//                console.log(element);
//                console.log(element.length);
//                expect(element[0].innerHTML.length).toBeGreaterThan(0);
//                expect(element.length).toEqual(1);
//
//            }));
//
//
//        });
//
//
//    })
//;
//

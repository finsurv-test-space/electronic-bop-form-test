/**
 * Created by nishaal on 2/10/16.
 */

define(['require',
        'src/app',
        'angularMocks',
        'src/fieldDirectives/ccfield',
        'src/fieldDirectives/datefield',
        'src/fieldDirectives/ifield',
        'src/fieldDirectives/mfield',
        'src/fieldDirectives/sfield',


        'src/partials/fields/sfield.html'


    ],
    function (require,app) {

        console.log("Loading sfield_spec.js");

        describe('sField', function () {


            var $compile,
                $rootScope;

            // Load the myApp module, which contains the directive
            beforeEach(module('bopForm'));
            beforeEach(module('src/partials/fields/sfield.html'));

            // Store references to $rootScope and $compile
            // so they are available to all tests in this describe block
            beforeEach(inject(function (_$compile_, _$rootScope_) {
                // The injector unwraps the underscores (_) from around the parameter names when matching

                app.initOptions({
                    config:{
                        fieldPartialsPath : "src/partials/fields/"
                    }
                },function(){})
                $compile = _$compile_;
                $rootScope = _$rootScope_;
            }));


            //afterEach(inject(function(){
            //


            it('that sfield (val) compiles', inject(function ($compile, $rootScope) {

                $rootScope.data = {
                    val: "hello"
                };

                // Compile element and run $digest
                var htmlTxt = '<sfield label="foo" model="data" lookup-values="[\'foo\',\'bar\']"></sfield>';
                var element = $compile(htmlTxt)($rootScope);
                $rootScope.$digest();

                console.log("Val");
                console.log(element);
                console.log(element.length);

                expect(element[0].innerHTML.length).toBeGreaterThan(0);
                expect(element.length).toEqual(1);

            }));


            it('that sfield (warning) compiles', inject(function ($compile, $rootScope) {

                $rootScope.data = {
                    val: "hello",
                    warning: true,
                    hide: false,
                    msg: ["warning"]
                };

                // Compile element and run $digest
                var htmlTxt = '<sfield label="foo" model="data" lookup-values="[\'foo\',\'bar\']"></sfield>';
                var element = $compile(htmlTxt)($rootScope);
                $rootScope.$digest();

                console.log("Warning");
                console.log(element);
                console.log(element.length);

                expect(element[0].getElementsByTagName('span')[1].classList.contains('ng-hide')).toBeFalsy();
                expect(element[0].innerHTML).toContain("warning");

            }));


            it('that sfield (error) compiles', inject(function ($compile, $rootScope) {

                $rootScope.data = {
                    val: "hello",
                    error: true,
                    hide: false,
                    msg: ["error"]
                };

                // Compile element and run $digest
                var htmlTxt = '<sfield label="foo" model="data" lookup-values="[\'foo\',\'bar\']"></sfield>';
                var element = $compile(htmlTxt)($rootScope);
                $rootScope.$digest();

                console.log("Error");
                console.log(element);
                console.log(element.length);

                expect(element[0].getElementsByTagName('span')[1].classList.contains('ng-hide')).toBeTruthy();
                expect(element[0].innerHTML).toContain("error");

            }));

            it('that sfield (hide) compiles', inject(function ($compile, $rootScope) {

                $rootScope.data = {
                    val: "hello",
                    hide: true,
                    msg: ["hide"]
                };

                // Compile element and run $digest
                var htmlTxt = '<sfield label="foo" model="data" lookup-values="[\'foo\',\'bar\']"></sfield>';
                var element = $compile(htmlTxt)($rootScope);
                $rootScope.$digest();

                console.log("Hide");
                console.log(element);
                console.log(element.length);

                expect(element[0].getElementsByTagName('div')[0].classList.contains('ng-hide')).toBeTruthy();
                expect(element[0].innerHTML).toContain("hide");

            }));



        });


    })
;


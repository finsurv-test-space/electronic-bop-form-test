/**
 * Created by nishaal on 2/12/16.
 */

//TODO: need to complete unit tests for error, warning and hide!!!!!

define(['require',
        'src/app',
        'angularMocks',
        'src/fieldDirectives/ccfield',
        'src/fieldDirectives/datefield',
        'src/fieldDirectives/ifield',
        'src/fieldDirectives/mfield',
        'src/fieldDirectives/sfield',


        'src/partials/fields/dfield.html'


    ],
    function (require,app) {

        console.log("Loading dfield_spec.js");

        describe('dField', function () {


            var $compile,
                $rootScope;

            // Load the myApp module, which contains the directive
            beforeEach(module('bopForm'));
            beforeEach(module('src/partials/fields/dfield.html'));

            // Store references to $rootScope and $compile
            // so they are available to all tests in this describe block
            beforeEach(inject(function (_$compile_, _$rootScope_) {
                // The injector unwraps the underscores (_) from around the parameter names when matching

                app.initOptions({
                    config:{
                        fieldPartialsPath : "src/partials/fields/"
                    }
                },function(){})
                $compile = _$compile_;
                $rootScope = _$rootScope_;
            }));


            //afterEach(inject(function(){
            //


            it('that dfield (val) compiles', inject(function ($compile, $rootScope) {

                $rootScope.data = {
                    val: "hello"
                };

                // Compile element and run $digest
                var htmlTxt = '<dfield label="foo" model="data"></dfield>';
                var element = $compile(htmlTxt)($rootScope);
                $rootScope.$digest();

                console.log("Val");
                console.log(element);
                console.log(element.length);

                expect(element[0].innerHTML.length).toBeGreaterThan(0);
                expect(element.length).toEqual(1);

            }));


        });


    })
;


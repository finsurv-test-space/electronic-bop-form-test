// Karma configuration
// Generated on Tue Apr 28 2015 14:57:14 GMT+0200 (SAST)

module.exports = function (config) {
  config.set({

    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '../',


    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine', 'requirejs'],


    // list of files / patterns to load in the browser
    files: [
      'bower_components/angular/angular.js',
      {pattern: 'bower_components/requirejs/*.js', included: false},
      {pattern: 'bower_components/angular**/*.js', included: false},
      'test/test-main.js',
      {pattern: 'bower_components/underscore/*.js', included: true},
     // {pattern: 'test/spec/**/*.js', included: false},
      {pattern: 'test/spec/directives/*.js', included: false},
      //{pattern: 'src/bopForm/forms/*.js', included: false},
      //{pattern: 'src/bopForm/fieldDirectives/*.js', included: false},
      {pattern: 'src/**/*.js', included: false},
      {pattern: 'node_modules/angular-mocks/*.js', included: false},
      {pattern: 'src/partials/fields/*.html', included: false},
      {pattern: 'rules/*.js', included: false},
      {pattern: 'rules/formDef/*.js', included: false}

    ],


    // list of files to exclude
    //exclude: ['**/coreRules.js'
    //],


    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {
      'src/partials/fields/*.html': ['ng-html2js']
    },


    ngHtml2JsPreprocessor: {
      // strip this from the file path
      stripPrefix: 'base/',
      //prependPrefix: 'app/directive'

      // setting this option will create only a single module that contains templates
      // from all the files, so you can load them all with module('foo')
      //moduleName: 'directiveTemplates'
    },

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['teamcity','progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],


    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: true
  });
};

var allTestFiles = [];
var TEST_REGEXP = /(spec|test)\.js$/i;
for (var file in window.__karma__.files) {
  if (TEST_REGEXP.test(file)) allTestFiles.push(file);
}

requirejs.config({

  paths: {
    'app':'/base/src/app',
    'angular': '/base/bower_components/angular/angular',
    'es6-promise' : '/base/bower_components/es6-promise/es6-promise',
    'angularMocks': '/base/node_modules/angular-mocks/angular-mocks',
    'angular-animate': '/base/bower_components/angular-animate/angular-animate',
    'angular-sanitize': '/base/bower_components/angular-sanitize/angular-sanitize',
    'angular-resource': '/base/bower_components/angular-resource/angular-resource',
    'angular-bootstrap': '/base/bower_components/angular-bootstrap/ui-bootstrap',
    'angular-bootstrap-templates': '/base/bower_components/angular-bootstrap/ui-bootstrap-tpls',
    'underscore': '/base/bower_components/underscore/underscore',
    //'templates':'custom/NBOL/partials/fields',
    //'datapop':'/base/src/bopForm/datapop',
    //'fieldMap':'/base/src/bopForm/fieldMap',
    //'helpers':'/base/src/bopForm/helpers',
    //'bopContext':'/base/src/bopForm/bopContext',
    //'objContext':'/base/src/bopForm/objContext',
    //'defaultForms':'/base/src/bopForm/forms/default',

    //coreRules:'../rules/coreRules'
  },

  // Karma serves files under /base, which is the basePath from your config file
  baseUrl: '/base',

  packages: [
    {
      name: 'coreRules',
      location: 'rules',
      main: 'coreRules'
    }
  ],

  shim: {
    //If asynchronously loading dependencies must use array list {deps:[]}
    'angular': {deps: ['underscore'], 'exports': 'angular'},
    'angularMocks': {deps: ['angular'],'export':'angular.mock'},
    'angular-animate': {deps: ['angular']},
    'angular-sanitize': {deps: ['angular']},
    'angular-resource': {deps: ['angular']},
    'angular-bootstrap': {deps: ['angular']},
    'angular-bootstrap-templates': ['angular', 'angular-bootstrap'],
    'coreRules': {deps: ['angular']},
    //'templates/pfield.html':{deps:['angular']}
},

  // dynamically load all test files
  deps: allTestFiles,

  // we have to kickoff jasmine, as it is asynchronous
  callback: window.__karma__.start
});
